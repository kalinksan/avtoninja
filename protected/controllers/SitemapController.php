<?php

class SitemapController extends Controller
{
    public function actionIndex()
    {
        ini_set('max_execution_time',0);
        ini_set('memory_limit', '128M');
        if (!$xml = Yii::app()->cache->get('sitemap'))
        {    
            $classes = array(
                'MyArchive' => array(DSitemap::WEEKLY, 0.2), 
                'MyObjects' => array(DSitemap::DAILY, 0.8), 
                'MyReview' => array(DSitemap::DAILY, 0.5), 
            );    
 
            $categoryMarks = array(
                'MyReview' => array(DSitemap::DAILY, 0.5), 
                'MyTestdrive' => array(DSitemap::DAILY, 0.5), 
            );  
            
            $categoryModel = array(
                'MyReview' => array(DSitemap::DAILY, 0.5), 
                'MyTestdrive' => array(DSitemap::DAILY, 0.5), 
            );
 
            $sitemap = new DSitemap();
 
            $sitemap->addUrl(Yii::app()->homeUrl, DSitemap::DAILY);
            $sitemap->addUrl(Yii::app()->createUrl('review/default/index'), DSitemap::DAILY);
            $sitemap->addUrl(Yii::app()->createUrl('testdrive/default/index'), DSitemap::DAILY);
 
            foreach ($categoryMarks as $catMark=>$options)  
                $sitemap->addMarkCategory(CActiveRecord::model($catMark)->markCategory()->findAll(), $options[0], $options[1]);

            foreach ($categoryModel as $catModel=>$options)  
                $sitemap->addModelCategory(CActiveRecord::model($catModel)->markCategory()->findAll(), $options[0], $options[1]);
            
            foreach ($classes as $class=>$options)
                $sitemap->addModels(CActiveRecord::model($class)->published()->findAll(), $options[0], $options[1]); 
            $xml = $sitemap->render();
            Yii::app()->cache->set('sitemap', $xml, 3600*6);
        }
        header("Content-type: text/xml");
        echo $xml;
        Yii::app()->end();      
    }
}

