<?php

class PerenosController extends Controller
{
    public function actionModels(){
        //очиска таблицы модели
        /*$model = MyMark::model()->findAll();
        foreach($model as $m){
            $m->delete($m->mark_id);
        }*/
        $xml = simplexml_load_file(dirname(__FILE__).'/../data/autoru_catalog.xml');
        $marksDb = array();
        foreach($xml->mark as $mark){
            $markAt = $mark->attributes();
            foreach($mark->folder as $model){
                $modelAt = $model->attributes();
                if((string)$markAt['name']<>''){
                    $modelPart = explode(',',(string)$modelAt['name']);
                    $marksDb[(string)$markAt['name']][trim($modelPart[0])]=trim($modelPart[0]); 
                }
            }
        }
        foreach($marksDb as $markExp=>$modelsExp){
            $mark = new MyMark();
            $mark->name = $markExp;
            $mark->scenario = 'turboexp';
            if($mark->validate()){
                $mark->save();
            }else{
                print_r( $mark->getErrors());die;
            }
            foreach($modelsExp as $modelExp){
                $mmodel = new MyMmodel();
                $mmodel->name = $modelExp;
                $mmodel->mark_id = $mark->mark_id;
                $mmodel->scenario = 'turboexp';
                if($mmodel->validate()){
                    $mmodel->save();
                }else{
                    print_r( $mmodel->getErrors());die;
                }
            }
           // print_r($mark);die;
        }
    }
    
    public function actionDelObj($id){
        MyObjects::model()->findByPk($id)->delete();
    }
    
	public function actionDelAll(){
        MyObjects::model()->deleteAll();
    }
	
    public function actionLoadTurboD(){
        MyObjects::LoadTurboD();
    }
	
	
	public function actionEmail(){
		$email = Yii::app()->email;
		print_r($email);die;
		$email->to = 'kalinksan@gmail.com';
		$email->subject = 'Hello';
		$email->message = 'Hello brother';
		$email->send();
	}
    
    public function actionOuterTurboD(){
        MyObjects::OuterTurboD();
    }   
    
    public function turboUpload($id,$img_url)
    {           				
        $pathinfo = pathinfo($img_url);
        $ch = curl_init($img_url);
        $path = Yii::getPathOfAlias('webroot') . "/images/turboexp/".$pathinfo['filename'] . '.' . mb_strtolower($pathinfo['extension']);
        $fp = fopen($path, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);
        
        $folder='images/objects/';
        if ($filename === null) {
            //$filename = $pathinfo['filename'];
            $filename = md5(uniqid());
        }
        $ext = $pathinfo['extension'];
        $filename = $filename . '.' . mb_strtolower($ext);
        
        @mkdir($folder.'small');
        @mkdir($folder.'middle');
        @mkdir($folder.'big');
        $image = new Image($path);
        $image->save($folder.$filename);
        
        $image = new Image($path);
        $image->centeredpreview(79, 53);
        $image->save($folder.'small/'.$filename);

        $image = new Image($path);
        $image->centeredpreview(171, 126);
        $image->save($folder.'middle/'.$filename);

        $image = new Image($path);
        $image->centeredpreview(331, 242);
        $image->save($folder.'big/'.$filename); 
        unlink($path);

        if($id > 0) {
            $advertImage = new ObjectsImages();
            $advertImage->image = $filename;
            $advertImage->entity_id = intVal($id);
            $advertImage->save(false);
        }
    }
    
    public function actionGetArchive(){
        //require_once( dirname(__FILE__) . '/db.php');
        //print dirname(__FILE__);die
        $f = fopen(Yii::app()->basePath.'/data/articles.csv', "rt") or die("Ошибка!");
        for ($i=0; $data=fgetcsv($f,1000,";"); $i++) {
            $url = 'http://avto-avto.ru'.$data[0];
            $check_url = Archive::model()->findByAttributes(array('oldurl'=>$url));
            if(empty($check_url)){
                $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $page = curl_exec($ch);
                    curl_close($ch);
                $document = phpQuery::newDocument($page);

                $article_header = trim($document->find('#mainhead #hhead')->text());

                $article_metatitle = trim($document->find('title')->text());

                $content = $document->find('table#tcenter td#content index');
                $pq = pq($content);
                $pq->find('.w100p')->remove();
                $pq->find('div.pad')->remove();
                $pq->find('a[href=http://www.avto-avto.ru/register/]')->parent()->remove();
                $article_text = $pq->html();

                $article_text = trim(iconv('windows-1251','utf-8',$article_text));
                
                $model = new Archive;
                $model->title = $article_header;
                $model->oldurl = $url;
                $model->type = 'article';
                $model->fulltext = $article_text;
                $model->metatitle = $article_metatitle;
                $model->save();
            }
        }
        fclose($f);
    }
    
    public function actionGetAutoshops(){
        $f = fopen(Yii::app()->basePath.'/data/autoshops.csv', "rt") or die("Ошибка!");
        for ($i=0; $data=fgetcsv($f,1000,";"); $i++) {
            $url = 'http://avto-avto.ru'.$data[0];
            $check_url = Archive::model()->findByAttributes(array('oldurl'=>$url));
            if(empty($check_url)){
                $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $page = curl_exec($ch);
                    curl_close($ch);
                $document = phpQuery::newDocument($page);

                $article_header = trim($document->find('#mainhead #hhead')->text());

                $article_metatitle = trim($document->find('title')->text());

                $content = $document->find('#content table.maxwidth');
                $pq = pq($content);
                $pq->find('div:contains("Просмотров:")')->remove();
                $article_text = $pq->htmlOuter();
                
                $article_text = trim(iconv('windows-1251','utf-8',$article_text));
                $model = new Archive;
                $model->title = $article_header;
                $model->oldurl = $url;
                $model->type = 'autoshops';
                $model->fulltext = $article_text;
                $model->metatitle = $article_metatitle;
                $model->save();
            }
        }
        fclose($f);
    }    
    private function curlConnect($url){
        $options = array(
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_USERAGENT      => "Mozilla/5.0 (Windows; U; Windows NT 5.1; de; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3",					
          CURLOPT_AUTOREFERER    => true,
          CURLOPT_CONNECTTIMEOUT => 120,
          CURLOPT_TIMEOUT        => 120,
          CURLOPT_MAXREDIRS      => 10,
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_COOKIEJAR	   => $user_cookie_file,
          CURLOPT_COOKIEFILE	   => $user_cookie_file
        );
        $ch      = curl_init( $url );
        curl_setopt_array( $ch, $options );            
        $page = curl_exec($ch);
        curl_close($ch);
        return $page;
    }

    public function actionGrab(){
        $page = $this->curlConnect('https://auto.yandex.ru/articles');
        $this->grabMarkPage($page);
    }        
    
    private $mark_id;
    private $model_id;
    private $white_site = array('TopGear Russia', 'www.drive.ru', 'motor.ru', 'Авто Mail.Ru');
    
    private function grabMarkPage($page){
        
        $document = phpQuery::newDocument($page);
        $linkMarks = $document->find('.list_content_all a');
        foreach($linkMarks as $mark){
            $mark = pq($mark);
            $hrefMark =  $mark->attr('href');
            $nameMark = preg_replace('/<span[^>]*>(.*?)<\/span>/i', '', $mark->html());
            $dbMark = MyMark::model()->findByAttributes(array('name' => trim($nameMark)));
            if($dbMark){
                $this->mark_id = $dbMark->mark_id;
                $pageMarks = $this->curlConnect('https://auto.yandex.ru'.$hrefMark);
                $this->grabModelPage($pageMarks);
            }else{
                continue;
            }
        }        
    }
    
    private function grabModelPage($page){
        $document = phpQuery::newDocument($page);
        $linkModel = $document->find('.list_content_popular a');
        foreach($linkModel as $model){
            $model = pq($model);
            $hrefModel =  $model->attr('href');
            $nameModel = preg_replace('/<span[^>]*>(.*?)<\/span>/i', '', $model->html());
            $dbModel = MyMmodel::model()->findByAttributes(array('name' => trim($nameModel),'mark_id' => $this->mark_id));
            if($dbModel){
                $this->model_id = $dbModel->model_id;
                /*
                //Первый запуск для сайтов не входящих в white лист
                $countDbModel = TestdriveModel::model()->countByAttributes(array('model_id'=>$this->model_id));
                if(!$countDbModel){
                    $pageArticles = $this->curlConnect('https://auto.yandex.ru'.$hrefModel.'?sort=dtreviewed_desc');
                    $this->grabArticleBlackPage($pageArticles);
                }else{
                    continue;
                }*/
                $pageArticles = $this->curlConnect('https://auto.yandex.ru'.$hrefModel.'?sort=dtreviewed_desc');
                $this->grabArticlePage($pageArticles);
            }else{
                continue;
            }
        }        
    }
    
    private function grabArticlePage($page){
        $document = phpQuery::newDocument($page);
        $articleBlocks = $document->find('.b-articles .b-article');
        foreach($articleBlocks as $ab){
            $ab = pq($ab);
            $siteSource = trim($ab->find('.b-article__item_type_info a')->text());
            if( in_array($siteSource, $this->white_site) ){
                $siteAuthor = trim($ab->find('.b-article__item_type_info span.b-article__author')->text());
               // $siteDate = trim($ab->find('.b-article__item_type_info span.b-article__date')->text());
                $siteTitle = trim($ab->find('h3 a')->text());
                $siteHref = trim($ab->find('h3 a')->attr('href'));
                $checkSave = MyTestdrive::model()->countByAttributes(array('type'=>'text', 'link'=>$siteHref));
                if(!$checkSave){
                    $testdrive = new MyTestdrive;
                    $testdrive->type = 'text';
                    $testdrive->title = $siteTitle;
                    $testdrive->link = $siteHref;
                    $testdrive->author = $siteAuthor;
                    $testdrive->testdriveModel = $this->model_id;
                    $testdrive->published = 1;
                    if( $testdrive->save() ){
                        $testdrive->saveRelated('testdriveModel');
                        break;
                    }
                }
           }
        }
    }
    
    private function grabArticleBlackPage($page){
        $document = phpQuery::newDocument($page);
        $articleBlocks = $document->find('.b-articles .b-article');
        //счетчик сохраненных статей
        $k = 0;
        //сколько должны сохранить
        $haveSave = 2; 
        foreach($articleBlocks as $ab){
            $ab = pq($ab);
            $siteSource = trim($ab->find('.b-article__item_type_info a')->text());
            $siteAuthor = trim($ab->find('.b-article__item_type_info span.b-article__author')->text());
            // $siteDate = trim($ab->find('.b-article__item_type_info span.b-article__date')->text());
            $siteTitle = trim($ab->find('h3 a')->text());
            $siteHref = trim($ab->find('h3 a')->attr('href'));
            $checkSave = MyTestdrive::model()->countByAttributes(array('type'=>'text', 'link'=>$siteHref));
            if(!$checkSave){
                $testdrive = new MyTestdrive;
                $testdrive->type = 'text';
                $testdrive->title = $siteTitle;
                $testdrive->link = $siteHref;
                $testdrive->author = $siteAuthor;
                $testdrive->testdriveModel = $this->model_id;
                $testdrive->published = 1;
                if( $testdrive->save() ){
                    $testdrive->saveRelated('testdriveModel');
                    $k++;
                    if($k==$haveSave){
                        break;
                    }
                }
            }
        }       
    }
    
    public function actionGetCrash(){
        $f = fopen(Yii::app()->basePath.'/data/crush.csv', "rt") or die("Ошибка!");
        for ($i=0; $data=fgetcsv($f,1000,";"); $i++) {
            $url = 'http://avto-avto.ru/encyclopedia'.$data[0];
            $check_url = Archive::model()->findByAttributes(array('oldurl'=>$url));
            if(empty($check_url)){
                $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $page = curl_exec($ch);
                    curl_close($ch);
                $document = phpQuery::newDocument($page);

                $article_header = trim($document->find('#mainhead #hhead')->text());

                $article_metatitle = trim($document->find('title')->text());

                $content = $document->find('#content index div.dcont');
                $pq = pq($content);
                $pq->find('p.MsoNoSpacing font:contains("По материалам:")')->parent()->remove();
                $pq->find('div.divadv')->remove();
                $article_text = $pq->htmlOuter();
                $article_text = trim(iconv('windows-1251','utf-8',$article_text));
                $model = new Archive;
                $model->title = $article_header;
                $model->oldurl = $url;
                $model->type = 'crash';
                $model->fulltext = $article_text;
                $model->metatitle = $article_metatitle;
                $model->save();
            }
        }
        fclose($f);
    }    
    
    public function actionGetResponse(){
        $f = fopen(Yii::app()->basePath.'/data/response.csv', "rt") or die("Ошибка!");
        for ($i=0; $data=fgetcsv($f,1000,";"); $i++) {
            $url = 'http://avto-avto.ru/encyclopedia'.$data[0];
            $check_url = Archive::model()->findByAttributes(array('oldurl'=>$url));
            if(empty($check_url)){
                $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $page = curl_exec($ch);
                    curl_close($ch);
                $document = phpQuery::newDocument($page);

                $article_header = trim($document->find('#mainhead #hhead')->text());

                $article_metatitle = trim($document->find('title')->text());

                $content = $document->find('#content index');
                $pq = pq($content);
                $pq->find('table.subparts')->remove();
                $pq->find('div.divadv')->remove();
                $pq->find('div.pad')->remove();
                $pq->find('style')->remove();
                $article_text = $pq->html();
                $article_text = trim(iconv('windows-1251','utf-8',$article_text));
                $model = new Archive;
                $model->title = $article_header;
                $model->oldurl = $url;
                $model->type = 'response';
                $model->fulltext = $article_text;
                $model->metatitle = $article_metatitle;
                $model->save();
            }
        }
        fclose($f);
    }     
    
    public function actionGetTech(){
        $f = fopen(Yii::app()->basePath.'/data/tech.csv', "rt") or die("Ошибка!");
        for ($i=0; $data=fgetcsv($f,1000,";"); $i++) {
            $url = 'http://avto-avto.ru/encyclopedia'.$data[0];
            $check_url = Archive::model()->findByAttributes(array('oldurl'=>$url));
            if(empty($check_url)){
                $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $page = curl_exec($ch);
                    curl_close($ch);
                $document = phpQuery::newDocument($page);

                $article_header = trim($document->find('#mainhead #hhead')->text());

                $article_metatitle = trim($document->find('title')->text());

                $content = $document->find('#content index div.dcont');
                $pq = pq($content);
                $pq->find('p.MsoNoSpacing font:contains("По материалам:")')->parent()->remove();
                $pq->find('div.divadv')->remove();
                $article_text = $pq->htmlOuter();
                $article_text = trim(iconv('windows-1251','utf-8',$article_text));
                $model = new Archive;
                $model->title = $article_header;
                $model->oldurl = $url;
                $model->type = 'tech';
                $model->fulltext = $article_text;
                $model->metatitle = $article_metatitle;
                $model->save();
            }
        }
        fclose($f);
    } 
    
    public function actionGetTest(){
        $f = fopen(Yii::app()->basePath.'/data/test.csv', "rt") or die("Ошибка!");
        for ($i=0; $data=fgetcsv($f,1000,";"); $i++) {
            $url = 'http://avto-avto.ru/encyclopedia'.$data[0];
            $check_url = Archive::model()->findByAttributes(array('oldurl'=>$url));
            if(empty($check_url)){
                $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $page = curl_exec($ch);
                    curl_close($ch);
                $document = phpQuery::newDocument($page);

                $article_header = trim($document->find('#mainhead #hhead')->text());

                $article_metatitle = trim($document->find('title')->text());

                $content = $document->find('#content index div.dcont');
                $pq = pq($content);
                $pq->find('p:contains("По материалам:")')->remove();
                $pq->find('div.divadv')->remove();
                $article_text = $pq->htmlOuter();
                $article_text = trim(iconv('windows-1251','utf-8',$article_text));
                $model = new Archive;
                $model->title = $article_header;
                $model->oldurl = $url;
                $model->type = 'test';
                $model->fulltext = $article_text;
                $model->metatitle = $article_metatitle;
                $model->save();
            }
        }
        fclose($f);
    } 
    
    public function actionGetText(){
        $f = fopen(Yii::app()->basePath.'/data/text.csv', "rt") or die("Ошибка!");
        for ($i=0; $data=fgetcsv($f,1000,";"); $i++) {
            $url = 'http://avto-avto.ru/encyclopedia'.$data[0];
            $check_url = Archive::model()->findByAttributes(array('oldurl'=>$url));
            if(empty($check_url)){
                $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $page = curl_exec($ch);
                    curl_close($ch);
                $document = phpQuery::newDocument($page);

                $article_header = trim($document->find('#mainhead #hhead')->text());

                $article_metatitle = trim($document->find('title')->text());

                $content = $document->find('#content index div.dcont');
                $pq = pq($content);
                $pq->find('p:contains("По материалам:")')->remove();
                $pq->find('div.divadv')->remove();
                $article_text = $pq->htmlOuter();
                $article_text = trim(iconv('windows-1251','utf-8',$article_text));
                $model = new Archive;
                $model->title = $article_header;
                $model->oldurl = $url;
                $model->type = 'text';
                $model->fulltext = $article_text;
                $model->metatitle = $article_metatitle;
                $model->save();
            }
        }
        fclose($f);
    }     
	
    public function actionGetRedirectString(){
            //Redirect 301 /encyclopedia/text/nissan/prairie/ http://avto.ninja/archive/2245
            //RewriteRule ^encyclopedia/text/uaz/3153/3376/$ http://avto.ninja/archive/2345 [R=301,L]
            $archives = MyArchive::model()->findAll('type=""');
            foreach($archives as $archive){
                    print "RewriteRule ^".str_replace('http://bestreals.ru/','',$archive->oldurl)."$ http://avto.ninja/archive/".$archive->id." [R=301,L]<br>";
            }
    }
    
    public function actionVika(){
        vika();
    }
}
