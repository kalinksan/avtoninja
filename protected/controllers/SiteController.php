<?php

class SiteController extends Controller
{
	
    	public function filters()
	{
	    return array(
		    'ajaxOnly + citycheck', // we only allow deletion via POST request
	    );
	}
        /**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
        
        public function actionIndex()
	{
	}
        
        public function actionThnx()
        {
            if(!empty($_GET['from'])){
                $this->render('thanks', array('message'=>'Ваш запрос принят! Авто.Ниндзя свяжется с Вами сегодня.'));
            }else{
                throw new CHttpException(404, 'Запрашиваемая страница не существует.');
            }
        }
        
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
            if($error=Yii::app()->errorHandler->error)
            {
                    if(Yii::app()->request->isAjaxRequest)
                            echo $error['message'];
                    else
                            $this->render('error', $error);
            }
	}
        
        public function actionCityCheck(){
            unset(Yii::app()->request->cookies['guessVisible']);
        }
        
        public function actionCityChange(){
            $model = new FormsCityChange;
            $this->performAjaxValidation($model);
	    if(isset($_POST['FormsCityChange'])){
                $model->attributes=$_POST['FormsCityChange'];
                if($model->validate()) {
                    $data = MyCity::model()->findByPk($model->id);
                    if(!empty($data)){
                        Yii::app()->request->cookies['city_name'] = new CHttpCookie('city_name', $data->city_name,array('expire'=>time()+60*60*24*180));
                        Yii::app()->request->cookies['city_id'] = new CHttpCookie('city_id', $model->id, array('expire'=>time()+60*60*24*180));
                        unset(Yii::app()->request->cookies['guessVisible']);
                    }
                }
	    }
            $this->redirect(Yii::app()->request->urlReferrer);
        }
        
        public function actionContacts(){
	    $this->render('pages/contacts',array(
	    ));	    
	}
        
        public function actionAbout(){
	    $this->render('pages/about',array(
	    ));	    
	}

        public function actionCertified(){
	    $this->render('pages/certified',array(
	    ));	    
	}
        
        public function actionLove(){
	    $this->render('pages/love',array(
	    ));	    
	}   
        
        public function actionOtzivy(){
	    $this->render('pages/otzivy',array(
	    ));	    
	}   
        
        public function actionPress(){
	    $this->render('pages/press',array(
	    ));	    
	}  
        
        public function actionPrices(){
	    $this->render('pages/prices',array(
	    ));	    
	}      
        
        public function actionPrivacyPolicy(){
	    $this->render('pages/privacyPolicy',array(
	    ));	    
	}   
        
        public function actionTeam(){
	    $this->render('pages/team',array(
	    ));	    
	}       

        public function actionTopFiveQuestions(){
	    $this->render('pages/topFiveQuestions',array(
	    ));	    
	}  
        
        /**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		/*if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}*/
		// display the login form
		$this->render('login',array('model'=>$model));
	}
        
        protected function performAjaxValidation($model) {
	    if(isset($_POST['ajax'])) {
		echo CActiveForm::validate($model);
		 Yii::app()->end(); 
	    }
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}