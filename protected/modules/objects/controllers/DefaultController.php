<?php

class DefaultController extends Controller {

    private $_model;

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'ajaxOnly + dynamicmodel'
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('view', 'index', 'recomended','orderCall','writeLetter','askQuestion'),
                'users' => array('*'),
            ),
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array(), //'create', 'upload', 'DelImage'
                'roles' => array('@'),
            ),
            array('deny',
                'users' => array('?'),
            ),
        );
    }

    public function actionDynamicModel() {
        $mark_id = Yii::app()->request->getParam('mark_id');
        $data = MyMmodel::model()->findAll(array('condition'=>'mark_id=:mark_id','order'=>'t.name', 'params'=>array(':mark_id'=>(int) $mark_id)));

        $data = CHtml::listData($data, 'model_id', 'name');
        echo CHtml::tag('option', array('value' => ''), CHtml::encode('-- Выберите модель --'), true);
        foreach ($data as $value => $name) {
            echo CHtml::tag('option', array('value' => $value), CHtml::encode($name), true);
        }
    }
/*
	public function actionTestObj() {
		MyObjects::LoadTurboD();
	}*/
	
    public function beforeAction($action) {
        if($action->getId()=="create"){
            if(Yii::app()->user->id != "admin"){
                $this->redirect('http://www.turbodealer.ru/offer?from-AVTO.NINJA#choose', TRUE, 302);
            }else{
                
                return true;
            }
                
        }
        return parent::beforeAction($action);
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {        
        $object = $this->loadModel($id);
        $object->check_data = unserialize(urldecode($object->check_data));
        $testdrivesCount = MyTestdrive::model()->with('testdriveModel')->count(
                array(
                    'condition'=>'testdriveModel.model_id=:mmodel',
                    'params'=>array(
                        ':mmodel'=>$object->model_id
                    )
                )
        );
        $reviewCount = MyReview::model()->count(
            array(
                'with'=>array(
                    'model'=>array(
                        'select'=>'model_id',
                        'condition'=>'model.model_id=:model_id',
                        'params' => array(':model_id'=>$object->model_id),
                        )
                    ),
                )
            );        
        
        $this->pageDescription = '';
        $this->pageKeywords = '';
        $linkPart = parse_url(Yii::app()->request->urlReferrer);
        if ($linkPart['path'] == '/') {
            $search_link = Yii::app()->request->urlReferrer;
        } else {
            $search_link = '/';
        }
        
        Yii::app()->clientScript->registerCssFile(Yii::app()->controller->assetsBase . '/css/jquery.bxslider.css');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->assetsBase . '/js/jquery.bxslider.min.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->assetsBase . '/js/gallery.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->assetsBase . '/js/jquery.maskedinput.min.js');
        
        $this->render($object->state ? 'view_recomended' : 'view', array(
            'model' => $object,
            'testCount' => $testdrivesCount,
            'reviewCount' => $reviewCount,
            'search_link' => $search_link
        ));
    }

    public function actionPreview($id) {
        $this->render('preview', array(
            'model' => $this->loadModelProfile($id),
        ));
    }

    public function actionCreate() {
        $this->pageTitle = 'Добавление объявления';
        $model = new MyObjects;
        $model->city_id = Yii::app()->request->cookies['city_id']->value;
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation(array($model));
        if (isset($_POST['MyObjects'])) {
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model->attributes = $_POST['MyObjects'];

                if ($model->validate()) {
                    if ($model->save(false)) {
                        if (!empty($_POST['images'])) {
                            foreach ($_POST['images'] as $image) {
                                $images = new ObjectsImages();
                                $images->image = $image;
                                $images->entity_id = $model->id;
                                $images->save();
                            }
                        }
                        // Yii::app()->user->setFlash('successMessage',"Объявление успешно добавлено.");
                        $transaction->commit();
                    } else {
                        $transaction->rollback();
                    }
                    $this->redirect(array('preview', 'id' => $model->id));
                }
            } catch (Exception $e) {
                print($e->getMessage());
                $transaction->rollback();
                exit();
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id) {
        $model = $this->loadModelProfile($id);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation(array($model));
        if (isset($_POST['MyObjects'])) {
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model->attributes = $_POST['MyObjects'];
                if ($model->validate()) {
                    if ($model->save(false)) {
                        if (!empty($_POST['images'])) {
                            foreach ($_POST['images'] as $image) {
                                $images = new ObjectsImages();
                                $images->image = $image;
                                $images->entity_id = $model->id;
                                $images->save();
                            }
                        }
                        //Yii::app()->user->setFlash('successMessage_advert',"Ваше объявление обновлено.");
                        $transaction->commit();
                    }
                    //$this->redirect(Yii::app()->request->requestUri);
                    $this->redirect(array('preview', 'id' => $model->id));
                }
            } catch (Exception $e) {
                print($e->getMessage());
                $transaction->rollback();
                exit();
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModelProfile($id)->delete();
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionUpload() {
        Yii::import("ext.EAjaxUpload.qqFileUploader");

        // folder for uploaded files
        $folder = 'images/objects/';
        @mkdir($folder);
        //array("jpg","jpeg","gif","exe","mov" and etc...
        $allowedExtensions = array("jpg", "jpeg", "gif", "png");
        // maximum file size in bytes
        //$sizeLimit = 2 * 1024 * 1024;
        $fileMaxSize['postSize'] = toBytes(ini_get('post_max_size'));
        $fileMaxSize['uploadSize'] = toBytes(ini_get('upload_max_filesize'));

        $sizeLimit = min($fileMaxSize);

        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload($folder);

        $fileSize = filesize($folder . $result['filename']);
        $fileName = $result['filename'];

        if ($_GET['id'] > 0) {
            $advertImage = new ObjectsImages();
            $advertImage->image = $fileName;
            $advertImage->entity_id = intVal($_GET['id']);
            $advertImage->save(false);
        }

        // resize file
        if ($fileName) {
            @mkdir($folder . 'small');
            @mkdir($folder . 'middle');
            @mkdir($folder . 'big');
            $image = new Image($folder . $fileName);
            $image->centeredpreview(79, 53);
            $image->save($folder . 'small/' . $fileName);

            $image = new Image($folder . $fileName);
            $image->centeredpreview(171, 126);
            $image->save($folder . 'middle/' . $fileName);

            $image = new Image($folder . $fileName);
            $image->centeredpreview(331, 242);
            $image->save($folder . 'big/' . $fileName);
            // result echo
            $result['thumbPath'] = '/' . $folder . 'small/' . $fileName;
            $result['thumbWidth'] = 79;
            $result['newname'] = $fileName;
            $result['filename'] = '';
            $fileArr = explode(".", $fileName);
            $result['class'] = $fileArr[0];
            $return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);

            echo $return;
        } else {
            $this->redirect('/');
        }
    }

    public function actionDelImage() {
        $file = $_GET['file'];
        $criteria = new CDbCriteria;
        $criteria->condition = 'image = :image';
        $criteria->params = array(':image' => $file);
        ObjectsImages::model()->deleteAll($criteria);
        findDelFile('images/objects', $file);
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $criteria = new CDbCriteria();
        $cityInfo = getCity();
        $city_id = $cityInfo['cityId'];
        $criteria->addCondition('city_id = :city_id');
        $criteria->params += array(':city_id' => $city_id);
        $criteria->addCondition('published = 1');
        if (!empty($_GET['MyObjects'])) {
            $filterObjects = $_GET['MyObjects'];
            if ($filterObjects['state']) {
                $criteria->compare('state', $filterObjects['state']);
            }
            if (!empty($filterObjects['cost_from'])) {
                $criteria->addCondition('cost >= :cost_from');
                $criteria->params += array(':cost_from' => $filterObjects['cost_from']);
            }
            if (!empty($filterObjects['cost_to'])) {
                $criteria->addCondition('cost <= :cost_to');
                $criteria->params += array(':cost_to' => $filterObjects['cost_to']);
            }
            if (!empty($filterObjects['year_from'])) {
                $criteria->addCondition('year >= :year_from');
                $criteria->params += array(':year_from' => $filterObjects['year_from']);
            }
            if (!empty($filterObjects['year_to'])) {
                $criteria->addCondition('year <= :year_to');
                $criteria->params += array(':year_to' => $filterObjects['year_to']);
            }
            if ($filterObjects['transmission']) {
                $criteria->addInCondition('transmission', $filterObjects['transmission']);
            }
            if ($filterObjects['body_type']) {
                $criteria->addInCondition('body_type', $filterObjects['body_type']);
            }
            if ($filterObjects['privod']) {
                $criteria->addInCondition('privod', $filterObjects['privod']);
            }
            if ($filterObjects['nonpop_mark_id']) {
                if ($filterObjects['mark_id']) {
                    $filterObjects['mark_id'] = array_merge($filterObjects['mark_id'], $filterObjects['nonpop_mark_id']);
                } else {
                    $filterObjects['mark_id'] = $filterObjects['nonpop_mark_id'];
                }
            }
            if ($filterObjects['mark_id']) {
                $criteria->addInCondition('mark_id', $filterObjects['mark_id']);
            }
            if ($filterObjects['engine']) {
                $criteria->addInCondition('engine', $filterObjects['engine']);
            }
            if (!empty($filterObjects['volume_from'])) {
                $criteria->addCondition('volume >= :volume_from');
                $criteria->params += array(':volume_from' => $filterObjects['volume_from']);
            }
            if (!empty($filterObjects['volume_to'])) {
                $criteria->addCondition('volume <= :volume_to');
                $criteria->params += array(':volume_to' => $filterObjects['volume_to']);
            }

            if (!empty($filterObjects['km_age_from'])) {
                $criteria->addCondition('km_age >= :km_age_from');
                $criteria->params += array(':km_age_from' => $filterObjects['km_age_from']);
            }
            if (!empty($filterObjects['km_age_to'])) {
                $criteria->addCondition('km_age <= :km_age_to');
                $criteria->params += array(':km_age_to' => $filterObjects['km_age_to']);
            }
            if ($filterObjects['wheel']) {
                $criteria->addInCondition('wheel', $filterObjects['wheel']);
            }
        }

        $dataProvider = new CActiveDataProvider('MyObjects', array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 20,
            ),
            'sort' => array(
                'attributes' => array(
                    'cost' => array(
                        'asc' => 'cost ASC',
                        'desc' => 'cost DESC',
                        'default' => 'desc',
                    ),
                    'km_age' => array(
                        'asc' => 'km_age ASC',
                        'desc' => 'km_age DESC',
                        'default' => 'desc',
                    ),
                    'year' => array(
                        'asc' => 'year ASC',
                        'desc' => 'year DESC',
                        'default' => 'desc',
                    ),
                    'created' => array(
                        'asc' => 'created ASC',
                        'desc' => 'created DESC',
                        'default' => 'desc',
                    ),
                ),
                'defaultOrder' => array(
                    'created' => CSort::SORT_DESC,
                ),
            )
                )
        );
        $this->pageDescription = 'Объявления о продаже авто с пробегом — цены на б/у машины в Москве, Санкт-Петербурге, Екатеринбурге, Новосибирске и РФ. Тест-драйвы и отзывы на Авто.Ниндзе';
	$this->pageKeywords = 'Авто Ниндзя, продажа автомобилей в России, купить авто с пробегом, бу машины в России, объявления о продаже авто, б у авто'; 
	$this->pageTitle = 'Авто.Ниндзя — авто объявления о продаже, тест-драйвы б/у автомобилей, отзывы';
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionRecomended() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('state = :state');
        $criteria->params += array(':state' => 1);
        $criteria->with = 'city';
        $criteria->addCondition('published = 1');
        
        if (!empty($_GET['MyObjects'])) {
            $filterObjects = $_GET['MyObjects'];
            if ($filterObjects['state']) {
                $criteria->compare('state', $filterObjects['state']);
            }
            if (!empty($filterObjects['cost_from'])) {
                $criteria->addCondition('cost >= :cost_from');
                $criteria->params += array(':cost_from' => $filterObjects['cost_from']);
            }
            if (!empty($filterObjects['cost_to'])) {
                $criteria->addCondition('cost <= :cost_to');
                $criteria->params += array(':cost_to' => $filterObjects['cost_to']);
            }
            if (!empty($filterObjects['year_from'])) {
                $criteria->addCondition('year >= :year_from');
                $criteria->params += array(':year_from' => $filterObjects['year_from']);
            }
            if (!empty($filterObjects['year_to'])) {
                $criteria->addCondition('year <= :year_to');
                $criteria->params += array(':year_to' => $filterObjects['year_to']);
            }
            if ($filterObjects['transmission']) {
                $criteria->addInCondition('transmission', $filterObjects['transmission']);
            }
            if ($filterObjects['body_type']) {
                $criteria->addInCondition('body_type', $filterObjects['body_type']);
            }
            if ($filterObjects['privod']) {
                $criteria->addInCondition('privod', $filterObjects['privod']);
            }
            if ($filterObjects['nonpop_mark_id']) {
                if ($filterObjects['mark_id']) {
                    $filterObjects['mark_id'] = array_merge($filterObjects['mark_id'], $filterObjects['nonpop_mark_id']);
                } else {
                    $filterObjects['mark_id'] = $filterObjects['nonpop_mark_id'];
                }
            }
            if ($filterObjects['mark_id']) {
                $criteria->addInCondition('mark_id', $filterObjects['mark_id']);
            }
            if ($filterObjects['engine']) {
                $criteria->addInCondition('engine', $filterObjects['engine']);
            }
            if (!empty($filterObjects['volume_from'])) {
                $criteria->addCondition('volume >= :volume_from');
                $criteria->params += array(':volume_from' => $filterObjects['volume_from']);
            }
            if (!empty($filterObjects['volume_to'])) {
                $criteria->addCondition('volume <= :volume_to');
                $criteria->params += array(':volume_to' => $filterObjects['volume_to']);
            }

            if (!empty($filterObjects['km_age_from'])) {
                $criteria->addCondition('km_age >= :km_age_from');
                $criteria->params += array(':km_age_from' => $filterObjects['km_age_from']);
            }
            if (!empty($filterObjects['km_age_to'])) {
                $criteria->addCondition('km_age <= :km_age_to');
                $criteria->params += array(':km_age_to' => $filterObjects['km_age_to']);
            }
            if ($filterObjects['wheel']) {
                $criteria->addInCondition('wheel', $filterObjects['wheel']);
            }
        }

        $dataProvider = new CActiveDataProvider('MyObjects', array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 20,
            ),
            'sort' => array(
                'attributes' => array(
                    'cost' => array(
                        'asc' => 'cost ASC',
                        'desc' => 'cost DESC',
                        'default' => 'desc',
                    ),
                    'km_age' => array(
                        'asc' => 'km_age ASC',
                        'desc' => 'km_age DESC',
                        'default' => 'desc',
                    ),
                    'year' => array(
                        'asc' => 'year ASC',
                        'desc' => 'year DESC',
                        'default' => 'desc',
                    ),
                    'created' => array(
                        'asc' => 'created ASC',
                        'desc' => 'created DESC',
                        'default' => 'desc',
                    ),
                ),
                'defaultOrder' => array(
                    'created' => CSort::SORT_DESC,
                ),
            )
            )
        );
        
        $this->pageDescription = '';
	$this->pageKeywords = ''; 
	$this->pageTitle = 'Проверено Авто.Ниндзя';
        
        $this->render('recomended', array(
            'dataProvider' => $dataProvider,
        ));
    }    
    
    public function loadModelProfile($id) {
        $user_id = Yii::app()->user->id;
        if ($this->_model === null && $user_id) {
            if (isset($id)) {
                $criteria = new CDbCriteria;
                $criteria->condition = 'id=:id AND user_id=:user_id';
                $criteria->params = array(':id' => $id, ':user_id' => $user_id);
                $this->_model = MyObjects::model()->find($criteria);
            }
            if ($this->_model === null)
                throw new CHttpException(404, 'Запрашиваемая страница не существует.');
        }
        return $this->_model;
    }

    public function loadModel($id) {
        $model = MyObjects::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'Запрашиваемая страница не существует.');
        return $model;
    }
    
    public function actionOrderCall(){
        $model = new FormsOrderCall;
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
        $model->setScenario('captchaCheck');
        if(isset($_POST['FormsOrderCall'])) {
            $model->attributes=$_POST['FormsOrderCall'];
            if($model->validate() && check_robot($_POST['g-recaptcha-response'])) {
                $email = Yii::app()->email;
                $email->from = Yii::app()->params['fromEmail'];
                $email->to = Yii::app()->params['adminEmail'];
                $email->subject = 'Заполнена форма "Заказать звонок"';
                $email->message = CHtml::link($_POST['FormsOrderCall']['title'],Yii::app()->createAbsoluteUrl('/objects/default/view',array('id'=>$_POST['FormsOrderCall']['id'])));
                $email->message .= '<br>';
                $email->message .= 'Телефон: '.$_POST['FormsOrderCall']['phone'];
                $email->send();  
                Yii::app()->user->setFlash('message','Ваш запрос принят, Авто.Ниндзя свяжется с Вами сегодня');
                $this->redirect(array('/site/thnx','from'=>'objects/' . $_POST['FormsOrderCall']['id']));             
                exit();
            }
        }
        Yii::app()->user->setFlash('message','Отправлены неверные данные.');
        $this->redirect(array('/objects/view','id'=>$_POST['FormsOrderCall']['id'])); 
    }
    
    public function actionWriteLetter(){
        $model = new FormsWriteLetter();
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
        $model->setScenario('captchaCheck');
        if(isset($_POST['FormsWriteLetter'])) {
            $model->attributes=$_POST['FormsWriteLetter'];
            if($model->validate() && check_robot($_POST['g-recaptcha-response'])) {
                $email = Yii::app()->email;
                $email->from = Yii::app()->params['fromEmail'];
                $email->to = Yii::app()->params['adminEmail'];
                $email->subject = 'Заполнена форма "Написать письмо"';
                $email->message = CHtml::link($_POST['FormsWriteLetter']['title'],Yii::app()->createAbsoluteUrl('/objects/default/view',array('id'=>$_POST['FormsWriteLetter']['id'])));
                $email->message .= '<br>';
                $email->message .= 'Вопрос: '.$_POST['FormsWriteLetter']['text'];
                $email->message .= '<br>';
                $email->message .= 'E-mail: '.$_POST['FormsWriteLetter']['email'];
                if ( $_POST['FormsWriteLetter']['phone'] ) {
                    $email->message .= '<br>';
                    $email->message .= 'Телефон: '.$_POST['FormsWriteLetter']['phone'];
                }
                $email->send();
                Yii::app()->user->setFlash('message','Ваш запрос принят, Авто.Ниндзя свяжется с Вами сегодня');
                $this->redirect(array('/site/thnx','from'=>'objects/' . $_POST['FormsWriteLetter']['id']));                
                exit();
            }
        }
        Yii::app()->user->setFlash('message','Отправлены неверные данные.');
        $this->redirect(array('/objects/view','id'=>$_POST['FormsWriteLetter']['id'])); 
    }    
    
    public function actionAskQuestion(){
        
        $model = new FormsAskQuestion();
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
        $model->setScenario('captchaCheck');
        if(isset($_POST['FormsAskQuestion'])) {
            $model->attributes=$_POST['FormsAskQuestion'];
            if($model->validate() && check_robot($_POST['g-recaptcha-response'])) {
                $email = Yii::app()->email;
                $email->from = Yii::app()->params['fromEmail'];
                $email->to = Yii::app()->params['adminEmail'];
                $email->subject = 'Заполнена форма "Задать вопрос"';
                $email->message = CHtml::link($_POST['FormsAskQuestion']['title'],Yii::app()->createAbsoluteUrl('/objects/default/view',array('id'=>$_POST['FormsAskQuestion']['id'])));
                $email->message .= '<br>';
                $email->message .= 'Вопрос: '.$_POST['FormsAskQuestion']['text'];
                $email->message .= '<br>';
                $email->message .= 'E-mail: '.$_POST['FormsAskQuestion']['email'];
                $email->send();
                Yii::app()->user->setFlash('message','Ваш запрос принят, Авто.Ниндзя свяжется с Вами сегодня');
                $this->redirect(array('/site/thnx','from'=>'objects/' . $_POST['FormsAskQuestion']['id']));                
                exit();
            }
        }
        Yii::app()->user->setFlash('message','Форма не была отправлена. Введены неверные данные.');
        $this->redirect(array('/objects/view','id'=>$_POST['FormsAskQuestion']['id'])); 
    }    
    
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax'])) {
            $errors = CActiveForm::validate($model);
            if(!empty($_POST['g-recaptcha-response'])) {
                $model->setScenario('captchaCheck');
                $errors = CActiveForm::validate($model);
            }			
            echo $errors;
            Yii::app()->end();
        }
    }
}
