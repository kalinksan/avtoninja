<a class="item" href="<?php echo Yii::app()->createUrl('objects/view',array('id'=>$data->id))?>">
        <div class="image">
            <?php if(!empty($data->objectsImages[0])):?>
                <div class="enlargement">
                    <?php echo CHtml::image('/images/objects/big/'.$data->objectsImages[0]->image);?>
                </div>
                <?php echo CHtml::image('/images/objects/middle/'.$data->objectsImages[0]->image);?>
            <?php endif;?>
        </div>
    <div class="price">
        <p class="green_bold"><?php echo number_format($data->cost, 0, '.', ' ')?> Руб</p>
        <p class="green_light"><?php echo $data->service?></p>
    </div>
    <div class="year">
        <p class="green_bold"><?php echo $data->year?></p>
    </div>
    <div class="details">
        <p class="green_bold"><?php echo $data->mark->name . ' ' . $data->model->name ?></p>
        <p>
            <?php 
            $option_str = '';
            $option_str .= MyObjects::$body_type[$data->body_type] ? mb_strtolower(MyObjects::$body_type[$data->body_type], 'UTF-8').', ' : '';
            $option_str .= ($data->volume/1000).' л, ';
            $option_str .= mb_strtolower(MyObjects::$engine[$data->engine], 'UTF-8').', ';
            $option_str .= MyObjects::$privod[$data->privod] ? mb_strtolower(MyObjects::$privod[$data->privod], 'UTF-8').' привод, ' : '';
            $option_str .= number_format($data->km_age, 0, '.', ' ').' км';
            echo $option_str;
            ?>
        </p>
            <?php if(count($data->objectsImages)):?>
                <p class="green_light"><?php echo count($data->objectsImages);?> фото</p>
            <?php endif;?>
            <span class="star"></span> 
            <span class="date"><?php echo Yii::app()->dateFormatter->format('d MMMM yyyy', $data->updated)?></span>
    </div>   
</a>