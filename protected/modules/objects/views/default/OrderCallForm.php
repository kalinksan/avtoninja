<div class="hidden-block" >
    <div id="orderCall" class="pop-form">
        <div class="block_header">Продовец перезвонит</div>
        <div class="add_block">
            <?php
            $form=Yii::app()->controller->beginWidget('CActiveForm', array(
                'id'=>'order-call-form',
                'method' => 'POST',
                'action'=>Yii::app()->createUrl('/objects/orderCall'),
                'clientOptions' => array(
                    'validateOnSubmit' => 'true',
                ),
                'enableAjaxValidation' => true,
                'enableClientValidation'=>false,
            )); 
            ?>
                <div class="field text-field">
                    <?php echo $form->hiddenField($model,'title', array('value' => $title)); ?>
                    <?php echo $form->hiddenField($model,'id', array('value' => $id)); ?>
                    <?php echo $form->textField($model,'phone', array('class'=>'phone-mask','placeholder' => 'Введите ваш номер')); ?>
                    <?php echo $form->error($model,'phone'); ?>
                </div>
                <div class="field text-field">
                    <div class="field g-recaptcha" id="recapOrder"></div>
                    <?php echo $form->hiddenField($model,'recaptcha', array()); ?>
                    <?php echo $form->error($model,'recaptcha'); ?>
                </div>
                <div class="buttons">
                    <?php echo CHtml::submitButton('Жду звонка!',array('class'=>'publish')); ?>
                </div>
            <?php Yii::app()->controller->endWidget(); ?>
        </div>
    </div>
</div>
