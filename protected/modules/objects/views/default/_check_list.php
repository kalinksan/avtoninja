<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->assetsBase.'/js/jquery.collapse-0.9.2.min.js');?>
<?php Yii::app()->clientScript->registerScript('collapse', "
$(document).ready(function(){
    $('#checkList').collapse({
    show: function(){
                    this.animate({
                        opacity: 'toggle', 
                        height: 'toggle'
                    }, 700);
                },
    hide : function() {
                    this.animate({
                        opacity: 'toggle', 
                        height: 'toggle'
                    }, 700);
                }
    });
});
"); 
?>
<h2 class="about-avto check-list">Результаты технической проверки:</h2>
<div id="checkList">
    <?php foreach($checkData as $groupName => $checkParams):?>
        <div class="chel_wrap">
            <h3><?php echo $checkListGroup[$groupName];?></h3>       
            <div class="chel_body">
                <?php foreach($checkParams as $checkParamId => $checkParamName):?>
                    <div class="chel_item">
                        <div class="chel_p <?php echo $checkParamName['value'] ? 'true' : 'false'?>"><?php echo $checkListParam[$groupName][$checkParamId];?></div>
                        <div class="chel_comment"><?php echo $checkParamName['description']?></div>
                    </div>
                <?php endforeach;?> 
            </div>
        </div>
    <?php endforeach;?>
</div>