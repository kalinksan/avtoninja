<div id="askQuestion" class="form">
    <div class="block_header">Задать вопрос</div>
    <div class="add_block">
        <?php
        $form=Yii::app()->controller->beginWidget('CActiveForm', array(
            'id'=>'ask-question-form',
            'method' => 'POST',
            'action'=>Yii::app()->createUrl('/objects/askQuestion'),
            'clientOptions' => array(
                'validateOnSubmit' => 'true',
            ),
            'enableAjaxValidation' => true,
            'enableClientValidation'=>false,
        )); 
        ?>

            <?php echo $form->hiddenField($model,'title', array('value' => $title)); ?>
            <?php echo $form->hiddenField($model,'id', array('value' => $id)); ?>
            <div class="field textarea-field">    
                <?php echo $form->textArea($model,'text', array('placeholder' => 'Напишите вопрос')); ?>
                <?php echo $form->error($model,'text'); ?> 
            </div>
            <div class="field text-field">
                <?php echo $form->textField($model,'email', array('placeholder' => 'Ваш E-mail (для ответа)')); ?>
                <?php echo $form->error($model,'email'); ?>
            </div>
            <div class="field text-field">
                <div class="field g-recaptcha"  id="recapAsk"></div>
                <?php echo $form->hiddenField($model,'recaptcha', array()); ?>
                <?php echo $form->error($model,'recaptcha'); ?>
            </div>
            <div class="buttons">
                <?php echo CHtml::submitButton('Отправить',array('class'=>'publish')); ?>
            </div>
        <?php Yii::app()->controller->endWidget(); ?>
    </div>
</div>

