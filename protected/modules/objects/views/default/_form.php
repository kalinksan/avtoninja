<?php
    Yii::app()->clientScript->registerScript('del_review_img',
    "
    $('a.del-image').live('click',function(e){
        e.preventDefault();
        if(confirm('Вы действительно хотите удалить картинку?')){
                file = $(this).attr('data-delete');
                el = $(this);
                $.ajax({
                        type: 'GET',
                        url: '/objects/DelImage',
                        data: { file:file },
                }).done(function(html) {
                        el.parent().parent().fadeOut(300, function() {
                                $(this).remove();
                                if(!$('.qq-upload-list.loaded li').length) {
                                        $('.qq-upload-list.loaded').remove();
                                }
                        })
                        filecount = filecount - 1;
                });
        }
    });
    ");
?>
<?php 
    Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->assetsBase.'/js/jquery.maskedinput.min.js');
    $form=$this->beginWidget('CActiveForm', array(
    'id'=>'my-objects-form',
    'clientOptions' => array(
        'validateOnSubmit' => 'true',
    ),
    'enableAjaxValidation'=>true,
    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>
    <table>
        <tr>
            <td class="left_row"><?php echo $form->labelEx($model,'user_name'); ?></td>
            <td>
                <div class="input_container">
                    <?php echo $form->textField($model,'user_name',array('size'=>60,'maxlength'=>255)); ?>
                </div>
                <?php echo $form->error($model,'user_name'); ?>
            </td>
            <td></td>
        </tr>
        <tr>
            <td class="left_row"><?php echo $form->labelEx($model,'user_email'); ?></td>
            <td>
                <div class="input_container">
                    <?php echo $form->textField($model,'user_email',array('size'=>60,'maxlength'=>50)); ?>
                </div>
                <?php echo $form->error($model,'user_email'); ?>
            </td>
            <td></td>
        </tr>
        <tr>
            <td class="left_row"><?php echo $form->labelEx($model,'user_phone'); ?></td>
            <td>
                <div class="input_container">
                    <?php echo $form->textField($model,'user_phone',array('size'=>60,'maxlength'=>20)); ?>
                </div>
                <?php echo $form->error($model,'user_phone'); ?>
            </td>
            <td></td>
        </tr>
        
        <tr>
            <td class="left_row"><?php echo $form->labelEx($model,'city_id'); ?></td>
            <td>
                <?php echo $form->dropDownList($model,'city_id', MyObjects::CityChoices(),array('empty'=>'Город','data-placeholder' => 'Город',)); ?>
                <?php echo $form->error($model,'city_id'); ?>	
            </td>
            <td>
                <?php /*echo $form->dropDownList($model,'metro', MyObjects::MetroChoices(),$options); ?>
                <?php echo $form->error($model,'metro');*/ ?>										
            </td>
        </tr>
        
        <tr>
            <td class="left_row">Выберите параметры</td>
            <td>
                <?php 
                    echo $form->dropDownList($model,'mark_id', MyMmodel::MarkChoices(),
                        array( 
                            'empty'=>'Марка',
                            'data-placeholder' => 'Марка',
                            'ajax' => array(
                                'type'=>'GET', //request type
                                'url'=>$this->createUrl('dynamicmodel'),
                                'data'=>array(
                                    'mark_id'=>'js:this.value'
                                ),
                                'success' => 'js:function(data){$("#MyObjects_model_id").html(data); $("select#MyObjects_model_id").trigger("refresh")}',
                            ),
                        )
                    );
                ?>
                <?php echo $form->error($model,'mark_id'); ?>	
            </td>
            <td>
                <?php
                    $mmodels=array();
                    if(isset($model->id)){
                        $list=MyMmodel::model()->findAll('mark_id=:mark_id', array(':mark_id'=>(int) $model->mark_id));
                        $mmodels = CHtml::listData($list,'model_id','name');
                    }                
                ?>
                <?php echo $form->dropDownList($model,'model_id', $mmodels,array('empty'=>'Модель','data-placeholder'=>'Модель')); ?>
                <?php echo $form->error($model,'model_id'); ?>										
            </td>
        </tr>        
        
        <tr>
            <td class="left_row"></td>
            <td>
                <?php echo $form->dropDownList($model,'year', MyObjects::YearsChoices(),array('empty'=>'Год выпуска','data-placeholder' => 'Год выпуска')); ?>
                <?php echo $form->error($model,'year'); ?>	
            </td>
            <td>
                <div class="input_container">
                    <?php echo $form->textField($model,'km_age', array('size'=>60,'maxlength'=>50, 'placeholder' => 'Пробег, км')); ?>
                </div>
                <?php echo $form->error($model,'km_age'); ?>
            </td>
        </tr> 
        
        <tr>
            <td class="left_row"></td>
            <td>
                <?php echo $form->dropDownList($model,'body_type', MyObjects::$body_type,array('empty'=>'Тип кузова','data-placeholder' => 'Тип кузова')); ?>
                <?php echo $form->error($model,'body_type'); ?>	
            </td>
            <td>
                <?php echo $form->dropDownList($model,'color', MyObjects::$color,array('empty'=>'Цвет','data-placeholder' => 'Цвет')); ?>
                <?php echo $form->error($model,'color'); ?>	
            </td>
        </tr>
        
        <tr>
            <td class="left_row"></td>
            <td>
                <?php echo $form->dropDownList($model,'volume', MyObjects::VolumeChoices(),array('empty'=>'Объем двигателя','data-placeholder' => 'Объем двигателя','encode'=> false)); ?>
                <?php echo $form->error($model,'volume'); ?>	
            </td>
            <td>
                <?php echo $form->dropDownList($model,'transmission', MyObjects::$transmission,array('empty'=>'Коробка передач','data-placeholder' => 'Коробка передач')); ?>
                <?php echo $form->error($model,'transmission'); ?>	
            </td>
        </tr>
        
        <tr>
            <td class="left_row"></td>
            <td>
                <?php echo $form->dropDownList($model,'engine', MyObjects::$engine,array('empty'=>'Тип двигателя','data-placeholder' => 'Тип двигателя')); ?>
                <?php echo $form->error($model,'engine'); ?>	
            </td>
            <td>
                <?php echo $form->dropDownList($model,'privod', MyObjects::$privod,array('empty'=>'Привод','data-placeholder' => 'Привод')); ?>
                <?php echo $form->error($model,'privod'); ?>	
            </td>
        </tr>
        
        <tr>
            <td class="left_row"></td>
            <td>
                <?php echo $form->dropDownList($model,'wheel', MyObjects::$wheel,array('empty'=>'Руль','data-placeholder' => 'Руль')); ?>
                <?php echo $form->error($model,'wheel'); ?>	
            </td>
        </tr>        

        <tr>
            <td class="left_row top"><?php echo $form->labelEx($model,'description'); ?></td>
            <td colspan="2">
                <div class="textarea_container">
                        <?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
                </div>
                <?php echo $form->error($model,'description'); ?>
            </td>							
        </tr>
        
        <tr>
            <td class="left_row"><?php echo $form->labelEx($model,'cost'); ?></td>
            <td colspan="2">
                <div class="price_container">
                    <?php echo $form->textField($model,'cost',array('size'=>20,'maxlength'=>20)); ?>
                </div>
                <div class="rub">руб</div>
                <div class="price_description">Правильно указывайте цену
                        <div class="price_popup">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
                </div>
                <?php echo $form->error($model,'cost',array('class'=>'errorMessage spacer')); ?>
            </td>							
        </tr>        
        <tr>
            <td class="left_row top">Фотографии
                    <p>Вы можете загрузить не более 10 фотографий</p>
            </td>
            <td colspan="2">
                    <div class="select_file_container">
			<?php
			$input_image = '';
			if($model->isNewRecord) {
                            $input_image = '<input name=images[] type=hidden value="+responseJSON.newname+" />';
			}
			$this->widget('ext.EAjaxUpload.EAjaxUpload', array(
				'id' => 'object_uploadFile',
				'multiple'=> true,
				'config' => array(
					'request' => array(
						'endpoint' => '/objects/upload',
						'params' => array(
                                                    'object_id' => $model->id,
                                                    Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                                                    ),
					),
					'text' => array(
						'uploadButton' => '',
						'formatProgress' => '{percent}%',
                                                'deleteButton' => 'удалить'
					),
                                        'fileTemplate'=> '<li>'.
                                            '<div class="qq-progress-bar"></div>'.
                                            '<span class="qq-upload-spinner"></span>'.
                                            '<span class="qq-upload-finished"></span>'.
                                            '<span class="qq-upload-thumb"></span>'.
                                            '<span class="upload-delete-file"></span>'.
                                            '<span class="qq-upload-file"></span>'.
                                            '<span class="qq-upload-size"></span>'.
                                            '<a class="qq-upload-cancel" href="#">{cancelButtonText}</a>'.
                                            '<a class="qq-upload-retry" href="#">{retryButtonText}</a>'.
                                            '<a class="qq-upload-delete" href="#">{deleteButtonText}</a>'.
                                            '<span class="qq-upload-status-text">{statusText}</span>'.
                                            '</li>',                                    
					'validation' => array(
						'allowedExtensions' => array('jpg', 'jpeg', 'png', 'bmp','gif'),
						'sizeLimit' => 5048576, // max file size in bytes
						'countLimit' => 10,
						'fileCount' => count($model->objectsImages),
					   // 'minSizeLimit' => 256, // min file size in bytes
					),
					'callbacks' => array(
						'onComplete' => 'js:function(id, fileName, responseJSON){'
                                                                    . '$("#object_uploadFile").val(responseJSON.filename);'
                                                                    . '$("#object_uploadFile ."+responseJSON.class+" .upload-delete-file").html("'.$input_image.'<a class=del-image href=# data-delete="+responseJSON.newname+">Удалить</a>");'
                                                                    . '$(".upload-main-image .qq-upload-button").hide();}',
					),
				)
			));	
			?>
                        <?php if($model->objectsImages[0]): ?>
                        <ul class="qq-upload-list loaded">
                                <?php foreach($model->objectsImages as $item): ?>
                                    <li>
                                        <?php echo CHtml::image(ObjectsImages::$smallImageDir.$item->image)?>
                                        <a class="upload-delete-file del-image" href="javascript:;" data-delete="<?php echo $item->image; ?>">Удалить</a>
                                    </li>
                                <?php endforeach; ?>
                        </ul>		
                        <?php endif; ?>                          
                    </div>
            </td>							
        </tr>        
        <tr>
                <td><?php echo CHtml::submitButton('Опубликовать',array('class'=>'publish')); ?></td>
                <td colspan="2">
                    <?php echo CHtml::submitButton('поручить опубликовать на avto.ru и пр.',array('class'=>'publish2')); ?>    
                </td>			
        </tr>
    </table>

<?php $this->endWidget(); ?>