<div class="inner_block">
    <?php echo Yii::app()->controller->renderPartial('application.views.textBlocks.topRecommendedTextBlock'); ?>
    <a id="scrolling" href="#"></a>
    <?php $this->widget('zii.widgets.CListView',array(
            'dataProvider'=>$dataProvider,
            'itemView'=>'_view_recomended',
            'summaryText'=>'',
            'sorterHeader'=>'',
            //'ajaxUpdate'=>false,
            //'cssFile'=>Yii::app()->controller->assetsBase.'/css/objectViewList.css',
            'enableHistory'=>true, 
            'sortableAttributes'=>array(
                'cost',
                'km_age',
                'year',
                'created'=>'Дата',
            ),
            'afterAjaxUpdate'=>"function(id, data) {
                $.ias({
                    'history': false,
                    'triggerPageTreshold': 1,
                    'trigger': 'Загрузить ещё',
                    'container': '#obj-all > .results',
                    'item': '.item',
                    'pagination': '#obj-all .pager',
                    'next': '#obj-all .next:not(.disabled):not(.hidden) a',
                    'loader': 'Загрузка...'
                });
            }",
            'template' => '{sorter} {items} {pager}',
            'pager' => array(
                'class' => 'ext.infiniteScroll.IasPager', 
                'rowSelector'=>'.item', 
                'itemsSelector'=>' > .results',
                'listViewId' => 'obj-all', 
                'header' => '',
                'loaderText'=>'Загрузка...',
                'options' => array('history' => false, 'triggerPageTreshold' => 1, 'trigger'=>'Показать ещё'),
            ),
            'id'=>'obj-all',
            'itemsCssClass'=>'results',
            'htmlOptions'=>array(
            )
    )); ?>
</div>