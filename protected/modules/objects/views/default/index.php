<div class="inner_block">
    <a id="scrolling" href="#"></a>
    <?php echo Yii::app()->controller->renderPartial('application.views.reklama.topRekBlock');?>
    <?php $this->widget('zii.widgets.CListView',array(
            'dataProvider'=>$dataProvider,
            'itemView'=>'_view',
            'summaryText'=>'',
            'sorterHeader'=>'',
            //'ajaxUpdate'=>false,
            //'cssFile'=>Yii::app()->controller->assetsBase.'/css/objectViewList.css',
            'enableHistory'=>true, 
            'sortableAttributes'=>array(
                'cost',
                'km_age',
                'year',
                'created'=>'Дата',
            ),
            'afterAjaxUpdate'=>"function(id, data) {
                $.ias({
                    'history': false,
                    'triggerPageTreshold': 1,
                    'trigger': 'Загрузить ещё',
                    'container': '#obj-all > .results',
                    'item': '.item',
                    'pagination': '#obj-all .pager',
                    'next': '#obj-all .next:not(.disabled):not(.hidden) a',
                    'loader': 'Загрузка...'
                });
            }",
            'template' => '{sorter} {items} {pager}',
            'pager' => array(
                'class' => 'ext.infiniteScroll.IasPager', 
                'rowSelector'=>'.item', 
                'itemsSelector'=>' > .results',
                'listViewId' => 'obj-all', 
                'header' => '',
                'loaderText'=>'Загрузка...',
                'options' => array('history' => false, 'triggerPageTreshold' => 1, 'trigger'=>'Показать ещё'),
            ),
            'id'=>'obj-all',
            'itemsCssClass'=>'results',
            'htmlOptions'=>array(
            )
    )); ?>
    <?php echo Yii::app()->controller->renderPartial('application.views.reklama.bottomRekBlock');?>

</div>
    <div class="text_block_bottom">
        <?php echo Yii::app()->controller->renderPartial('application.views.textBlocks.bottomTextBlock'); ?>
    </div>
<?php
    Yii::app()->clientScript->registerScript('search',
    "
    var ajaxUpdateTimeout;
    $('#searchform').change(function(event) {
        SearchFunc();
        return false;
    });
    
    $('#searchform input').keydown(function (event) {
        if (event.keyCode && event.keyCode == '13') {
            SearchFunc();
            return false;
        } else {
            return true;
        }
    });
    /*$('#searchform input').keyup(function(){
        clearTimeout(ajaxUpdateTimeout);
        ajaxUpdateTimeout = setTimeout(SearchFunc, 800);
    });   */ 

    function SearchFunc(){
        var data = $('form#searchform').serialize();
        var url = document.URL;
        var params = $.param(data);
        url = url.substr(0, url.indexOf('?'));
        window.History.pushState(null, document.title, $.param.querystring(url, data));
		$('html, body').animate({ scrollTop: $('#scrolling').offset().top }, 500);  
    }
    ");

    $this->beginClip('sidebar');
        $this->renderPartial('_searchForm', array(
            'dataProvider'=>$dataProvider,
        ));
    $this->endClip();