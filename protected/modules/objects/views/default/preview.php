<div class="check_block">
    <div class="rounded_subheader">Проверьте объявление. Если всё правильно, нажмите на кнопку «Далее».</div>
<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'mark_id',
		'model_id',
		'user_name',
		'user_email',
		'user_phone',
		'city_id',
		'year',
		'cost',
		'km_age',
		'body_type',
		'color',
		'volume',
		'transmission',
		'engine',
		'privod',
		'wheel',
		'state',
		'description',
	),
));?>
    <div class="links_block">
        <?php echo CHtml::link('назад', Yii::app()->createUrl('/objects/update',array('id'=>$model->id)),array('class'=>'back'));?>
        <?php echo CHtml::link('далее', Yii::app()->createUrl('/objects/view',array('id'=>$model->id)),array('class'=>'forvard'));?>
    </div>
</div>
