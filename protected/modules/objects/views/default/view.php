<?php
$this->pageTitle = $model->mark->name . ' ' . $model->model->name . $privod . ' ' . ($model->volume / 1000) . ' ' . MyObjects::$transmissionMin[$model->transmission];
if ($model->city->city_locative) {
    $this->pageTitle .= ' в ' . $model->city->city_locative;
} else {
    $this->pageTitle .= ' в ' . $model->city->city_name;
}
?>
<div class="inner_block">
    <?php echo Yii::app()->controller->renderPartial('application.views.reklama.topRekBlock'); ?>
    <ul class="submenu">
        <li><?php echo CHtml::link('Назад к результатам поиска', $search_link) ?></li>
        <?php if ($reviewCount): ?>
            <li><?php echo CHtml::link('Отзывы '.$model->mark->name.' '.$model->model->name, $this->createUrl('/review/default/list', array('model' => $model->model->mname,'mark' => $model->mark->mname))) . '(' . $reviewCount . ')'; ?></li>
        <?php endif; ?>
        <?php if ($testCount): ?>
            <li><?php echo CHtml::link('Тест драйвы '.$model->mark->name.' '.$model->model->name, $this->createUrl('/testdrive/default/list', array('model' => $model->model->mname,'mark' => $model->mark->mname))) . '(' . $testCount . ')'; ?></li>
        <?php endif; ?>
    </ul>
    <div class="gray_block advertisement">
        <span class="star"></span>
        <?php $privod = ($privod == 'polnyi') ? ' 4WD' : '' ?>
        <h1>
            <?php
            $header = 'Продажа ' . $model->mark->name . ' ' . $model->model->name . $privod . ' ' . ($model->volume / 1000) . ' ' . MyObjects::$transmissionMin[$model->transmission];
            if ($model->city->city_locative) {
                $header .= ' в ' . $model->city->city_locative;
            } else {
                $header .= ' в ' . $model->city->city_name;
            }
            echo $header;
            ?>
        </h1>
        <div class="left_pane">
            <?php if (!empty($model->objectsImages) && $model->object_status): ?>
                <div id="objectGallery">
                    <ul class="bxObject">
                        <?php foreach ($model->objectsImages as $k => $img): ?>
                            <li>
                                <?php echo CHtml::image('/images/objects/big/' . $img->image, '', array()) ?>									
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <div id="bx-pager-object">
                        <?php foreach ($model->objectsImages as $k => $img): ?>
                            <?php echo CHtml::link(CHtml::image('/images/objects/small/' . $img->image, '', array()), '/images/objects/big/' . $img->image, array('data-slide-index' => $k)) ?>									
                        <?php endforeach; ?>
                    </div> 
                </div>
            <?php elseif (!$model->object_status): ?>
                <div class="soldout">
                    <b>Продано!</b><br>Вы можете посмотреть другие объявления на <a href="<?php echo Yii::app()->homeUrl ?>">главной странице</a>.
                </div>
            <?php endif; ?>
        </div>
        <div class="right_pane">
            <table>
                <tr>
                    <td class="left_row">Цена</td>
                    <td><?php echo number_format($model->cost, 0, '.', ' ') . ' руб.' ?></td>
                </tr>
                <tr>
                    <td class="left_row">Год выпуска</td>
                    <td><?php echo $model->year . ' г.' ?></td>
                </tr>
                <tr>
                    <td class="left_row">Пробег</td>
                    <td><?php echo number_format($model->km_age, 0, '.', ' ') . ' км' ?></td>
                </tr>
                <tr>
                    <td class="left_row">Кузов</td>
                    <td><?php echo MyObjects::$body_type[$model->body_type] ?></td>
                </tr>
                <tr>
                    <td class="left_row">Двигатель</td>
                    <td><?php echo ($model->volume / 1000) . 'л / ' . MyObjects::$engine[$model->engine] ?></td>
                </tr>
                <tr>
                    <td class="left_row">КПП</td>
                    <td><?php echo MyObjects::$transmission[$model->transmission] ?></td>
                </tr>
                <tr>
                    <td class="left_row">Привод</td>
                    <td><?php echo MyObjects::$privod[$model->privod] ?></td>
                </tr>
                <tr>
                    <td class="left_row">Цвет</td>
                    <td><?php echo MyObjects::$color[$model->color] ?></td>
                </tr>
                <tr>
                    <td class="left_row">Руль</td>
                    <td><?php echo MyObjects::$wheel[$model->wheel] ?></td>
                </tr>
            </table>
            <?php if ($model->user_phone && $model->object_status && $model->published): ?>
                <?php $phone_part_last = substr($model->user_phone, 13); ?>
                <?php $phone_part_first = substr($model->user_phone, 0, 13); ?>
                <div class="phone_trig">
                        <a href="tel:<?php echo $phone_part_first ?><?php echo $phone_part_last ?>" class="phone"><?php echo $phone_part_first ?><span class="full_number_text">...Посмотреть</span><span class="full_number"><?php echo $phone_part_last ?></span></a>
                </div>
            <?php endif; ?>
        </div>
        <div class="spacer"></div>
        <?php if (trim($model->description) <> ''): ?>
            <div class="description_block">
                <h2 class="about-avto">Подробнее об автомобиле:</h2>
                <p><?php echo nl2br($model->description)?></p>
            </div>
        <?php endif; ?>
    </div>
    <?php echo Yii::app()->controller->renderPartial('application.views.reklama.bottomRekBlock'); ?>
</div>		