<?php 
    $model = $dataProvider->model;
    if($_GET['MyObjects']){
        $model->attributes = $_GET['MyObjects'];
    } 
?>
<?php echo CHtml::beginForm('', 'get', array('id'=>'searchform')); ?>
    <div class="item active">Параметры</div>
    <div class="subitems" style="display:block">
        <div class="state_check"><?php echo CHtml::link('Проверено Авто.Ниндзей',Yii::app()->createUrl('objects/recomended'))?></div>
        <div class="select_container">
                <div class="input1">
                    <?php echo CHtml::activeTextField($model, 'cost_from',array('placeholder'=>'Цена от','class'=>'intonly'));?>					
                </div>
                <div class="input1 no_border">
                    <?php echo CHtml::activeTextField($model, 'cost_to',array('placeholder'=>'Цена до','class'=>'intonly'));?>
                </div>
        </div>
        <div class="subheader">Год выпуска</div>
        <div class="select_container">
                <div class="select1">
                    <?php echo CHtml::activeDropDownList($model,'year_from', MyObjects::YearsChoices(),array('empty'=>'от','data-placeholder' => 'от')); ?>
                </div>					
                <div class="select1 no_border">
                    <?php echo CHtml::activeDropDownList($model,'year_to', MyObjects::YearsChoices(),array('empty'=>'до','data-placeholder' => 'до')); ?>
                </div>
        </div>
        <ul class="checkbox_list">  
            <?php       
                echo CHtml::activeCheckBoxList($model,'transmission',MyObjects::$transmission, array(
                'template' => '<li>{input}{label}</li>',
                'separator' => '',
            )) ?>
            <!--<li>
                <?php echo CHtml::activeCheckBox($model,'with_photo') ?>
                <?php echo CHtml::activeLabel($model,'with_photo') ?>
            </li>-->
        </ul>
</div>
<div class="item">Кузов</div>

<div class="subitems">
        <?php       
        echo CHtml::activeCheckBoxList($model,'body_type',MyObjects::$body_type, array(
            'template' => '<div class="body_type">{input}{label}</div>',
            'separator' => '',
        )) ?>	
        <div class="subheader">Привод</div>
        <ul class="checkbox_list">
            <?php       
                echo CHtml::activeCheckBoxList($model,'privod',MyObjects::$privod, array(
                'template' => '<li>{input}{label}</li>',
                'separator' => '',
            )) ?>					
        </ul>
        <div class="rspacer"></div>		
</div>
<div class="item">Марка</div>
<div class="subitems">
        <div class="filter-model active popular"><a href="#">Популярные</a></div>
        <div class="filter-model all"><a href="#">Все</a></div>
        <ul class="checkbox_list">
                <?php       
                    echo CHtml::activeCheckBoxList($model,'mark_id',MyMmodel::MarkChoices('popular'), array(
                    'template' => '<li>{input}{label}</li>',
                    'separator' => '',
                )) ?>
                <?php       
                echo CHtml::activeCheckBoxList($model,'nonpop_mark_id',MyMmodel::MarkChoices('non-popular'), array(
                    'template' => '<li class="non-popular">{input}{label}</li>',
                    'separator' => '',
                ))?>
        </ul>
</div>
<div class="item">Двигатель</div>
<div class="subitems">
        <ul class="checkbox_list">
            <?php       
                echo CHtml::activeCheckBoxList($model,'engine',MyObjects::$engine, array(
                'template' => '<li>{input}{label}</li>',
                'separator' => '',
            )) ?>
        </ul>
        <div class="subheader">Объем двигателя</div>
        <div class="select_container">
                <div class="select1">
                    <?php echo CHtml::activeDropDownList($model,'volume_from', MyObjects::VolumeChoices(),array('empty'=>'от','data-placeholder' => 'от','encode'=> false)); ?>
                </div>					
                <div class="select1 no_border">
                    <?php echo CHtml::activeDropDownList($model,'volume_to', MyObjects::VolumeChoices(),array('empty'=>'до','data-placeholder' => 'до','encode'=> false)); ?>
                </div>
        </div>
        <div class="subheader">Пробег</div>
        <div class="select_container">
                <div class="input1">
                    <?php echo CHtml::activeTextField($model, 'km_age_from',array('placeholder'=>'от','class'=>'intonly'));?>					
                </div>
                <div class="input1 no_border">
                    <?php echo CHtml::activeTextField($model, 'km_age_to',array('placeholder'=>'до','class'=>'intonly'));?>
                </div>
        </div>
        <div class="rspacer"></div>	
</div>
<div class="item">Детали</div>
<div class="subitems">
    <div class="subheader">Руль</div>
    <ul class="checkbox_list fl">
        <?php       
            echo CHtml::activeCheckBoxList($model,'wheel',MyObjects::$wheel, array(
            'template' => '<li>{input}{label}</li>',
            'separator' => '',
        )) ?>
    </ul>
</div>
<?php echo CHtml::endForm(); ?>