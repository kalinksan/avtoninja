<div class="add_block">
<?php
    Yii::app()->clientScript->registerScript('del_review_img',
    "
    $('a.del-image').live('click',function(e){
        e.preventDefault();
        if(confirm('Вы действительно хотите удалить картинку?')){
                file = $(this).attr('data-delete');
                el = $(this);
                $.ajax({
                        type: 'GET',
                        url: '/review/DelImage',
                        data: { file:file },
                }).done(function(html) {
                        el.parent().parent().fadeOut(300, function() {
                                $(this).remove();
                                if(!$('.qq-upload-list.loaded li').length) {
                                        $('.qq-upload-list.loaded').remove();
                                }
                        })
                        $('.qq-upload-button').show();
                        $('.upload-main-image').show();
                });
        }
    });
    ");
?>
<?php 
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'review-form',
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>
        <div class="row">
            <div class="half-column">
                <div class="row">
                        <?php echo $form->labelEx($model,'mark_id'); ?>
                        <?php 
                            echo $form->dropDownList($model,'mark_id', MyMmodel::MarkChoices(),
                                array( 
                                    'empty'=>'Марка',
                                    'data-placeholder' => 'Марка',
                                    'ajax' => array(
                                        'type'=>'GET', //request type
                                        'url'=>$this->createUrl('/review/dynamicmodel'),
                                        'data'=>array(
                                            'mark_id'=>'js:this.value',
                                        ),
                                        'success' => 'js:function(data){$("#MyReview_model_id").html(data); $("select#MyReview_model_id").trigger("refresh")}',
                                    ),
                                )
                            );
                        ?>
                        <?php echo $form->error($model,'mark_id'); ?>
                </div>

                <div class="row">
                        <?php echo $form->labelEx($model,'model_id'); ?>
                        <?php
                            $mmodels=array();
                            if(isset($model->id)){
                                $list=MyMmodel::model()->findAll(array('condition'=>'mark_id=:mark_id','order'=>'t.name', 'params'=>array(':mark_id'=>(int) $model->mark_id)));
                                $mmodels = CHtml::listData($list,'model_id','name');
                            }                
                        ?>
                        <?php echo $form->dropDownList($model,'model_id', $mmodels,array('empty'=>'Модель','data-placeholder'=>'Модель')); ?>
                        <?php echo $form->error($model,'model_id'); ?>
                </div>

                <div class="row">
                        <?php echo $form->labelEx($model,'own'); ?>
                        <?php echo $form->dropDownList($model,'own', $model::$own,array('empty'=>'Владею','data-placeholder' => 'Владею')); ?>
                        <?php echo $form->error($model,'own'); ?>
                </div>

                <div class="row">
                        <?php echo $form->labelEx($model,'year'); ?>
                        <?php echo $form->dropDownList($model,'year', MyObjects::YearsChoices(),array('empty'=>'Год выпуска','data-placeholder' => 'Год выпуска')); ?>
                        <?php echo $form->error($model,'year'); ?>
                </div>
            </div>

            <div class="half-column">
                <div class="row">
                        <?php echo $form->labelEx($model,'face'); ?>
                        <?php
                            $this->widget('CStarRating',array(
                                'model'=>$model,
                                'attribute'=>'face',
                                'resetText'=>'Сбросить',
                                'minRating'=>1,
                                'maxRating'=>5,
                                'starCount'=>5,
                                ));
                        ?>
                        <?php echo $form->error($model,'face'); ?>
                </div>

                <div class="row">
                        <?php echo $form->labelEx($model,'comfort'); ?>
                        <?php
                            $this->widget('CStarRating',array(
                                'model'=>$model,
                                'attribute'=>'comfort',
                                'resetText'=>'Сбросить',
                                'minRating'=>1,
                                'maxRating'=>5,
                                'starCount'=>5,
                                ));
                        ?>
                        <?php echo $form->error($model,'comfort'); ?>
                </div>

                <div class="row">
                        <?php echo $form->labelEx($model,'safety'); ?>
                        <?php
                            $this->widget('CStarRating',array(
                                'model'=>$model,
                                'attribute'=>'safety',
                                'resetText'=>'Сбросить',
                                'minRating'=>1,
                                'maxRating'=>5,
                                'starCount'=>5,
                                ));
                        ?>
                        <?php echo $form->error($model,'safety'); ?>
                </div>

                <div class="row">
                        <?php echo $form->labelEx($model,'reliability'); ?>
                        <?php
                            $this->widget('CStarRating',array(
                                'model'=>$model,
                                'attribute'=>'reliability',
                                'resetText'=>'Сбросить',
                                'minRating'=>1,
                                'maxRating'=>5,
                                'starCount'=>5,
                                ));
                        ?>
                        <?php echo $form->error($model,'reliability'); ?>
                </div>

                <div class="row">
                        <?php echo $form->labelEx($model,'driving'); ?>
                        <?php
                            $this->widget('CStarRating',array(
                                'model'=>$model,
                                'attribute'=>'driving',
                                'resetText'=>'Сбросить',
                                'minRating'=>1,
                                'maxRating'=>5,
                                'starCount'=>5,
                                ));
                        ?>
                        <?php echo $form->error($model,'driving'); ?>
                </div>
            </div>
        </div>
    
	<div class="row-img row">
		<?php echo $form->labelEx($model,'main_image'); ?>
                    <div class="select_file_container">
			<?php
                        echo $form->hiddenField($model,'main_image');
                        ?>
                        <!--<div <?php print (empty($model->main_image) || $model->main_image=="NULL") ? '' : 'style="display:none"' ?> >-->
                        <div class="upload-main-image" <?php print (empty($model->main_image) || $model->main_image=="NULL") ? '' : 'style="display:none"' ?>>
                            <?php
                            $this->widget('ext.EAjaxUpload.EAjaxUpload', array(
                                'id' => 'review_uploadFile',
                                'multiple'=> false,
                                'config' => array(
                                    'request' => array(
                                            'endpoint' => '/review/upload',
                                            'params' => array(
                                                'review_id' => $model->id,
                                                Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                                            ),
                                    ),
                                    'text' => array(
                                            'uploadButton' => '',
                                            'formatProgress' => '{percent}%',
                                            'deleteButton' => 'удалить',
                                            'retryButton' => 'повторить',
                                            'failUpload' => 'ошибка загрузки',
                                    ),
                                    'fileTemplate'=> '<li>'.
                                        '<div class="qq-progress-bar"></div>'.
                                        '<span class="qq-upload-spinner"></span>'.
                                        '<span class="qq-upload-finished"></span>'.
                                        '<span class="qq-upload-thumb"></span>'.
                                        '<span class="upload-delete-file"></span>'.
                                        '<span class="qq-upload-file"></span>'.
                                        '<span class="qq-upload-size"></span>'.
                                        '<a class="qq-upload-cancel" href="#">{cancelButtonText}</a>'.
                                        '<a class="qq-upload-retry" href="#">{retryButtonText}</a>'.
                                        '<a class="qq-upload-delete" href="#">{deleteButtonText}</a>'.
                                        '<span class="qq-upload-status-text">{statusText}</span>'.
                                        '</li>',                                    
                                    'validation' => array(
                                            'allowedExtensions' => array('jpg', 'jpeg', 'png', 'bmp','gif'),
                                            'sizeLimit' => 5048576, // max file size in bytes
                                       // 'minSizeLimit' => 256, // min file size in bytes
                                    ),
                                    'callbacks' => array(
                                            'onComplete' => 'js:function(id, fileName, responseJSON){'
                                                . '$("#MyReview_main_image").val(responseJSON.newname);'
                                                . '$("#review_uploadFile .upload-delete-file").html("<a class=del-image href=# data-delete="+responseJSON.newname+">удалить</a>");'
                                                . '$(".qq-upload-button").hide();}',
                                    ),
                                )
                            ));	
                            ?>
                        </div>
                        <?php if($model->main_image): ?>
                        <ul class="qq-upload-list loaded">
                            <li>
                                <span class="qq-upload-thumb">
                                    <?php echo CHtml::image($model::$smallImageDir.$model->main_image)?>
                                </span>
                                <span class="upload-delete-file">
                                    <a class="del-image" href="javascript:;" data-delete="<?php echo $model->main_image; ?>">удалить</a>
                                </span>
                            </li>
                        </ul>		
                        <?php endif; ?>                          
                    </div>
		<?php echo $form->error($model,'main_image'); ?>
	</div>    
	<div class="row row-long">
		<?php echo $form->labelEx($model,'title'); ?>
                <div class="input_container">
                    <?php echo $form->textField($model,'title',array('maxlength'=>255)); ?>
                </div>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row row-long">
		<?php echo $form->labelEx($model,'fulltext'); ?>
                <div class="textarea_container">
                    <?php echo $form->textArea($model,'fulltext'); ?>
                </div>
                <?php echo $form->error($model,'fulltext'); ?>
	</div>
        <div class="row plus-minus row-long">
            <div class="half-column">
                <div class="row">
                        <?php echo $form->labelEx($model,'plus',array('class'=>'plus')); ?>
                        <div class="textarea_container">
                            <?php echo $form->textArea($model,'plus'); ?>
                        </div>
                        <?php echo $form->error($model,'plus'); ?>
                </div>
            </div>

            <div class="half-column">
                <div class="row">
                        <?php echo $form->labelEx($model,'minus',array('class'=>'minus')); ?>
                        <div class="textarea_container">
                            <?php echo $form->textArea($model,'minus'); ?>
                        </div>
                        <?php echo $form->error($model,'minus'); ?>
                </div>
            </div>
            <p class="hint">
                Каждое новое предложение в полях "плюсы" и "минусы" следует писать с новой строки.
            </p>
        </div>
	<div class="row email">
		<?php echo $form->labelEx($model,'email'); ?>
                <div class="input_container">
                    <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255)); ?>
                </div>
                <p class="hint">
                    Будет использоваться только для отправки вам уведомлении о публикации вашего отзыва.
                </p>
                <?php echo $form->error($model,'email'); ?>

	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Отправить' : 'Сохранить', array('class'=>'publish')); ?>
	</div>

<?php $this->endWidget(); ?>
</div>