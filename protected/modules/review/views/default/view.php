<?php $this->pageTitle = $model->metatitle ? $model->metatitle : $model->title;?>
<?php $this->pageDescription = $model->metadesc;?>
<?php $this->pageKeywords = $model->metakey;?>
<?php //$this->pageCanonicalUrl = $model->canonical; ?>
<?php 
if(!$model->moderation){
  Yii::app()->clientScript->registerMetaTag("noindex", 'robots');  
  Yii::app()->clientScript->registerMetaTag("noindex, nofollow", 'robots'); 
}  
?>
<div class="inner_block">
    <?php echo Yii::app()->controller->renderPartial('application.views.reklama.topRekBlock');?>   
    <?php if($model->moderation):?>
    <ul class="submenu">
        <li><?php echo CHtml::link('Объявления', Yii::app()->homeUrl);?></li>
        <?php if($testdriveCount):?>
            <li>
            <?php 
                echo CHtml::link(
                'Тест-драйвы '.$model->mark->name.' '.$model->model->name, 
                Yii::app()->createUrl('/testdrive/default/list', array('model'=>$model->model->mname,'mark' => $model->mark->mname))
                );
            ?>
            </li>
        <?php endif;?>
        <li>
            <?php echo CHtml::link(
                'Отзывы '.$model->mark->name.' '.$model->model->name, 
                Yii::app()->createUrl('/review/default/list', array('model'=>$model->model->mname,'mark' => $model->mark->mname))
                );?>
        </li>
        <?php if($isEdit):?>
        <li class="edit-link">
            <?php echo CHtml::link('Редактировать', Yii::app()->createUrl('/review/update',array('id'=>$model->id, 'mark' => $model->mark->mname, 'model' => $model->model->mname)))?>
        </li>
        <?php endif;?>
    </ul>
    <?php endif;?>
    <div class="gray_block reply">
        <?php if(!$model->moderation):?>
            <div class="flash-box moderation">Ваш отзыв будет опубликован на сайте после прохождения модерации.</div>
        <?php endif;?>
        <h1><?php echo $model->title;?></h1>
        <div class="rating_block">
            <div class="block_header"><?php echo Yii::app()->dateFormatter->format('d MMMM yyyy', $model->created);?></div>
            <table>
                    <tr>
                            <td>Внешний вид:</td>
                            <td>
                                <?php
                                    $this->widget('review.components.NinjaStarsWidget',array(
                                        'countStars' => 5,
                                        'countSelect' => $model->face,
                                    ));
                                ?>
                            </td>
                    </tr>
                    <tr>
                            <td>Комфорт:</td>
                            <td>
                                <?php
                                    $this->widget('review.components.NinjaStarsWidget',array(
                                        'countStars' => 5,
                                        'countSelect' => $model->comfort,
                                    ));
                                ?>
                            </td>
                    </tr>
                    <tr>
                            <td>Безопасность:</td>
                            <td>
                                <?php
                                    $this->widget('review.components.NinjaStarsWidget',array(
                                        'countStars' => 5,
                                        'countSelect' => $model->safety,
                                    ));
                                ?>
                            </td>
                    </tr>
                    <tr>
                            <td>Надежность:</td>
                            <td>
                                <?php
                                    $this->widget('review.components.NinjaStarsWidget',array(
                                        'countStars' => 5,
                                        'countSelect' => $model->reliability,
                                    ));
                                ?>
                            </td>
                    </tr>
                    <tr>
                            <td>Ходовые качества:</td>
                            <td>
                                <?php
                                    $this->widget('review.components.NinjaStarsWidget',array(
                                        'countStars' => 5,
                                        'countSelect' => $model->driving,
                                    ));
                                ?>
                            </td>
                    </tr>
            </table>
        </div>
        <div class="positive_block">
                <div class="block_header">Плюсы</div>
                <?php 
                    $plus = explode("\n",$model->plus);
                    foreach($plus as $p):?>
                    <p><?php echo trim($p);?></p>
                <?php endforeach;?>
        </div>
        <div class="negative_block">
                <div class="block_header">Минусы</div>
                <?php 
                    $minus = explode("\n",$model->minus);
                    foreach($minus as $m):?>
                    <p><?php echo trim($m);?></p>
                <?php endforeach;?>
        </div>
        <div class="reply_block_spacer"></div>
        <div class="reply_text">
            <?php //echo '<p>'.str_replace("\r\n", '</p><p>', $model->fulltext).'</p>';?>
            <?php echo nl2br($model->fulltext);?>
        </div>
    </div>
    <?php echo Yii::app()->controller->renderPartial('application.views.reklama.bottomRekBlock');?>
</div>
<?php $this->beginClip('sidebar');?>
<div class="usefull_goods_block">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- На Авто Ниндзю -->
    <ins class="adsbygoogle"
    style="display:block"
    data-ad-client="ca-pub-7917871120550568"
    data-ad-slot="6092430064"
    data-ad-format="auto"></ins>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</div>
<?php $this->endClip();?>