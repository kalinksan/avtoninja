<?php
    $pageTitle = 'Отзывы ' . $mark->name . ' ' . $reviews[0]->model->name;
    $synonymMark = explode(',', trim($mark->synonym));
    $synonymModel = explode(',', trim($reviews[0]->model->synonym));
    $synonym = array_merge($synonymMark,$synonymModel);
    $pageTitle = $synonym ? $pageTitle . " (" .  trim(implode(', ', $synonym), ', ') . ")" : $pageTitle; 
    $this->pageTitle = $pageTitle;  
    $this->pageDescription = isset($reviews[0]->model->review_metadesc) ? $reviews[0]->model->review_metadesc : '';
?>
<?php if($reviews[0]->model->text_block_review == ''){
    Yii::app()->clientScript->registerMetaTag("noindex", 'robots');  
    Yii::app()->clientScript->registerMetaTag("noindex, nofollow", 'robots'); 
}?>
<?php //$this->pageCanonicalUrl = $model->canonical;    ?>
<?php //if($model->noindex) Yii::app()->clientScript->registerMetaTag("noindex", 'robots');    ?>
<div class="inner_block">
    <?php echo Yii::app()->controller->renderPartial('application.views.reklama.topRekBlock'); ?>
    <ul class="submenu">
        <li><?php echo CHtml::link('Объявления', Yii::app()->homeUrl);?></li>
        <?php if($testdriveCount):?>
            <li>
                <?php echo CHtml::link(
                    'Тест-драйв '.$mark->name, 
                    Yii::app()->createUrl('/testdrive/default/ModelList', array('mark' => $mark->mname))
                );?>
            </li>
        <?php endif;?>
        <li>
            <?php echo CHtml::link(
                    'Отзывы '.$mark->name, 
                    Yii::app()->createUrl('/review/default/ModelList', array('mark' => $mark->mname))
                    );
            ?>
        </li>
    </ul>
    <div class="gray_block reply">
        <h1><?php echo $pageTitle ?></h1>
        <p class="under-h"><?php echo  Yii::t('test', '{n} отзыв|{n} отзыва|{n} отзывов|{n} отзыва', count($reviews)).' (последний '.Yii::app()->dateFormatter->format('d MMMM yyyy', $reviews[0]->created).')';?></p>
        <div class="grade_block">
            <span class="grade_text">Средняя оценка </span>
            <?php
                $this->widget('review.components.NinjaStarsWidget',array(
                    'countStars' => 5,
                    'countSelect' => $srOcenka,
                ));
            ?>            
        </div>        
        <?php foreach ($reviews as $review): ?>
            <div class="reply_item">
                <?php if ($review->main_image): ?>
                    <div class="image">
                        <?php echo CHtml::link(CHtml::image('/images/reviews/thumb/' . $review->main_image, '', array('width' => '95px', 'height' => '95px')), $this->createUrl('/review/default/view', array('id' => $review->id, 'mark'=>$mark->mname, 'model'=>$review->model->mname))) ?>
                    </div>
                <?php endif; ?>
                <div class="description">
                    <h2 class="item_header">
                        <span class="txt">
                            <?php// print_r($review);die;?>
                            <?php echo CHtml::link($review->title, $this->createUrl('/review/default/view', array('id' => $review->id, 'mark'=>$mark->mname, 'model'=>$review->model->mname))) ?>
                        </span>
                        <?php
                            $this->widget('review.components.NinjaStarsWidget',array(
                                'countStars' => 5,
                                'countSelect' => round(($review->comfort+$review->face+$review->safety+$review->reliability+$review->driving)/5),
                            ));
                        ?>
                    </h2>
                    <p><?php echo truncateText($review->fulltext,15,'...')?></p>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    
    <?php echo Yii::app()->controller->renderPartial('application.views.reklama.bottomRekBlock'); ?>
    
    <?php if(!empty($gallerys)):?>
        <div class="gallery">
            <?php foreach($gallerys as $gallery):?>
                <?php if(!empty($gallery->gallery->galleryPhotos)):?>
                    <?php foreach($gallery->gallery->galleryPhotos as $img):?>
                        <?php echo CHtml::image('/gallery/'.$img->id.'big.jpg',$img->name);?>
                    <?php endforeach;?>
                <?php endif;?>
            <?php endforeach;?>
        </div>
    <?php endif;?>
    
    <?php if($reviews[0]->model->text_block_review!=''):?>
        <div class="text_block_bottom">
            <?php echo $reviews[0]->model->text_block_review;?>
        </div>
    <?php endif;?>  
</div>
<?php $this->beginClip('sidebar'); ?>
<div class="usefull_goods_block">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- На Авто Ниндзю -->
    <ins class="adsbygoogle"
    style="display:block"
    data-ad-client="ca-pub-7917871120550568"
    data-ad-slot="6092430064"
    data-ad-format="auto"></ins>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</div>
<?php $this->endClip(); ?>
