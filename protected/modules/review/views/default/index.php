<?php $this->pageTitle = 'Отзывы'; ?>
<?php //$this->pageCanonicalUrl = $model->canonical;  ?>
<?php //if($model->noindex) Yii::app()->clientScript->registerMetaTag("noindex", 'robots');  ?>
<?php
    Yii::app()->clientScript->registerScript('testdrive_marks',
    "
    $('button.marks-switch').click(function(event) {
        var hBlock = $('.marks_group_menu');
        $(this).text($('.marks_group_menu.all_marks').is(':visible') ? 'Показать все марки' : 'Показать только популярные');
        hBlock.toggle();
    });
    ");
?>
<div class="inner_block testdrive_list">
    <h1>Отзывы</h1>
    <?php echo Yii::app()->controller->renderPartial('application.views.reklama.topRekBlock'); ?>
    <div class="testdrive_block">
        <?php if (!empty($marks)): ?>
            <div class="marks_group_menu popular_marks">
                <?php foreach($menuPopulargroups as $mg):?>
                    <?php $this->widget('zii.widgets.CMenu', array('items'=>$mg,'htmlOptions'=>array('class'=>'group_cat'))); ?>
                <?php endforeach;?>
            </div>        
            <div class="marks_group_menu all_marks">
                <?php foreach($menuGroups as $mg):?>
                    <?php $this->widget('zii.widgets.CMenu', array('items'=>$mg,'htmlOptions'=>array('class'=>'group_cat'))); ?>
                <?php endforeach;?>
            </div>
            <div class="switch-button-block">
                <button class="marks-switch">Показать все марки</button>
            </div>
        <?php else: ?>
            Раздел в стадии наполения.
        <?php endif; ?>
    </div>
    <?php echo Yii::app()->controller->renderPartial('application.views.reklama.bottomRekBlock'); ?>
    <div class="text_block_bottom">
        <?php echo Yii::app()->controller->renderPartial('application.views.textBlocks.bottomTextBlockReviews'); ?>
    </div>
</div>

<?php $this->beginClip('sidebar'); ?>
<div class="usefull_goods_block">
    <h4>Может быть интересно</h4>
    <p class="direct_subheader">Реклама от Яндекс Директ</p>
</div>
<?php $this->endClip(); ?>