<?php

class DefaultController extends Controller
{
        private $_model;
        
        public function filters() {
            return array(
                'accessControl', // perform access control for CRUD operations
                'ajaxOnly + dynamicmodel'
            );
        }
        
        public function accessRules() {
            return array(
                array('allow', // allow all users to perform 'index' and 'view' actions
                    'actions' => array('view', 'index', 'modellist', 'list'),
                    'users' => array('*'),
                ),
                array('allow', // allow all users to perform 'index' and 'view' actions
                    'actions' => array(), //'create', 'upload', 'DelImage'
                    'roles' => array('@'),
                ),
                array('deny',
                    'users' => array('?'),
                ),
            );
        }
        
        public function actionIndex() {
            $marks = MyMark::model()->with(
                            array(
                                'reviews' => array(
                                    'select'=>'id'
                                )
                            )
                    )->findAll(
                    array(
                        'select' => 'mark_id, popular, mname, name',
                        'condition' => '`reviews`.`moderation`=1',
                        'order' => '`t`.`name` ASC',
                    )
            );
            $groups = MyReview::getIndexMenu($marks);
            $menuGroups = $groups['allMarks'];
            $menuPopulargroups = $groups['popularMarks'];
            $this->render('index', array(
                'marks' => $marks,
                'menuGroups' => $menuGroups,
                'menuPopulargroups' => $menuPopulargroups
            ));
        }
        
        public function actionModelList($mark) {
            $marks = MyMark::model()->with(
                        array(
                            'automodels' => array(
                                'with' => array(
                                    'reviews' => array(
                                        'select' => 'id',
                                    ),
                                ),
                                'select'=>'model_id, name, mname',
                            )
                        )
                    )->find(
                        array(
                            'condition' => '`reviews`.`moderation`=1 AND `t`.`mname`=:mark',
                            'params' => array(':mark' => $mark)
                        )
                    );
            if (empty($marks))
                throw new CHttpException(404, 'Запрашиваемая страница не существует.');
            
            $groups = MyReview::getIndexModelList($marks);

            $this->render('mmodel_list', array(
                'marks' => $marks,
                'menuGroups' => $groups,
            ));
        }        
        
        public function actionList() {
            $mmodel = Yii::app()->request->getParam('model');
            $markGet = Yii::app()->request->getParam('mark');
            if ($mmodel) {
                $reviews = MyReview::model()->with(
                        array(
                                'model'
                            )
                        )->findAll(
                        array(
                            'condition' => 'model.mname=:mmodel AND moderation=1',
                            'params' => array(':mmodel' => $mmodel),
                            'order' => 'created DESC'
                ));
                
                if (empty($reviews)){
                    throw new CHttpException(404, 'Запрашиваемая страница не существует.');
                }
                
                /* Вычисление средней оценки */
                $sum = 0;
                foreach($reviews as $review){
                    $sumRev = ($review->comfort+$review->face+$review->safety+$review->reliability+$review->driving)/5;
                    $sum = $sum + $sumRev;
                }
                $srOcenka = round($sum/count($reviews));                
                
                $mark = MyMark::model()->findByPk($reviews[0]->mark_id);

                if($markGet != $mark->mname){
                    throw new CHttpException(404, 'Запрашиваемая страница не существует.');
                }

                $gallerys = MyAvtoGallery::model()->with(
                    array(
                        'galleryModel'=>array(
                            'select' => 'model_id'
                        ),
                        'gallery'=>array(
                            'select' => 'id',
                            'with' => array(
                                'galleryPhotos'=>array(
                                    'select'=>'id, name'
                                )
                            )
                        )
                    )
                )->findAll(
                    array(
                        'condition' => 'galleryModel.mname=:mmodel AND razdel LIKE "%reply%" AND published=1',
                        'params' => array(':mmodel' => $mmodel)
                    )
                ); 
                
                /* Проверка существования тестдрайвов марки для вывода в меню */
                $testdrive = MyTestdrive::model()->count(
                    array(
                        'with'=>array(
                            'testdriveModel'=>array(
                                'select'=>'model_id',
                                'with'=>array(
                                    'mark'=>array(
                                        'select'=>'`mark`.`mark_id`',
                                        'condition'=>'`mark`.`mark_id`=:mark_id',
                                        'params' => array(':mark_id'=>$mark->mark_id),
                                    )
                                )
                            )
                        ),
                        'select'=>'id',
                        )
                    );
                $this->render('list', array(
                    'mark' => $mark,
                    'reviews' => $reviews,
                    'gallerys' => $gallerys,
                    'srOcenka' => $srOcenka,
                    'testdriveCount'=> ($testdrive) ? true : false
                ));
            }else {
                throw new CHttpException(404, 'Запрашиваемая страница не существует.');
            }
        }        
        
        public function actionDynamicModel() {
            $mark_id = Yii::app()->request->getParam('mark_id');
            $data = MyMmodel::model()->findAll(array('condition'=>'mark_id=:mark_id','order'=>'t.name', 'params'=>array(':mark_id'=>(int) $mark_id)));

            $data = CHtml::listData($data, 'model_id', 'name');
            echo CHtml::tag('option', array('value' => ''), CHtml::encode('-- Выберите модель --'), true);
            foreach ($data as $value => $name) {
                echo CHtml::tag('option', array('value' => $value), CHtml::encode($name), true);
            }
        }        

        public function actionView() {
            $review = $this->loadModel(Yii::app()->request->getParam('id'),Yii::app()->request->getParam('mark'),Yii::app()->request->getParam('model'));
            
            /* Проверка на владельца отзыва */
            $isEdit = false;
            if(!Yii::app()->user->isGuest && (Yii::app()->user->id == $review->user_id || Yii::app()->user->id=='admin')){
                $isEdit = true;
            }
            
            /* Проверка существования тестдрайвов модели для вывода в меню */
            $testdrive = MyMmodel::model()->find(
                    array(
                        'with'=>array(
                            'testdrives'=>array('select'=>'id')
                            ),
                        'condition'=>'t.model_id=:model_id',
                        'params' => array(':model_id'=>$review->model_id)
                        )
                    );
            $this->render('view', array(
                'model' => $review,
                'isEdit' => $isEdit,
                'testdriveCount'=> (count($testdrive->testdrives)) ? true : false
            ));
        }
        
        public function actionUpload() {
            Yii::import("ext.EAjaxUpload.qqFileUploader");

            // folder for uploaded files
            $folder = 'images/reviews/';
            @mkdir($folder);
            //array("jpg","jpeg","gif","exe","mov" and etc...
            $allowedExtensions = array("jpg", "jpeg", "gif", "png");
            // maximum file size in bytes
            //$sizeLimit = 2 * 1024 * 1024;
            $fileMaxSize['postSize'] = toBytes(ini_get('post_max_size'));
            $fileMaxSize['uploadSize'] = toBytes(ini_get('upload_max_filesize'));

            $sizeLimit = min($fileMaxSize);

            $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
            $result = $uploader->handleUpload($folder);

            $fileSize = filesize($folder . $result['filename']);
            $fileName = $result['filename'];

            // resize file
            if ($fileName) {
                @mkdir($folder . 'thumb');
                @mkdir($folder . 'big');
                $image = new Image($folder . $fileName);
                $image->centeredpreview(95, 95);
                $image->save($folder . 'thumb/' . $fileName);

                $image = new Image($folder . $fileName);
                $image->centeredpreview(331, 242);
                $image->save($folder . 'big/' . $fileName);
                // result echo
                $result['thumbPath'] = '/' . $folder . 'thumb/' . $fileName;
                $result['thumbWidth'] = 79;
                $result['newname'] = $fileName;
                $result['filename'] = '';
                $return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);

                echo $return;
            } 
        }
        
        public function actionDelImage() {
            $file = $_GET['file'];
            MyReview::model()->updateAll(array( 'main_image' => '' ), 'main_image = :main_image',array(':main_image'=>$file));
            findDelFile('images/reviews', $file);
        }
        
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
            $model=new MyReview;

            // Uncomment the following line if AJAX validation is needed
            $this->performAjaxValidation($model);

            if(isset($_POST['MyReview']))
            {
                    $model->attributes=$_POST['MyReview'];
                    if($model->save()){
                            $this->redirect(array('view','id'=>$model->id, 'mark'=>$model->mark->mname, 'model'=>$model->model->mname));
                    }
            }

            $this->render('create',array(
                    'model'=>$model,
            ));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                if (Yii::app()->user->id=='admin'){
                    $mark = Yii::app()->getRequest()->getQuery('mark');
                    $mmodel = Yii::app()->getRequest()->getQuery('model');
                    $model=$this->loadModel($id,$mark,$mmodel);
                }else{
                    $model=$this->loadModelProfile($id);
                }
                
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
                //print_r($_POST['MyReview']);die;
		if(isset($_POST['MyReview']))
		{
			$model->attributes=$_POST['MyReview'];
                        $model->moderation = 0;
			if($model->save())
                            $this->redirect(array('view','id'=>$model->id, 'mark'=>$model->mark->mname, 'model'=>$model->model->mname));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
        
        public function loadModel($id,$mark,$model) {
            if ($this->_model === null) {
                if (isset($id)) {
                    $criteria = new CDbCriteria;
                    $criteria->condition = '`id`=:id AND model.mname=:model AND mark.mname=:mark';
                    $criteria->with = array('model'=>array('select'=>'name,mname'), 'mark'=>array('select'=>'name,mname'));
                    $criteria->params = array(':id' => $id, ':model'=>$model, ':mark'=>$mark);
                    $this->_model = MyReview::model()->find($criteria);
                }
                if ($this->_model === null)
                    throw new CHttpException(404, 'Запрашиваемая страница не существует.');
            }
            return $this->_model;
        }
        
        public function loadModelProfile($id) {
            
            $user_id = Yii::app()->user->id;
            if ($this->_model === null && $user_id) {
                if (isset($id)) {
                    $criteria = new CDbCriteria;
                    $criteria->condition = '`id`=:id AND user_id=:user_id';
                    $criteria->with = array('model'=>array('select'=>'name,mname'), 'mark'=>array('select'=>'name,mname'));
                    $criteria->params = array(':id' => $id, ':user_id'=>$user_id);
                    $this->_model = MyReview::model()->find($criteria);
                }
                if ($this->_model === null)
                    throw new CHttpException(404, 'Запрашиваемая страница не существует.');
            }
            return $this->_model;
        }
	/**
	 * Performs the AJAX validation.
	 * @param MyReview $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='review-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}