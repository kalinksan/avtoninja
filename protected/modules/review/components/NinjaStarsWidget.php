<?php
class NinjaStarsWidget extends CWidget {
    public $countStars;
    public $countSelect;    
    public function run(){
        $this->render('review.views.widgets.NinjaStars', array(
            'countStars' => $this->countStars,
            'countSelect' => $this->countSelect,
        ));
    }
}
?>