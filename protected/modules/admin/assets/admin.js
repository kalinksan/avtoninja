jQuery.fn.live = function (types, data, fn){jQuery(this.context).on(types,this.selector,data,fn); return this;};
$( document ).ready(function() {
    $('#my-testdrive-form #uploadFile a.del-main-image').live('click',function(e){
        e.preventDefault();
        file = $(this).attr('data-delete');
        el = $(this);
        $.ajax({
                type: "GET",
                url: "/admin/MyTestdrive/DelMainImage",
                data: { file:file },
        }).done(function(html) {
                $("#MyTestdrive_main_image").val(""); 
                $(".upload-main-image .qq-upload-button").show();
                el.parent().parent().hide();
        });
    });
    
    $('#my-review-form #uploadFile a.del-main-image').live('click',function(e){
        e.preventDefault();
        file = $(this).attr('data-delete');
        el = $(this);
        $.ajax({
                type: "GET",
                url: "/admin/MyReview/DelMainImage",
                data: { file:file },
        }).done(function(html) {
                $("#MyTestdrive_main_image").val(""); 
                $(".upload-main-image .qq-upload-button").show();
                el.parent().parent().hide();
        });
    });
    
    if($("#MyObjects_user_phone").length){
        $("#MyObjects_user_phone").mask("+7 (999) 999-99-99");
    }
});