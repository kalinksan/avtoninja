<?php

class DefaultController extends Controller
{
        public $layout='/layouts/column2';
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
        
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
		    array('allow',
			'actions'=>array(),
			'users'=>array('admin','copywriter'),
		    ),	
		    array('deny',
			'actions'=>array(),
			'users'=>array('@'),
		    ),
		);
	}

	public function actionIndex()
	{
            $this->redirect(array('/admin/login/index'));
	}

        public function actionHelloAdmin()
	{
		$this->render('helloadmin');
	}
        
	public function actionFlush() {
		Yii::app()->cache->flush();
		$this->redirect(array('/admin/login/index'));
	}
        
}