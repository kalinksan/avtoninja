<?php

class MyReviewController extends Controller
{

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
        public function accessRules() {
            return array(
                array('allow', // allow all users to perform 'index' and 'view' actions
                    'actions' => array('index', 'view'),
                    'users' => array('*'),
                ),
                array('allow', // allow authenticated user to perform 'create' and 'update' actions
                    'actions' => array('create', 'update'),
                    'users' => array('@'),
                ),
                array('allow', // allow admin user to perform 'admin' and 'delete' actions
                    'actions' => array('admin', 'delete', 'upload', 'delmainimage'),
                    'users' => array('admin'),
                ),
                array('deny', // deny all users
                    'users' => array('*'),
                ),
            );
        }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id,$mark_id,$model_id)
	{     
            $mark = MyMark::model()->findByPk($mark_id);
            $model = MyMmodel::model()->findByPk($model_id);
            $this->redirect( Yii::app()->createUrl('review/default/view',array('id' => $id, 'mark'=>$mark->mname, 'model'=>$model->mname)) );
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new MyReview;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['MyReview']))
		{
			$model->attributes=$_POST['MyReview'];
			if($model->save())
				$this->redirect(array('update','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
        
        public function actionUpload() {
            Yii::import("ext.EAjaxUpload.qqFileUploader");

            // folder for uploaded files
            $folder = 'images/reviews/';
            @mkdir($folder);
            //array("jpg","jpeg","gif","exe","mov" and etc...
            $allowedExtensions = array("jpg", "jpeg", "gif", "png");
            // maximum file size in bytes
            //$sizeLimit = 2 * 1024 * 1024;
            $fileMaxSize['postSize'] = toBytes(ini_get('post_max_size'));
            $fileMaxSize['uploadSize'] = toBytes(ini_get('upload_max_filesize'));

            $sizeLimit = min($fileMaxSize);

            $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
            $result = $uploader->handleUpload($folder);

            $fileSize = filesize($folder . $result['filename']);
            $fileName = $result['filename'];

            // resize file
            if ($fileName) {
                @mkdir($folder . 'thumb');
                @mkdir($folder . 'big');
                $image = new Image($folder . $fileName);
                $image->centeredpreview(95, 95);
                $image->save($folder . 'thumb/' . $fileName);

                $image = new Image($folder . $fileName);
                $image->centeredpreview(331, 242);
                $image->save($folder . 'big/' . $fileName);
                // result echo
                $result['thumbPath'] = '/' . $folder . 'thumb/' . $fileName;
                $result['thumbWidth'] = 79;
                $result['newname'] = $fileName;
                $result['filename'] = '';
                $return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);

                echo $return;
            } 
        }        

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['MyReview']))
		{
			$model->attributes=$_POST['MyReview'];
			if($model->save())
				$this->redirect(array('update','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
        
        public function actionDelMainImage($file){  
            MyReview::model()->updateAll(array( 'main_image' => '' ), 'main_image = :main_image',array(':main_image'=>$file));
            findDelFile('images/reviews',$file);
        }
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('MyReview');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new MyReview('search');
                $model->unsetAttributes();
		$mark = new MyMark('search');
                $mark->unsetAttributes();
 		$mmodel = new MyMmodel('search');
                $mmodel->unsetAttributes();               
		if(isset($_GET['MyReview'])){
			$model->attributes=$_GET['MyReview'];
                }
		if(isset($_GET['MyMark'])){
			$mark->attributes=$_GET['MyMark'];
                }   
		if(isset($_GET['MyMmodel'])){
			$mmodel->attributes=$_GET['MyMmodel'];
                }                  
                $model->searchMark = $mark;
                $model->searchModel = $mmodel;
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=MyReview::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='my-review-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
