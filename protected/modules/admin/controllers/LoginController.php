<?php

class LoginController extends Controller
{
	public $layout='/layouts/login_l';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(),
				'users'=>array('@'),
			),
			array('allow',  // deny all users
				'actions'=>array('index'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}	
	
	public function actionIndex(){ 
		if(!Yii::app()->user->isGuest) {
		    $this->goToAdminPanel();
		} else {
		    $this->actionLogin();
		}
	}

	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->goToAdminPanel();
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
	    Yii::app()->user->logout();
	    $this->redirect(Yii::app()->homeUrl);
    }

    /**
     * Переправляет в админку
     */
    protected function goToAdminPanel($module = '')
    {
        if($module){
            $this->redirect(Yii::app()->request->hostInfo.'/admin/'.$module.'/admin');
        }else{
            $this->redirect(Yii::app()->request->hostInfo.'/admin/default/helloadmin');
        }
    }
}