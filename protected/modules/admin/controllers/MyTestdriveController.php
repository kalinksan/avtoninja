<?php

class MyTestdriveController extends Controller {

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'upload', 'delmainimage'),
                'users' => array('admin','copywriter'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view',array(
                'model'=>$this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        
        $model = new MyTestdrive;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        
        if (isset($_POST['MyTestdrive'])) {
            $model->attributes = $_POST['MyTestdrive'];
            
            if ($model->save()){
                $model->saveRelated('testdriveModel');
                $this->redirect(array('update', 'id' => $model->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['MyTestdrive'])) {
            $model->attributes = $_POST['MyTestdrive'];
            if ($model->save()){
                $model->saveRelated('testdriveModel');
                $this->redirect(array('update', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionUpload() {
        Yii::import("ext.EAjaxUpload.qqFileUploader");
        // folder for uploaded files
        $folder = 'images/testdrive/';
        //array("jpg","jpeg","gif","exe","mov" and etc...
        $allowedExtensions = array("jpg", "jpeg", "gif", "png");
        // maximum file size in bytes
        //$sizeLimit = 2 * 1024 * 1024;
        $fileMaxSize['postSize'] = toBytes(ini_get('post_max_size'));
        $fileMaxSize['uploadSize'] = toBytes(ini_get('upload_max_filesize'));

        $sizeLimit = min($fileMaxSize);

        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload($folder);

        $fileSize = filesize($folder . $result['filename']);
        $fileName = $result['filename'];

        $image = new Image($folder . $fileName);
        $image->resize(331, 242, Image::WIDTH);
        $image->quality(100);
        $image->save($folder . 'big/' . $fileName);

        // create thumb
        $image->centeredpreview(95, 95);
        $image->save($folder . 'thumb/' . $fileName);

        // result echo
        $result['thumbPath'] = '/' . $folder . 'thumb/' . $fileName;
        $result['thumbWidth'] = 95;
        $result['newname'] = $fileName;
        $fileArr = explode(".", $fileName);
        $result['class'] = $fileArr[0];
        $return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        echo $return;
    }
    
    public function actionDelMainImage($file){  
        MyTestdrive::model()->updateAll(array( 'main_image' => '' ), 'main_image = :main_image',array(':main_image'=>$file));
        findDelFile('images/testdrive',$file);
    }
    
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('MyTestdrive');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new MyTestdrive('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['MyTestdrive']))
            $model->attributes = $_GET['MyTestdrive'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = MyTestdrive::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'my-testdrive-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
