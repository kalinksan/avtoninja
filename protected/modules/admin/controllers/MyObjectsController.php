<?php

class MyObjectsController extends Controller
{
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
                $this->redirect( Yii::app()->createUrl('objects/default/view',array('id'=>$id)) );
	}
        
        public function actionCreate() {
            $model = new MyObjects('turboexp');
            $model->city_id = Yii::app()->request->cookies['city_id']->value;
            // Uncomment the following line if AJAX validation is needed
            $this->performAjaxValidation(array($model));
            if (isset($_POST['MyObjects'])) {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $model->attributes = $_POST['MyObjects'];
                    $model->user_id = Yii::app()->params['adminBaseId'];
                    if($_POST['MyObjects']['state']){
                        $objModule = Yii::app()->getModule('objects');
                        $checkListGroup = $objModule->params['check_list_group'];
                        $checkData = array();
                        foreach($checkListGroup as $grId=>$grName){
                            $paramListArr = (array)$_POST['checkList_'.$grId];
                            $paramDescrArr = (array)$_POST['checkListComment_'.$grId];
                            foreach($paramListArr as $checkParamId=>$checkParamValue){
                                $checkData[$grId][$checkParamId]['value'] = $checkParamValue;
                            }
                            foreach($paramDescrArr as $checkParamId=>$checkParamDescr){
                                $checkData[$grId][$checkParamId]['description'] = $checkParamDescr;
                            }
                        }
                        $checkData = urlencode(serialize($checkData));
                        $model->check_data = $checkData;  
                    }
                    if ($model->validate()) {
                        if ($model->save(false)) {
                            if (!empty($_POST['images'])) {
                                foreach ($_POST['images'] as $image) {
                                    $images = new ObjectsImages();
                                    $images->image = $image;
                                    $images->entity_id = $model->id;
                                    $images->save();
                                }
                            }
                            // Yii::app()->user->setFlash('successMessage',"Объявление успешно добавлено.");
                            $transaction->commit();
                        } else {
                            $transaction->rollback();
                        }
                        $this->redirect(array('update', 'id' => $model->id));
                    }
                } catch (Exception $e) {
                    print($e->getMessage());
                    $transaction->rollback();
                    exit();
                }
            }

            $this->render('create', array(
                'model' => $model,
            ));
        }
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
            $model = $this->loadModel($id);
            $model->scenario = 'turboexp';
            // Uncomment the following line if AJAX validation is needed
            $this->performAjaxValidation(array($model));
            if (isset($_POST['MyObjects'])) {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $model->attributes = $_POST['MyObjects'];
                    if($_POST['MyObjects']['state']){
                        //print_r($_POST);die;
                        $objModule = Yii::app()->getModule('objects');
                        $checkListGroup = $objModule->params['check_list_group'];
                        $checkData = array();
                        foreach($checkListGroup as $grId=>$grName){
                            $paramListArr = (array)$_POST['checkList_'.$grId];
                            $paramDescrArr = (array)$_POST['checkListComment_'.$grId];
                            foreach($paramListArr as $checkParamId=>$checkParamValue){
                                $checkData[$grId][$checkParamId]['value'] = $checkParamValue;
                            }
                            foreach($paramDescrArr as $checkParamId=>$checkParamDescr){
                                $checkData[$grId][$checkParamId]['description'] = $checkParamDescr;
                            }
                        }
                        $checkData = urlencode(serialize($checkData));
                        $model->check_data = $checkData;  
                    }
                    
                    if ($model->validate()) {
                        if ($model->save(false)) {
                            if (!empty($_POST['images'])) {
                                foreach ($_POST['images'] as $image) {
                                    $images = new ObjectsImages();
                                    $images->image = $image;
                                    $images->entity_id = $model->id;
                                    $images->save();
                                }
                            }
                            //Yii::app()->user->setFlash('successMessage_advert',"Ваше объявление обновлено.");
                            $transaction->commit();
                        }
                        $this->redirect(array('update', 'id' => $model->id));
                    }
                } catch (Exception $e) {
                    print($e->getMessage());
                    $transaction->rollback();
                    exit();
                }
            }

            $this->render('update', array(
                'model' => $model,
            ));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('MyObjects');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{       
		$model=new MyObjects('search');
                $model->unsetAttributes();
		$mark = new MyMark('search');
                $mark->unsetAttributes();
 		$mmodel = new MyMmodel('search');
                $mmodel->unsetAttributes();  
 		$city = new MyCity('search');
                $city->unsetAttributes();                 
		if(isset($_GET['MyObjects'])){
			$model->attributes=$_GET['MyObjects'];
                }
		if(isset($_GET['MyMark'])){
			$mark->attributes=$_GET['MyMark'];
                }   
		if(isset($_GET['MyMmodel'])){
			$mmodel->attributes=$_GET['MyMmodel'];
                }    
		if(isset($_GET['MyCity'])){
			$city->attributes=$_GET['MyCity'];
                }                
                $model->searchMark = $mark;
                $model->searchModel = $mmodel;
                $model->searchCity = $city;
		$this->render('admin',array(
			'model'=>$model,
		));                
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=MyObjects::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='my-objects-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
