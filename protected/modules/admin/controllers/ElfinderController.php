<?php
class ElfinderController extends CController
{
	public function actions()
	{
		return array(
			'connector' => array(
				'class' => 'ext.elFinder.ElFinderConnectorAction',
				'settings' => array(
					'root' => Yii::getPathOfAlias('webroot') . '/images/from_tiny/',
					'URL' => Yii::app()->baseUrl . '/images/from_tiny/',
					'rootAlias' => 'Home',
					'mimeDetect' => 'none'
				)
			),
		);
	}
}
?>