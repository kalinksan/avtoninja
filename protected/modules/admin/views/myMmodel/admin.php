<?php
//print_r(MyMmodel::MarkChoices());die;
$this->menu=array(
	array('label'=>'+ Добавить модель','url'=>array('create')),
);

?>

<h1>Модели автомобилей</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'my-mmodel-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
            array(
                    'name'=>'mark_id',
                    'value'=>'$data->mark->name',
                    'filter'=>  MyMmodel::MarkChoices(),
            ),
            'name',
            array(
                    'class'=>'bootstrap.widgets.TbButtonColumn',
            ),
	),
)); ?>
