<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('model_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->model_id),array('view','id'=>$data->model_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mark_id')); ?>:</b>
	<?php echo CHtml::encode($data->mark_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mname')); ?>:</b>
	<?php echo CHtml::encode($data->mname); ?>
	<br />


</div>