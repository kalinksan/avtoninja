<?php
$this->menu=array(
	array('label'=>'Управление моделями','url'=>array('admin')),
);
?>

<h1>Добавление новой модели</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>