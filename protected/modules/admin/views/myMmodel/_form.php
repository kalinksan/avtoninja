<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'my-mmodel-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>
        <div class="row">
            <?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>255,'readonly'=>(!$model->isNewRecord && Yii::app()->user->id != 'admin') ? true : false)); ?>
            
            <?php echo $form->dropDownListRow($model,'mark_id', MyMmodel::MarkChoices(),$options); ?>
            <?php echo $form->error($model,'mark_id'); ?>
        </div>
        <div class="row">
            <?php echo $form->textFieldRow($model,'synonym',array('class'=>'span5','maxlength'=>255)); ?>
        </div>
        <?php echo $form->hiddenField($model,'mname',array('class'=>'span5','maxlength'=>255)); ?>
        <div class="row">           
            <?php echo $form->labelEx($model, 'text_block'); ?>
            <?php 
            $this->widget('ext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
                'model' => $model,
                'attribute' => 'text_block',
                'options' => array(
                    'lang' => 'ru',
                    'maxHeight' => 800,
                    'imageUpload' => Yii::app()->createUrl('/admin/RedactorFile/upload'),
                    'fileUpload' => Yii::app()->createUrl('/admin/RedactorFile/uploadFile')
                ),
                'plugins' => array(
                    'fullscreen' => array(
                        'js' => array('fullscreen.js', ),
                    ),
                    'filemanager'=> array(
                        'js' => array('filemanager.js', ),
                    ),
                    'imagemanager'=> array(
                        'js' => array('imagemanager.js', ),
                    ),
                    'table'=> array(
                        'js' => array('table.js', ),
                    ),
                    'video'=> array(
                        'js' => array('video.js', ),
                    ),
                ),

            ));
            ?>
            <?php echo $form->error($model, 'text_block'); ?>
	</div>

        <div class="row">
            <?php echo $form->textAreaRow($model,'testdrive_metadesc',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>
        </div>

        <div class="row">           
            <?php echo $form->labelEx($model, 'text_block_review'); ?>
            <?php 
            $this->widget('ext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
                'model' => $model,
                'attribute' => 'text_block_review',
                'options' => array(
                    'lang' => 'ru',
                    'maxHeight' => 800,
                    'imageUpload' => Yii::app()->createUrl('/admin/RedactorFile/upload'),
                    'fileUpload' => Yii::app()->createUrl('/admin/RedactorFile/uploadFile')
                ),
                'plugins' => array(
                    'fullscreen' => array(
                        'js' => array('fullscreen.js', ),
                    ),
                    'filemanager'=> array(
                        'js' => array('filemanager.js', ),
                    ),
                    'imagemanager'=> array(
                        'js' => array('imagemanager.js', ),
                    ),
                    'table'=> array(
                        'js' => array('table.js', ),
                    ),
                    'video'=> array(
                        'js' => array('video.js', ),
                    ),
                ),

            ));
            ?>
            <?php echo $form->error($model, 'text_block_review'); ?>
	</div>

        <div class="row">
            <?php echo $form->textAreaRow($model,'review_metadesc',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>
        </div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Сохранить' : 'Сохранить',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
