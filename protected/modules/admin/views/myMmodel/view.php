<?php
$this->breadcrumbs=array(
	'My Mmodels'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List MyMmodel','url'=>array('index')),
	array('label'=>'Create MyMmodel','url'=>array('create')),
	array('label'=>'Update MyMmodel','url'=>array('update','id'=>$model->model_id)),
	array('label'=>'Delete MyMmodel','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->model_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MyMmodel','url'=>array('admin')),
);
?>

<h1>View MyMmodel #<?php echo $model->model_id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'model_id',
		'mark_id',
		'name',
		'mname',
	),
)); ?>
