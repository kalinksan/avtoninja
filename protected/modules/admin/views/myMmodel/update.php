<?php
$this->menu=array(
	array('label'=>'Добавить новую модель','url'=>array('create')),
	array('label'=>'Управление моделями','url'=>array('admin')),
);
?>

<h1>Обновление модели <?php echo $model->model_id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>