<?php
$this->breadcrumbs=array(
	'My Mmodels',
);

$this->menu=array(
	array('label'=>'Create MyMmodel','url'=>array('create')),
	array('label'=>'Manage MyMmodel','url'=>array('admin')),
);
?>

<h1>My Mmodels</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
