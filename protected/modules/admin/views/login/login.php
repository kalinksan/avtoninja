<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */
?>

<div class="span3 offset4 well">
	<legend>Вход в панель управления</legend>
	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'login-form',
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
    )); ?>
    <div>
        <?php echo $form->textFieldRow($model,'username', array('class'=>'span3')); ?>
    </div>

    <div>
        <?php echo $form->passwordFieldRow($model,'password', array('class'=>'span3')); ?>
    </div>

    <div class="rememberMe">
        <?php echo $form->checkBoxRow($model,'rememberMe'); ?>
    </div>
    <div class="clearfix" />
    <div class="buttons">
        <?php echo CHtml::submitButton('Войти', array('class' => 'pull-right btn btn-inverse')); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->