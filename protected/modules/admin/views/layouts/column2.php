<?php /* @var $this Controller */ ?>
<?php //$this->beginContent('/layouts/main'); ?>
<?php $this->beginContent('application.modules.admin.views.layouts.main'); ?>

<div class="span3">
	<div class="well sidebar-nav">
		<?php $this->widget('bootstrap.widgets.TbMenu', array(
			'type'=>'list',
			'encodeLabel' => false,
			'items'=>array(
                            array('label'=>'Контент'),
                            array('label'=>'Объявления', 'icon'=>'pencil', 'url'=>'/admin/myObjects/admin', 'visible'=>Yii::app()->user->name == 'admin', 'active'=>Yii::app()->controller->id=='myObjects'),
                            array('label'=>'Тестдрайвы', 'icon'=>'pencil', 'url'=>'/admin/myTestdrive/admin', 'active'=>Yii::app()->controller->id=='myTestdrive'),
                            array('label'=>'Отзывы', 'icon'=>'pencil', 'url'=>'/admin/myReview/admin', 'visible'=>Yii::app()->user->name == 'admin', 'active'=>Yii::app()->controller->id=='myReview'),
                            array('label'=>'Архивные материалы', 'icon'=>'pencil', 'url'=>'/admin/myArchive/admin', 'visible'=>Yii::app()->user->name == 'admin', 'active'=>Yii::app()->controller->id=='myArchive'),
                            array('label'=>'Галереи', 'icon'=>'picture', 'url'=>'/admin/MyAvtoGallery/admin', 'active'=>Yii::app()->controller->id=='myAvtoGallery'),
                            array('label'=>'Списки'),
                            array('label'=>'Марки автомобилей', 'icon'=>'list', 'url'=>'/admin/myMark/admin', 'active'=>Yii::app()->controller->id=='myMark'),		
                            array('label'=>'Модели автомобилей', 'icon'=>'list', 'url'=>'/admin/myMmodel/admin', 'active'=>Yii::app()->controller->id=='myMmodel'),		
                            		
                        ),
		)); ?>
	</div><!-- sidebar -->
</div>
<div class="span9">
	<div id="content">
            <div class="row">
            <?php /*$this->widget('zii.widgets.CBreadcrumbs', array(
                'homeLink'=>CHtml::link('Главная','/admin'),
                'separator'=>' &rarr; ',
                'links'=>$this->breadcrumbs,
            )); */?>
            </div>
            <?php  $this->widget('bootstrap.widgets.TbMenu', array(
                'type'=>'pills',
                'stacked'=>false,
                'items'=>$this->menu,
                'htmlOptions'=>array('class'=>'pull-right')
            ));  ?>
            <?php echo $content; ?>
	</div><!-- content -->
</div>
</div>
<?php $this->endContent(); ?>