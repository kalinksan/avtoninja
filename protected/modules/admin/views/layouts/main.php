<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="ru">
  <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="shortcut icon" href="<?php echo $baseUrl; ?>/images/fav_admin.ico" type="image/x-icon" />
	<meta name="language" content="ru" />
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<?php Yii::app()->bootstrap->register(); ?>	
	<?php
		Yii::app()->clientScript->registerCoreScript('jquery');
		Yii::app()->getClientScript()->registerCoreScript('jquery.ui');
		Yii::app()->clientScript->registerScriptFile($this->module->assetsUrl.'/jquery.browser.min.js');
		Yii::app()->clientScript->registerScriptFile($this->module->assetsUrl.'/admin.js');
		Yii::app()->clientScript->registerCssFile($this->module->assetsUrl.'/admin-styles.css');
	?>
        <?php Yii::app()->clientScript->registerScriptFile($this->module->assetsUrl.'/jquery.maskedinput.min.js');?>
</head>

<body id="top">
			<div id="messages"><?php YII_DEBUG ? $this->widget('application.extensions.email.debug') : ''; ?></div>
			<?php  $this->widget('bootstrap.widgets.TbNavbar', array(
			//'type'=>'inverse', // null or 'inverse'
			'brand'=>'Панель управления «'.CHtml::encode(Yii::app()->name).'»',
			'brandUrl'=>'#',
			'fixed' => 'top',
			'collapse'=>false, // requires bootstrap-responsive.css
			'items'=>array(
				array(
					'class'=>'bootstrap.widgets.TbMenu',
					'htmlOptions'=>array('class'=>'pull-right'),
					'items'=>array(
						array('label'=>'Очистить кэш', 'url'=>array('/admin/default/flush'), 'visible'=>!Yii::app()->user->checkAccess('Photograph')),
						array('label'=>'Просмотр сайта', 'url'=>Yii::app()->homeUrl),
						array('label'=>'Выход('.Yii::app()->user->name.')', 'url'=>array('/admin/login/logout')),
					),
				),
			),
		));  ?>
	<div class="bootnavbar-delimiter"></div>
	<div class="container-fluid">
		<div class="row-fluid">	
			<?php echo $content; ?>
		</div>
	</div>
</body>
</html>
