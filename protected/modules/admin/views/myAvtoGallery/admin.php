<?php
$this->breadcrumbs=array(
	'My Avto Galleries'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'+ Добавить галерею','url'=>array('create')),
);

?>

<h1>Управление галереями</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'my-avto-gallery-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                array(
                    'name'=>'galleryModel',
                    'value'=>'$data->relModels',
                    'filter'=>CHtml::activeTextField($model, 'relModelsStr'),
                ),
                array(
                    'name'=>'published',
                    'value'=>'$data->published==1 ? CHtml::encode("Да") : CHtml::encode("Нет")',
                    'filter'=>CHtml::activeDropDownList($model, 'published', array(''=>'', 1=>'Да', 0=>'Нет')),
                ),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
