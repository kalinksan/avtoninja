<?php 
Yii::import('ext.chosen.Chosen');
$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'my-avto-gallery-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

        <div class="row">
            <?php echo $form->labelEx($model, 'gallery_id'); ?>
            <?php
            if ($model->galleryBehavior->getGallery() === null) {
                echo '<p>Необходимо сохранить галлерею перед добавлением картинок</p>';
            } else {
                $this->widget('ext.galleryManager.GalleryManager', array(
                    'gallery' => $model->galleryBehavior->getGallery(),
                    'controllerRoute' => '/admin/gallery', //route to gallery controller
                ));
            }
            ?>        
        </div>
        <div class="row">
            <?php echo $form->labelEx($model, 'galleryModel'); ?>
            <?php echo Chosen::activeMultiSelect($model, 'galleryModel', MyMmodel::modelAdminChoices(), array('class'=>'span8'));?>
            <?php echo $form->error($model, 'galleryModel'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model, 'razdel'); ?>
            <?php echo $form->checkboxList($model,'razdel',array('testdrive'=>'тест-драйвы','reply'=>'отзывы')); ?>
        </div>
        <div class="row">
            <?php echo $form->checkboxRow($model,'published'); ?>
        </div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Сохранить' : 'Сохранить',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
