<?php
$this->breadcrumbs=array(
	'My Avto Galleries'=>array('index'),
	$model->gallery_id=>array('view','id'=>$model->gallery_id),
	'Update',
);

$this->menu=array(
	array('label'=>'Добавить галерею','url'=>array('create')),
	array('label'=>'Управление галереями','url'=>array('admin')),
);
?>

<h1>Обновление галлереи <?php echo $model->gallery_id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>