<?php
$this->breadcrumbs=array(
	'My Avto Galleries'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Управление галереями','url'=>array('admin')),
);
?>

<h1>Создание галереи</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>