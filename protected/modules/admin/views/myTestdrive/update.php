<?php
$this->breadcrumbs=array(
	'My Testdrives'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	//array('label'=>'Посмотреть на сайте','url'=>array('view','id'=>$model->id)),
	array('label'=>'Добавить тестдрайв','url'=>array('create')),
	array('label'=>'Список тестдрайвов','url'=>array('admin')),
);
?>

<h1>Обновление <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>