<?php
$this->breadcrumbs=array(
	'My Testdrives'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'Create MyTestdrive','url'=>array('create')),
	array('label'=>'Update MyTestdrive','url'=>array('update','id'=>$model->id)),
	array('label'=>'Manage MyTestdrive','url'=>array('admin')),
);
?>

<h1>View MyTestdrive #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'type',
                'link',
	),
)); ?>
