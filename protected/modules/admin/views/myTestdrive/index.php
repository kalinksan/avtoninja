<?php
$this->breadcrumbs=array(
	'My Testdrives',
);

$this->menu=array(
	array('label'=>'Create MyTestdrive','url'=>array('create')),
	array('label'=>'Manage MyTestdrive','url'=>array('admin')),
);
?>

<h1>My Testdrives</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
