<?php
$this->breadcrumbs=array(
	'My Testdrives'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Список тестдрайвов','url'=>array('admin')),
);
?>

<h1>Добавить тестдрайв</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>