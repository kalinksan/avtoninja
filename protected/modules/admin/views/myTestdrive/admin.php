<?php

$this->menu=array(
	array('label'=>'Добавить тестдрайв','url'=>array('create')),
);

?>

<h1>Управление тестдрайвами</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'my-testdrive-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'title',
                array(
                    'name'=>'published',
                    'value'=>'$data->published==1 ? CHtml::encode("Да") : CHtml::encode("Нет")',
                    'filter'=>CHtml::activeDropDownList($model, 'published', array(''=>'', 1=>'Да', 0=>'Нет')),
                ),
		array(
                    'class'=>'bootstrap.widgets.TbButtonColumn',
                    'viewButtonUrl'=>'Yii::app()->createUrl("/admin/MyTestdrive/view", array("id"=>$data->id))',
		),
	),
)); ?>
