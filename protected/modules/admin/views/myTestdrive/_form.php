<?php 
Yii::import('ext.chosen.Chosen');

Yii::app()->clientScript->registerScript('hidden_type',
"
    if ($('#MyTestdrive_type_1').prop('checked')) {
        $('#MyTestdrive_link').parent('.row').hide();
    }
    $('input[name=\"MyTestdrive[type]\"]:radio').change(function() {
        if ($('#MyTestdrive_type_1').prop('checked')) {
            $('#MyTestdrive_link').parent('.row').fadeOut();
        }else{
            $('#MyTestdrive_link').parent('.row').fadeIn();
        }
    });
");

$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'my-testdrive-form',
	'enableAjaxValidation'=>false,
    	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
	'clientOptions' => array(
		 'validateOnSubmit' => 'true',
	),
)); ?>

	<?php echo $form->errorSummary($model); ?>

        
	<div class="row"><?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>255)); ?></div>

        <div class="row">
            <?php echo $form->labelEx($model, 'testdriveModel'); ?>
            <?php echo Chosen::activeMultiSelect($model, 'testdriveModel', MyMmodel::modelAdminChoices(), array('class'=>'span8'));?>
            <?php echo $form->error($model, 'testdriveModel'); ?>
        </div>
        
        <div class="row"><?php echo $form->radioButtonListRow($model, 'type', $model->types); ?></div>
        <div class="row"><?php echo $form->textFieldRow($model,'link',array('class'=>'span5','maxlength'=>255)); ?></div>
        
        <div class="row">           
            <?php echo $form->labelEx($model, 'introtext'); ?>
            <?php 
            $this->widget('ext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
                'model' => $model,
                'attribute' => 'introtext',
                'options' => array(
                    'lang' => 'ru',
                    'maxHeight' => 800,
                    'imageUpload' => Yii::app()->createUrl('/admin/RedactorFile/upload'),
                    'fileUpload' => Yii::app()->createUrl('/admin/RedactorFile/uploadFile')
                ),
                'plugins' => array(
                    'fullscreen' => array(
                        'js' => array('fullscreen.js', ),
                    ),
                    'filemanager'=> array(
                        'js' => array('filemanager.js', ),
                    ),
                    'imagemanager'=> array(
                        'js' => array('imagemanager.js', ),
                    ),
                    'table'=> array(
                        'js' => array('table.js', ),
                    ),
                    'video'=> array(
                        'js' => array('video.js', ),
                    ),
                ),

            ));
            ?>
            <?php echo $form->error($model, 'introtext'); ?>
	</div>
	
        <div class="row"><?php echo $form->textFieldRow($model,'author',array('class'=>'span5','maxlength'=>100)); ?></div>
        
        <div class="row">
            <?php echo $form->labelEx($model, 'main_image'); ?>
            <?php echo $form->hiddenField($model,'main_image',array('size'=>60,'maxlength'=>255)); ?>
            <div class="upload-main-image" <?php print (empty($model->main_image) || $model->main_image=="NULL") ? '' : 'style="display:none"' ?>>		
                    <?php
                    $this->widget('ext.EAjaxUpload.EAjaxUpload', array(
                            'id' => 'uploadFile',
                            'multiple'=> true,
                            'config' => array(
                                    'request' => array(
                                            'endpoint' => '/admin/MyTestdrive/upload',
                                    ),
                                    'validation' => array(
                                            'allowedExtensions' => array('jpg', 'jpeg', 'png', 'bmp','gif'),
                                            'sizeLimit' => 5048576, // max file size in bytes
                                       // 'minSizeLimit' => 256, // min file size in bytes
                                    ),
                                    'callbacks' => array(
                                            'onComplete' => 'js:function(id, fileName, responseJSON){$("#MyTestdrive_main_image").val(responseJSON.filename);$("#uploadFile ."+responseJSON.class+" .upload-delete-file").html("<a class=del-main-image href=# data-delete="+responseJSON.filename+">Удалить</a>"); $(".upload-main-image .qq-upload-button").hide();}',
                                    ),
                            )
                    ));	

                    ?>
            </div>

            <?php if(!empty($model->main_image) && ($model->main_image!="NULL")):?>
                    <div class="update-main-image well">
                            <?php
                            echo CHtml::image(Yii::app()->baseUrl.'/images/testdrive/thumb/'.$model->main_image);
                            $this->widget('bootstrap.widgets.TbButton', array(
                                    'label' => 'удалить',
                                    'type' => 'danger',
                                    'htmlOptions' => array(
                                            'ajax' => array(
                                                    'type' => 'POST',
                                                    'url' => $this->createUrl('/admin/MyTestdrive/DelMainImage',array('file' => $model->main_image)),
                                                    'success' => 'function(data) {  $("#MyTestdrive_main_image").val(""); $(".upload-main-image").show();$(".update-main-image").hide();}',
                                            ),
                                            'id'=>'main_image'
                                    ),
                            ));?>
                    </div>
            <?php endif;?>
	</div>

        <div class="row">
            <?php echo $form->checkboxRow($model,'published'); ?>
        </div>
        <div class="row buttons">
		<?php 	
			$buttons[]=array(
					'buttonType'=>'submit',
					'type'=>'primary',
					'label'=>'Сохранить',
					'htmlOptions'=>array('name'=>'_save')
			);
			if(Yii::app()->controller->action->id == 'update'){
				$buttons[] = array(
						'buttonType'=>'link',
						'type'=>'danger',
						'url'=>'javascript:;',
						'label'=>'Удалить',
						'htmlOptions'=>array(
							'submit'=>array(
								'delete',
								'id'=>$model->id,
							),
							'confirm'=>'Вы действительно хотите удалить эту запись?',
						)
				);
			}
		?>
		<div class="form-actions">
			<?php 
				$this->widget('bootstrap.widgets.TbButtonGroup',array(
					'type'=>'',
					'buttons'=>$buttons,
				)); 
			?>
		</div>
	</div>

<?php $this->endWidget(); ?>
