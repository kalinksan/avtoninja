<?php
$this->breadcrumbs=array(
	'My Objects'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MyObjects','url'=>array('index')),
	array('label'=>'Create MyObjects','url'=>array('create')),
	array('label'=>'Update MyObjects','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete MyObjects','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MyObjects','url'=>array('admin')),
);
?>

<h1>View MyObjects #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'mark_id',
		'model_id',
		'user_id',
		'user_name',
		'user_email',
		'user_phone',
		'city_id',
		'metro',
		'year',
		'cost',
		'km_age',
		'body_type',
		'color',
		'volume',
		'transmission',
		'engine',
		'privod',
		'wheel',
		'state',
		'description',
		'created',
		'updated',
		'turbodealer_id',
		'service',
		'published',
		'object_status',
	),
)); ?>
