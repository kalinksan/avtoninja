<?php

$this->menu=array(
	array('label'=>'+ Добавить объявление','url'=>array('create')),
);

?>

<h1>Управление объявлениями</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'my-objects-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
                array(
                        'name'=>'mark.name',
                        'filter'=>CHtml::activeTextField($model->searchMark, 'name'),
                ),
                array(
                        'name'=>'model.name',
                        'filter'=>CHtml::activeTextField($model->searchModel, 'name'),
                ),
                array(
                        'name'=>'city.city_name',
                        'filter'=>CHtml::activeTextField($model->searchCity, 'city_name'),
                ),
                array(
                    'name'=>'state',
                    'value'=>'$data->state==1 ? CHtml::encode("Да") : CHtml::encode("Нет")',
                    'filter'=>CHtml::activeDropDownList($model, 'state', array(''=>'', 1=>'Да', 0=>'Нет')),
                ),
                array(
                    'name'=>'published',
                    'value'=>'$data->published==1 ? CHtml::encode("Да") : CHtml::encode("Нет")',
                    'filter'=>CHtml::activeDropDownList($model, 'published', array(''=>'', 1=>'Да', 0=>'Нет')),
                ),
		/*
		'user_phone',
		'metro',
		'year',
		'cost',
		'km_age',
		'body_type',
		'color',
		'volume',
		'transmission',
		'engine',
		'privod',
		'wheel',
		'state',
		'description',
		'created',
		'updated',
		'turbodealer_id',
		'service',
		'object_status',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
