<?php
    Yii::app()->clientScript->registerScript('del_review_img',
    "
    $('a.del-image').live('click',function(e){
        e.preventDefault();
        if(confirm('Вы действительно хотите удалить картинку?')){
                file = $(this).attr('data-delete');
                el = $(this);
                $.ajax({
                        type: 'GET',
                        url: '/objects/DelImage',
                        data: { file:file },
                }).done(function(html) {
                        el.parent().parent().fadeOut(300, function() {
                                $(this).remove();
                                if(!$('.qq-upload-list.loaded li').length) {
                                        $('.qq-upload-list.loaded').remove();
                                }
                        })
                        filecount = filecount - 1;
                });
        }
    });
    if (!$('#MyObjects_state').prop('checked')) {
        $('.checkListBlock').hide();
    }
    $('input#MyObjects_state').change(function() {
        if (!$('#MyObjects_state').prop('checked')) {
            $('.checkListBlock').fadeOut();
        }else{
            $('.checkListBlock').fadeIn();
        }
    });
    ");
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'my-objects-form',
	'enableAjaxValidation'=>true,
    	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
	'clientOptions' => array(
		 'validateOnSubmit' => 'true',
	),
)); ?>
	<?php echo $form->errorSummary($model); ?>
        <div class="row span4">
            <div class="row">
                <?php echo $form->textFieldRow($model,'user_name',array('size'=>60,'maxlength'=>255)); ?>
            </div>
            <div class="row">
                <?php echo $form->textFieldRow($model,'user_email',array('size'=>60,'maxlength'=>50)); ?>
            </div>
            <div class="row">
                <?php echo $form->textFieldRow($model,'user_phone',array('size'=>60,'maxlength'=>20)); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model,'city_id'); ?>
                <?php echo $form->dropDownList($model,'city_id', MyObjects::CityChoices(),array('empty'=>'Город','data-placeholder' => 'Город',)); ?>
                <?php echo $form->error($model,'city_id'); ?>	
            </div>

            <div class="row">
                <?php 
                    echo $form->labelEx($model,'mark_id');
                    echo $form->dropDownList($model,'mark_id', MyMmodel::MarkChoices(),
                        array( 
                            'empty'=>'Марка',
                            'data-placeholder' => 'Марка',
                            'ajax' => array(
                                'type'=>'GET', //request type
                                'url'=>$this->createUrl('/objects/dynamicmodel'),
                                'data'=>array(
                                    'mark_id'=>'js:this.value'
                                ),
                                'success' => 'js:function(data){$("#MyObjects_model_id").html(data); $("select#MyObjects_model_id").trigger("refresh")}',
                            ),
                        )
                    );
                ?>
                <?php echo $form->error($model,'mark_id'); ?>
            </div>
            <div class="row">
                    <?php
                        echo $form->labelEx($model,'model_id');
                        $mmodels=array();
                        if(isset($model->id)){
                            $list=MyMmodel::model()->findAll('mark_id=:mark_id', array(':mark_id'=>(int) $model->mark_id));
                            $mmodels = CHtml::listData($list,'model_id','name');
                        }                
                    ?>
                    <?php echo $form->dropDownList($model,'model_id', $mmodels,array('empty'=>'Модель','data-placeholder'=>'Модель')); ?>
                    <?php echo $form->error($model,'model_id'); ?>	
            </div>
            <div class="row">
                <?php echo $form->labelEx($model,'year'); ?>
                <?php echo $form->dropDownList($model,'year', MyObjects::YearsChoices(),array('empty'=>'Год выпуска','data-placeholder' => 'Год выпуска')); ?>
                <?php echo $form->error($model,'year'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'km_age'); ?>
                <?php echo $form->textField($model,'km_age', array('size'=>60,'maxlength'=>50, 'placeholder' => 'Пробег, км')); ?>
                <?php echo $form->error($model,'km_age'); ?>
            </div>
        </div>

        <div class="row span4">
            <div class="row">
                    <?php echo $form->labelEx($model,'body_type'); ?>	
                    <?php echo $form->dropDownList($model,'body_type', MyObjects::$body_type,array('empty'=>'Тип кузова','data-placeholder' => 'Тип кузова')); ?>
                    <?php echo $form->error($model,'body_type'); ?>	
            </div>
            <div class="row">
                    <?php echo $form->labelEx($model,'color'); ?>
                    <?php echo $form->dropDownList($model,'color', MyObjects::$color,array('empty'=>'Цвет','data-placeholder' => 'Цвет')); ?>
                    <?php echo $form->error($model,'color'); ?>
            </div>
            <div class="row">
                    <?php echo $form->labelEx($model,'volume'); ?>
                    <?php echo $form->dropDownList($model,'volume', MyObjects::VolumeChoices(),array('empty'=>'Объем двигателя','data-placeholder' => 'Объем двигателя','encode'=> false)); ?>
                    <?php echo $form->error($model,'volume'); ?>	
            </div>
            <div class="row">
                    <?php echo $form->labelEx($model,'transmission'); ?>
                    <?php echo $form->dropDownList($model,'transmission', MyObjects::$transmission,array('empty'=>'Коробка передач','data-placeholder' => 'Коробка передач')); ?>
                    <?php echo $form->error($model,'transmission'); ?>	
            </div>

            <div class="row">
                    <?php echo $form->labelEx($model,'engine'); ?>
                    <?php echo $form->dropDownList($model,'engine', MyObjects::$engine,array('empty'=>'Тип двигателя','data-placeholder' => 'Тип двигателя')); ?>
                    <?php echo $form->error($model,'engine'); ?>	
            </div>
            <div class="row">
                    <?php echo $form->labelEx($model,'privod'); ?>
                    <?php echo $form->dropDownList($model,'privod', MyObjects::$privod,array('empty'=>'Привод','data-placeholder' => 'Привод')); ?>
                    <?php echo $form->error($model,'privod'); ?>	
            </div>

            <div class="row">
                    <?php echo $form->labelEx($model,'wheel'); ?>
                    <?php echo $form->dropDownList($model,'wheel', MyObjects::$wheel,array('empty'=>'Руль','data-placeholder' => 'Руль')); ?>
                    <?php echo $form->error($model,'wheel'); ?>	
            </div>

        </div>

        <div class="clearfix"></div>
       
        <div class="row">
            <?php echo $form->textAreaRow($model,'description',array('rows'=>8, 'class'=>'span8')); ?>
        </div>
        <div class="row">
            <?php echo $form->textFieldRow($model,'cost',array('size'=>20,'maxlength'=>20)); ?>
        </div>

        
        <div class="row">
            <label>Фотографии</label>
            <?php
            $input_image = '';
            if($model->isNewRecord) {
                $input_image = '<input name=images[] type=hidden value="+responseJSON.newname+" />';
            }
            $this->widget('ext.EAjaxUpload.EAjaxUpload', array(
                    'id' => 'object_uploadFile',
                    'multiple'=> true,
                    'config' => array(
                            'request' => array(
                                    'endpoint' => '/objects/upload',
                                    'params' => array(
                                        'id' => $model->id,
                                        //Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                                        ),
                            ),
                            'text' => array(
                                    'uploadButton' => 'Загрузить',
                                    'formatProgress' => '{percent}%',
                                    'deleteButton' => 'удалить'
                            ),
                            'fileTemplate'=> '<li>'.
                                '<div class="qq-progress-bar"></div>'.
                                '<span class="qq-upload-spinner"></span>'.
                                '<span class="qq-upload-finished"></span>'.
                                '<span class="qq-upload-thumb"></span>'.
                                '<span class="upload-delete-file"></span>'.
                                '<span class="qq-upload-file"></span>'.
                                '<span class="qq-upload-size"></span>'.
                                '<a class="qq-upload-cancel" href="#">{cancelButtonText}</a>'.
                                '<a class="qq-upload-retry" href="#">{retryButtonText}</a>'.
                                '<a class="qq-upload-delete" href="#">{deleteButtonText}</a>'.
                                '<span class="qq-upload-status-text">{statusText}</span>'.
                                '</li>',                                    
                            'validation' => array(
                                    'allowedExtensions' => array('jpg', 'jpeg', 'png', 'bmp','gif'),
                                    'sizeLimit' => 5048576, // max file size in bytes
                                    'countLimit' => 10,
                                    'fileCount' => count($model->objectsImages),
                               // 'minSizeLimit' => 256, // min file size in bytes
                            ),
                            'callbacks' => array(
                                    'onComplete' => 'js:function(id, fileName, responseJSON){'
                                                        . '$("#object_uploadFile").val(responseJSON.filename);'
                                                        . '$("#object_uploadFile ."+responseJSON.class+" .upload-delete-file").html("'.$input_image.'<a class=del-image href=# data-delete="+responseJSON.newname+">Удалить</a>");'
                                                        . '$(".upload-main-image .qq-upload-button").hide();}',
                            ),
                    )
            ));	
            ?>
            <?php if($model->objectsImages[0]): ?>
            <ul class="qq-upload-list loaded">
                    <?php foreach($model->objectsImages as $item): ?>
                        <li>
                            <?php echo CHtml::image(ObjectsImages::$smallImageDir.$item->image)?>
                            <span><a class="upload-delete-file del-image" href="javascript:;" data-delete="<?php echo $item->image; ?>">Удалить</a></span>
                        </li>
                    <?php endforeach; ?>
            </ul>		
            <?php endif; ?>                          
        </div>        
        
        
        <div class="row">
            <?php echo $form->checkboxRow($model,'published'); ?>
        </div>       
        <div class="row">
           <?php echo $form->checkboxRow($model,'state'); ?>
        </div>
        <?php 
            $objModule = Yii::app()->getModule('objects');
            $checkListParam = $objModule->params['check_list'];
            $checkListGroup = $objModule->params['check_list_group'];
        ?>       
        <?php if(empty($model->check_data)):?>
            <div class="checkListBlock row span7">
                <div class="panel-group" id="accordion">
                    <?php $collapse = $this->beginWidget('bootstrap.widgets.TbCollapse',array(
                            'id'=>'checkList'
                        ));?>
                        <?php foreach($checkListParam as $groupName => $checkParams):?>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $groupName?>">
                                            <?php echo $checkListGroup[$groupName];?>
                                        </a>
                                    </h4>
                                </div>         
                                <div id="collapse_<?php echo $groupName?>" class="panel-collapse collapse">
                                    <div class="panel-body">
                                    <?php foreach($checkParams as $checkParamId => $checkParamName):?>
                                        <div class="row">
                                            <input type="hidden" name="checkList_<?php echo $groupName?>[<?php echo $checkParamId;?>]" value="0">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="1" name="checkList_<?php echo $groupName?>[<?php echo $checkParamId;?>]" id="chp_<?php echo $checkParamId;?>" checked >
                                                    <?php echo $checkParamName;?>
                                                </label>
                                            </div>
                                            <textarea name="checkListComment_<?php echo $groupName?>[<?php echo $checkParamId;?>]"></textarea>
                                        </div>
                                    <?php endforeach;?> 
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;?>
                    <?php $this->endWidget(); ?>
                </div> 
            </div>
        <?php else:?>
            <?php $checkData = unserialize(urldecode($model->check_data));?>
            <div class="checkListBlock row span7">
                <div class="panel-group" id="accordion">
                    <?php $collapse = $this->beginWidget('bootstrap.widgets.TbCollapse',array(
                            'id'=>'checkList'
                        ));?>
                        <?php foreach($checkData as $groupName => $checkParams):?>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $groupName?>">
                                            <?php echo $checkListGroup[$groupName];?>
                                        </a>
                                    </h4>
                                </div>         
                                <div id="collapse_<?php echo $groupName?>" class="panel-collapse collapse">
                                    <div class="panel-body">
                                    <?php foreach($checkParams as $checkParamId => $checkParamName):?>
                                        <div class="row">
                                            <input type="hidden" name="checkList_<?php echo $groupName?>[<?php echo $checkParamId;?>]" value="0">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="1" name="checkList_<?php echo $groupName?>[<?php echo $checkParamId;?>]" id="chp_<?php echo $checkParamId;?>" <?php echo $checkParamName['value'] ? 'checked' : ''?>>
                                                    <?php echo $checkListParam[$groupName][$checkParamId];?>
                                                </label>
                                            </div>
                                            <textarea name="checkListComment_<?php echo $groupName?>[<?php echo $checkParamId;?>]"><?php echo $checkParamName['description']?></textarea>
                                        </div>
                                    <?php endforeach;?> 
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;?>
                    <?php $this->endWidget(); ?>
                </div> 
            </div>        
        <?php endif;?>
        <div class="clearfix"></div>
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Сохранить' : 'Сохранить',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
