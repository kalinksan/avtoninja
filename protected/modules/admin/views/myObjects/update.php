<?php

$this->menu=array(
        array('label'=>'Посмотреть объявление','url'=>array('view','id'=>$model->id)),
	array('label'=>'+ Добавить объявление','url'=>array('create')),
	array('label'=>'Управление объявлениями','url'=>array('admin')),
);
?>

<h1>Обновление объявления <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>