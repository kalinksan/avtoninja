<?php
$this->menu=array(
	array('label'=>'Добавить марку','url'=>array('create')),
	array('label'=>'Управление марками','url'=>array('admin')),
);
?>

<h1>Обновление марки <?php echo $model->mark_id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>