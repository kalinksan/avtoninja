<?php
$this->menu=array(
	array('label'=>'+ Добавить марку','url'=>array('create')),
);
?>

<h1>Марки автомобилей</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'my-mark-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'name',
                array(
                    'name'=>'popular',
                    'value'=>'$data->popular==1 ? CHtml::encode("Да") : CHtml::encode("Нет")',
                    'filter'=>CHtml::activeDropDownList($model, 'popular', array(''=>'', 1=>'Да', 0=>'Нет')),
                ),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
