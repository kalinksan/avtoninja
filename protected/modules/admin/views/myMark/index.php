<?php
$this->breadcrumbs=array(
	'My Marks',
);

$this->menu=array(
	array('label'=>'Create MyMark','url'=>array('create')),
	array('label'=>'Manage MyMark','url'=>array('admin')),
);
?>

<h1>My Marks</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
