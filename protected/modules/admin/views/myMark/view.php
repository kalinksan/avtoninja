<?php
$this->breadcrumbs=array(
	'My Marks'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List MyMark','url'=>array('index')),
	array('label'=>'Create MyMark','url'=>array('create')),
	array('label'=>'Update MyMark','url'=>array('update','id'=>$model->mark_id)),
	array('label'=>'Delete MyMark','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->mark_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MyMark','url'=>array('admin')),
);
?>

<h1>View MyMark #<?php echo $model->mark_id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'mark_id',
		'name',
		'mname',
		'popular',
	),
)); ?>
