<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('mark_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->mark_id),array('view','id'=>$data->mark_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mname')); ?>:</b>
	<?php echo CHtml::encode($data->mname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('popular')); ?>:</b>
	<?php echo CHtml::encode($data->popular); ?>
	<br />


</div>