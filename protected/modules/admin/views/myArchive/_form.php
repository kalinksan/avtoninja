<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'my-archive-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>255)); ?>

	<div class="row">           
            <?php echo $form->labelEx($model, 'fulltext'); ?>
            <?php 
            $this->widget('ext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
                'model' => $model,
                'attribute' => 'fulltext',
                'options' => array(
                    'lang' => 'ru',
                    'imageUpload' => Yii::app()->createUrl('/admin/RedactorFile/upload'),
                    'fileUpload' => Yii::app()->createUrl('/admin/RedactorFile/uploadFile')
                ),
                'plugins' => array(
                    'fullscreen' => array(
                        'js' => array('fullscreen.js', ),
                    ),
                    'filemanager'=> array(
                        'js' => array('filemanager.js', ),
                    ),
                    'imagemanager'=> array(
                        'js' => array('imagemanager.js', ),
                    ),
                    'table'=> array(
                        'js' => array('table.js', ),
                    ),
                    'video'=> array(
                        'js' => array('video.js', ),
                    ),
                    /*'clips'=> array(
                        'css' => array('clips.css',),
                        'js' => array('clips.js',),
                        'depends' => array('imperavi-redactor',),   
                    )*/
                ),

            ));
            ?>
	</div>

        <div class="row">
            <?php echo $form->textFieldRow($model,'oldurl',array('class'=>'span5','maxlength'=>255)); ?>
        </div>
        <div class="row">
            <?php echo $form->textFieldRow($model,'type',array('readonly'=>'readonly','class'=>'span5','maxlength'=>50)); ?>
        </div>    
        <div class="row">
            <?php echo $form->checkboxRow($model,'noindex'); ?>
        </div>
        
        <?php echo $form->textFieldRow($model,'metatitle',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'metakey',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textAreaRow($model,'metadesc',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>
        
        <?php echo $form->textFieldRow($model,'canonical',array('class'=>'span5','maxlength'=>255)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Сохранить' : 'Сохранить',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
