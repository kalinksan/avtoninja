<?php
$this->breadcrumbs=array(
	'My Archives'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage MyArchive','url'=>array('admin')),
);
?>

<h1>Create MyArchive</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>