<?php
$this->breadcrumbs=array(
	'My Archives',
);

$this->menu=array(
	array('label'=>'Create MyArchive','url'=>array('create')),
	array('label'=>'Manage MyArchive','url'=>array('admin')),
);
?>

<h1>My Archives</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
