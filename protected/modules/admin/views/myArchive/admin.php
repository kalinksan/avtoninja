<h1>Управление архивными материалами</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'my-archive-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'title',
		'oldurl',
                'type',
                array(
                    'name'=>'fulltext',
                    'value'=>'$data->text_size',
                    //'sortable'=>true,
                    'filter'=>false
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
