<?php
$this->menu=array(
	array('label'=>'Посмотреть материал','url'=>array('view','id'=>$model->id)),
	array('label'=>'К списку архивных материалов','url'=>array('admin')),
);
?>

<h1>Обновление архива <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>