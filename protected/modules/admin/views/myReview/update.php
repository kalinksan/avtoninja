<?php
$this->menu=array(
        array('label'=>'Посмотреть на сайте','url'=>array('view','id'=>$model->id,'mark_id'=>$model->mark_id,'model_id'=>$model->model_id), 'linkOptions'=>array('target'=>'_blank')),
	array('label'=>'Добавить новый отзыв','url'=>array('create')),
	array('label'=>'Управление отзывами','url'=>array('admin')),
);
?>

<h1>Обновление отзыва <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>