<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'my-review-form',
	'enableAjaxValidation'=>false,
    	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
	'clientOptions' => array(
		 'validateOnSubmit' => 'true',
	),
)); ?>

	<?php echo $form->errorSummary($model); ?>
        <div class="row">
            <?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>255)); ?>
        </div>
        <div class="row">
            <?php echo $form->checkboxRow($model,'moderation'); ?>
        </div>
        <div class="row span4">
            <div class="row">
                    <?php echo $form->labelEx($model,'mark_id'); ?>
                    <?php 
                        echo $form->dropDownList($model,'mark_id', MyMmodel::MarkChoices(),
                            array( 
                                'empty'=>'Марка',
                                'data-placeholder' => 'Марка',
                                'ajax' => array(
                                    'type'=>'GET', //request type
                                    'url'=>$this->createUrl('/review/dynamicmodel'),
                                    'data'=>array(
                                        'mark_id'=>'js:this.value',
                                    ),
                                    'success' => 'js:function(data){$("#MyReview_model_id").html(data); $("select#MyReview_model_id").trigger("refresh")}',
                                ),
                            )
                        );
                    ?>
                    <?php echo $form->error($model,'mark_id'); ?>
            </div>

            <div class="row">
                    <?php echo $form->labelEx($model,'model_id'); ?>
                    <?php
                        $mmodels=array();
                        if(isset($model->id)){
                            $list=MyMmodel::model()->findAll(array('condition'=>'mark_id=:mark_id','order'=>'t.name', 'params'=>array(':mark_id'=>(int) $model->mark_id)));
                            $mmodels = CHtml::listData($list,'model_id','name');
                        }                
                    ?>
                    <?php echo $form->dropDownList($model,'model_id', $mmodels,array('empty'=>'Модель','data-placeholder'=>'Модель')); ?>
                    <?php echo $form->error($model,'model_id'); ?>
            </div>

            <div class="row">
                    <?php echo $form->labelEx($model,'own'); ?>
                    <?php echo $form->dropDownList($model,'own', $model::$own,array('empty'=>'Владею','data-placeholder' => 'Владею')); ?>
                    <?php echo $form->error($model,'own'); ?>
            </div>

            <div class="row">
                    <?php echo $form->labelEx($model,'year'); ?>
                    <?php echo $form->dropDownList($model,'year', MyObjects::YearsChoices(),array('empty'=>'Год выпуска','data-placeholder' => 'Год выпуска')); ?>
                    <?php echo $form->error($model,'year'); ?>
            </div>
        </div>
        <div class="row span4">
            <div class="row">
                    <?php echo $form->labelEx($model,'face'); ?>
                    <?php
                        $this->widget('CStarRating',array(
                            'model'=>$model,
                            'attribute'=>'face',
                            'resetText'=>'Сбросить',
                            'minRating'=>1,
                            'maxRating'=>5,
                            'starCount'=>5,
                            ));
                    ?>
                    <?php echo $form->error($model,'face'); ?>
            </div>

            <div class="row">
                    <?php echo $form->labelEx($model,'comfort'); ?>
                    <?php
                        $this->widget('CStarRating',array(
                            'model'=>$model,
                            'attribute'=>'comfort',
                            'resetText'=>'Сбросить',
                            'minRating'=>1,
                            'maxRating'=>5,
                            'starCount'=>5,
                            ));
                    ?>
                    <?php echo $form->error($model,'comfort'); ?>
            </div>

            <div class="row">
                    <?php echo $form->labelEx($model,'safety'); ?>
                    <?php
                        $this->widget('CStarRating',array(
                            'model'=>$model,
                            'attribute'=>'safety',
                            'resetText'=>'Сбросить',
                            'minRating'=>1,
                            'maxRating'=>5,
                            'starCount'=>5,
                            ));
                    ?>
                    <?php echo $form->error($model,'safety'); ?>
            </div>

            <div class="row">
                    <?php echo $form->labelEx($model,'reliability'); ?>
                    <?php
                        $this->widget('CStarRating',array(
                            'model'=>$model,
                            'attribute'=>'reliability',
                            'resetText'=>'Сбросить',
                            'minRating'=>1,
                            'maxRating'=>5,
                            'starCount'=>5,
                            ));
                    ?>
                    <?php echo $form->error($model,'reliability'); ?>
            </div>
        
            <div class="row">
                    <?php echo $form->labelEx($model,'driving'); ?>
                    <?php
                        $this->widget('CStarRating',array(
                            'model'=>$model,
                            'attribute'=>'driving',
                            'resetText'=>'Сбросить',
                            'minRating'=>1,
                            'maxRating'=>5,
                            'starCount'=>5,
                            ));
                    ?>
                    <?php echo $form->error($model,'driving'); ?>
            </div>
        </div>
        
        <div class="clearfix"></div>
        
        <div class="row">
            <?php echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>255)); ?>
        </div>

        <div class="row">           
            <?php echo $form->labelEx($model, 'fulltext'); ?>
            <?php 
            $this->widget('ext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
                'model' => $model,
                'attribute' => 'fulltext',
                'options' => array(
                    'lang' => 'ru',
                    'maxHeight' => 800,
                    'imageUpload' => Yii::app()->createUrl('/admin/RedactorFile/upload'),
                    'fileUpload' => Yii::app()->createUrl('/admin/RedactorFile/uploadFile')
                ),
                'plugins' => array(
                    'fullscreen' => array(
                        'js' => array('fullscreen.js', ),
                    ),
                    'filemanager'=> array(
                        'js' => array('filemanager.js', ),
                    ),
                    'imagemanager'=> array(
                        'js' => array('imagemanager.js', ),
                    ),
                    'table'=> array(
                        'js' => array('table.js', ),
                    ),
                    'video'=> array(
                        'js' => array('video.js', ),
                    ),
                ),

            ));
            ?>
            <?php echo $form->error($model, 'fulltext'); ?>
	</div>
        <div class="row">
            <?php echo $form->labelEx($model, 'main_image'); ?>
            <?php echo $form->hiddenField($model,'main_image',array('size'=>60,'maxlength'=>255)); ?>
            <div class="upload-main-image" <?php print (empty($model->main_image) || $model->main_image=="NULL") ? '' : 'style="display:none"' ?>>		
                    <?php
                    $this->widget('ext.EAjaxUpload.EAjaxUpload', array(
                            'id' => 'uploadFile',
                            'multiple'=> true,
                            'config' => array(
                                    'request' => array(
                                            'endpoint' => '/admin/MyReview/upload',
                                    ),
                                    'validation' => array(
                                            'allowedExtensions' => array('jpg', 'jpeg', 'png', 'bmp','gif'),
                                            'sizeLimit' => 5048576, // max file size in bytes
                                       // 'minSizeLimit' => 256, // min file size in bytes
                                    ),
                                    'callbacks' => array(
                                            'onComplete' => 'js:function(id, fileName, responseJSON){$("#MyReview_main_image").val(responseJSON.newname);$("#uploadFile ."+responseJSON.class+" .upload-delete-file").html("<a class=del-main-image href=# data-delete="+responseJSON.newname+">Удалить</a>"); $(".upload-main-image .qq-upload-button").hide();}',
                                    ),
                            )
                    ));	

                    ?>
            </div>

            <?php if(!empty($model->main_image) && ($model->main_image!="NULL")):?>
                    <div class="update-main-image well">
                            <?php
                            echo CHtml::image(Yii::app()->baseUrl.'/images/reviews/thumb/'.$model->main_image);
                            $this->widget('bootstrap.widgets.TbButton', array(
                                    'label' => 'удалить',
                                    'type' => 'danger',
                                    'htmlOptions' => array(
                                            'ajax' => array(
                                                    'type' => 'POST',
                                                    'url' => $this->createUrl('/admin/MyReview/DelMainImage',array('file' => $model->main_image)),
                                                    'success' => 'function(data) {  $("#MyReview_main_image").val(""); $(".upload-main-image").show();$(".update-main-image").hide();}',
                                            ),
                                            'id'=>'main_image'
                                    ),
                            ));?>
                    </div>
            <?php endif;?>
	</div>
        <div class="row span4">
            <?php echo $form->textAreaRow($model,'plus',array('rows'=>6, 'cols'=>50, 'class'=>'span10')); ?>
        </div>
        <div class="row span4">
            <?php echo $form->textAreaRow($model,'minus',array('rows'=>6, 'cols'=>50, 'class'=>'span10')); ?>
        </div>
        
        <div class="clearfix"></div>

	<?php echo $form->textFieldRow($model,'metatitle',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textAreaRow($model,'metadesc',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'metakey',array('class'=>'span5','maxlength'=>255)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Сохранить' : 'Сохранить',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
