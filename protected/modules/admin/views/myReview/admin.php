<?php

$this->menu=array(
	array('label'=>'Добавить отзыв','url'=>array('create')),
);

?>

<h1>Управление отзывами</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'my-review-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'columns'=>array(
		'title',
                array(
                    'name'=>'mark.name',
                    'filter'=>CHtml::activeTextField($model->searchMark, 'name'),
                ),
                array(
                    'name'=>'model.name',
                    'filter'=>CHtml::activeTextField($model->searchModel, 'name'),
                ),
                array(
                    'name'=>'moderation',
                    'value'=>'$data->moderation==1 ? CHtml::encode("Да") : CHtml::encode("Нет")',
                    'filter'=>CHtml::activeDropDownList($model, 'moderation', array(''=>'', 1=>'Да', 0=>'Нет')),
                ),
                'created',
		array(
                    'class'=>'bootstrap.widgets.TbButtonColumn',
                    'viewButtonUrl'=>'Yii::app()->createUrl("/admin/myReview/view", array("id"=>$data->id, "mark_id"=>$data->mark_id, "model_id"=>$data->model_id))',
                    'viewButtonOptions'=>array('target'=>'_blank'),
		),
	),
)); ?>