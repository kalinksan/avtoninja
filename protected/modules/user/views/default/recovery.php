<?php 
$this->pageTitle=Yii::app()->name . ' - Восстановление пароля';
?>

<div class="inner_block">
	<div class="ribTitle">
		<h1>ВОССТАНОВЛЕНИЕ ПАРОЛЯ</h1>
	</div>
	<?php if(Yii::app()->user->hasFlash('successMessage')): ?>
	<div class="successMessage">
		<?php echo Yii::app()->user->getFlash('successMessage'); ?>
	</div>
	<?php else: ?>
	<div class="form">
		<?php $form = $this->beginWidget('CActiveForm', array(
				'id'=>'page-user-recovery',
				'enableAjaxValidation'=>true,
				'clientOptions'=>array(
					'validateOnSubmit'=>true,
				)
		)); ?>
			<div class="row">	
				<p class="hint">Пожалуйста, введите свой email адрес</p>
			</div>
			<div class="row">
				<p class="name">
					<?php echo $form->labelEx($user,'email'); ?>
				</p>
				<?php echo $form->textField($user,'email', array('class'=>'iField')) ?>
				<div class="ferror">
					<?php echo $form->error($user,'email'); ?>
				</div>
			</div>
			
			<div class="buttons">
				<?php echo CHtml::submitButton('Напомнить', array('class'=>'button med')); ?>
			</div>

		<?php $this->endWidget(); ?>
		</div><!-- form -->
	<?php endif; ?>
</div>