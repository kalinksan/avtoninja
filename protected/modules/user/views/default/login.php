<div class="inner_block">
    <div class="login_page">
            <div class="block_header">ВХОД</div>
            <div class="fb">
                <?php 
                    echo CHtml::link('facebook', Yii::app()->createUrl('/user/login/', array('service'=>'facebook')));
                ?>
            </div>
            <div class="google">
                <?php 
                    echo CHtml::link('google', Yii::app()->createUrl('/user/login/', array('service'=>'google')));
                ?>
            </div>
            <div class="vk">
                <?php 
                    echo CHtml::link('vkontakte', Yii::app()->createUrl('/user/login/', array('service'=>'vkontakte')));
                ?>
            </div>		
    </div>
</div>
