<?php
$this->pageTitle=Yii::app()->name . ' - Регистрация';
?>
<div class="inner_block">
	<div class="ribTitle">
		<h1>РЕГИСТРАЦИЯ</h1>
	</div>
	<div class="form">
		<?php $form = $this->beginWidget('CActiveForm', array(
				'id'=>'page-registration-form',
				'enableAjaxValidation'=>true,
				'clientOptions'=>array(
					'validateOnSubmit'=>true,
				)
		)); ?>
		<?php //echo $form->errorSummary($model); ?>
	 
		<div class="row">
			<p class="name">
				<?php echo $form->labelEx($model, 'email'); ?>
			</p>
			<?php echo $form->textField($model, 'email', array('class'=>'iField'))?>
			<div class="ferror">
				<?php echo $form->error($model,'email'); ?>
			</div>
		</div>
		
		<div class="row">
			<p class="name">
				<?php echo $form->labelEx($model, 'name'); ?>
			</p>
			<?php echo $form->textField($model, 'name', array('class'=>'iField'))?>
			<div class="ferror">
				<?php echo $form->error($model,'name'); ?>
			</div>
		</div>
	 
		<div class="row">
			<p class="name">
				<?php echo $form->labelEx($model, 'password'); ?>
			</p>
			<?php echo $form->passwordField($model, 'password', array('class'=>'iField')) ?>
			<div class="ferror">
				<?php echo $form->error($model,'password'); ?>
			</div>
		</div>

		<div class="buttons">
			<?=CHtml::submitButton('Зарегистрироваться', array('class'=>'button med', 'id' => "submit")); ?>
		</div>	 
		<?php $this->endWidget(); ?>
	</div><!-- form -->
	<div class="ribTitle">
		<h1>БЫСТРЫЙ РЕГИСТРАЦИЯ</h1>
	</div>
	<div class="socials">
		<?php Yii::app()->eauth->renderWidget(); ?>
		<div class="empty"></div>
	</div>
</div>