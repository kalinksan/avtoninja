<?php

class DefaultController extends Controller {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
            'ajaxOnly + clearUserCache,SetRedirectUrl',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('requestActivation'),
                'roles' => array('@'),
            ),
            array('deny', // deny all users
                'actions' => array('requestActivation'),
                'users' => array('?'),
            ),
        );
    }

    // сохраняет url для редиректа посетителя в сессии WebUser
    public function actionSetRedirectUrl() {
        if ($_POST['url']) {
            $url = strip_tags($_POST['url']);
            Yii::app()->user->setState('redirectUrl', $url);
            Yii::app()->end();
        }
        $this->redirect(Yii::app()->getHomeUrl());
    }

    public function actionIndex() {
        if (Yii::app()->user->isGuest) {
            $this->redirect(Yii::app()->createUrl('user/login'));
        } else {
            $this->redirect(Yii::app()->getHomeUrl());
        }
    }
    
    public function actionRecovery() {
        $user = new User('recovery');

        if (Yii::app()->user->id) {
            $this->redirect(Yii::app()->user->returnUrl);
        } else {
            $email = ((isset($_GET['email'])) ? $_GET['email'] : '');
            $hash_key = ((isset($_GET['hash'])) ? $_GET['hash'] : '');
            if ($email && $hash_key) {
                $user->scenario = 'changePass';
                $this->performAjaxValidation($user, 'user-changepassword-form');
                $find = $user->model()->findByAttributes(array('email' => $email, 'hash_key' => $hash_key, 'block' => 0));

                // если пользователь найден и не заблокирован
                if (isset($find) && !empty($find)) {
                    if (isset($_POST['User'])) {
                        $user->attributes = $_POST['User'];
                        if ($user->validate()) {
                            $salt = CUserHelper::genRandomPassword(32);
                            $crypt = User::getCryptedPassword($user->password, $salt);
                            $find->password = $crypt . ':' . $salt;
                            $find->hash_key = NULL;

                            if ($find->save(false)) {
                                $find->password = $user->password;
                                if ($find->login(array('email' => $find->email))) {
                                    // если вход при переходе на добавление объявления
                                    if (isset(Yii::app()->user->redirectUrl)) {
                                        if (!empty(Yii::app()->user->redirectUrl)) {
                                            $this->redirect(Yii::app()->user->redirectUrl);
                                            Yii::app()->user->setState('redirectUrl', NULL);
                                            exit();
                                        }
                                    }
                                    Yii::app()->user->setFlash('successMessage', "Новый пароль был сохранен. Теперь вы можете входить на сайт с новым паролем.");
                                    $this->redirect(Yii::app()->createUrl('user/profile'));
                                }
                            }
                        }
                    }
                    $this->render('changepassword', array('user' => $user));
                } else {
                    $this->redirect(Yii::app()->user->returnUrl);
                }
            } else {
                $this->performAjaxValidation($user, 'user-recovery-form');
                if (isset($_POST['User'])) {
                    $user->attributes = $_POST['User'];
                    if ($user->validate()) {
                        $salt = CUserHelper::genRandomPassword(32);
                        $hash_key = User::getCryptedPassword($user->email, $salt);
                        $user = User::model()->findByAttributes(array('email' => $user->email));

                        $user->hash_key = $hash_key;
                        $user->save(false);

                        $activation_url = '<a href="' . Yii::app()->request->hostInfo . '/user/recovery/hash/' . $hash_key . '/email/' . $user->email . '">' . Yii::app()->request->hostInfo . '/user/recovery/hash/' . $hash_key . '/email/' . $user->email . '</a>';
                        $subject = 'Восстановление пароля на сайте Pushkin.ru';
                        $message = 'Для создания нового пароля на сайте <a href="' . Yii::app()->request->hostInfo . '">pushkin.ru</a> перейдите по ссылке ' . $activation_url;

                        //
                        if (sendMail($user->email, 'pushkin@info.ru', $subject, $message)) {
                            $message = 'Пожалуйста, проверьте почту. Инструкции по смене пароля были отправлены на ваш e-mail.';
                            Yii::app()->user->setFlash('successMessage', $message);

                            if ($_POST['User']['operation'] == 'ajax') {
                                $response = array('status' => 1, 'message' => $message);
                                echo json_encode($response);
                                Yii::app()->end();
                            }
                        }
                        $this->refresh();
                    }
                }
                $this->render('recovery', array('user' => $user));
            }
        }
    }

    public function actionRequestActivation() {
        $id = intVal($_GET['id']);
        if ($id > 0) {
            $activation = substr(md5(uniqid(rand(), true)), 0, rand(10, 15));
            $user = User::model()->findByPk($id);
            if (User::model()->updateByPk($id, array('activation' => $activation, 'confirm_email' => 0))) {
                CUserHelper::sendActivationKey($activation, $user->email);
                Yii::app()->user->setFlash('successMessage', 'Код подтверждения выслан на ваш e-mail "' . $user->email . '"!');
            }
            $this->redirect(Yii::app()->createUrl('/user/profile/update'));
        }
        $this->redirect(Yii::app()->request->hostInfo);
    }

    public function actionActivation() {
        if (!empty($_GET['key'])) {
            $user = User::model()->find('activation = :activation', array(':activation' => $_GET['key']));
            if (!empty($user)) {
                if ($user->confirm_email == 1) {
                    Yii::app()->user->setFlash('successMessage', "Аккаунт уже активирован!");
                    $this->redirect('/user/profile', array('model' => $user));
                } else {
                    $user->confirm_email = 1;
                    $user->activation = NULL;
                    if ($user->save(false)) {
                        Yii::app()->user->setFlash('successMessage', "Аккаунт успешно активирован!");
                        Yii::app()->user->setState('confirm', $user->confirm_email);
                        if (!Yii::app()->user->isGuest) {
                            $this->redirect('/user/profile', array('model' => $user));
                        } else {
                            $this->redirect('/user/login', array('model' => $user));
                        }
                    }
                }
            }
        }
        $this->redirect(Yii::app()->user->returnUrl);
    }

    public function actionAddEmail() {
        $model = User::model()->find('id = :id', array(':id' => Yii::app()->user->id));
        $model->scenario = 'addEmail';

        $this->performAjaxValidation($model, 'add-email-form');

        if (isset($_POST['User']) && !$model->email) {
            $model->attributes = $_POST['User'];
            $model->activation = substr(md5(uniqid(rand(), true)), 0, rand(10, 15));
            $model->confirm_email = 0;
            if ($model->save()) {
                CUserHelper::sendActivationKey($model->activation, $model->email);
                Yii::app()->user->setFlash('successMessage', "Спасибо, e-mail успешно сохранен. Пожалуйста, проверьте почту. Инструкция по завершению объединения аккаунтов отправлена на ваш e-mail.");
                Yii::app()->user->setState('email', $model->email);

                //$this->redirect(Yii::app()->request->getUrlReferrer());
                $this->redirect(Yii::app()->createUrl('/user/profile'));
            }
        }
        $this->redirect(Yii::app()->request->hostInfo);
    }

    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect('/');
    }

    public function actionLogin() {
        if (Yii::app()->user->isGuest) {
            // авторизация через соц. сети
            // запись в базу происходит в ServiceUserIdentity
            $service = Yii::app()->request->getQuery('service');
            if (isset($service)) {
                $authIdentity = Yii::app()->eauth->getIdentity($service);
                if (Yii::app()->request->cookies['destination']->value && Yii::app()->request->cookies['destination']->value <> 'null') {
                    $redirectUrl = Yii::app()->request->cookies['destination']->value; //destination из атрибута ссылки вызывающей окно авторизации
                } else {
                    $redirectUrl = Yii::app()->request->urlReferrer;
                }
                $authIdentity->redirectUrl = $redirectUrl;
                $authIdentity->cancelUrl = $this->createAbsoluteUrl('user/login');
                if ($authIdentity->authenticate()) {
                    $identity = new ServiceUserIdentity($authIdentity);
                    // Успешный вход
                    if ($identity->authenticate()) {
                        unset(Yii::app()->request->cookies['destination']);
                        Yii::app()->user->login($identity);
                        $authIdentity->redirect();
                        exit();
                    } else {
                        // Закрываем popup окно и перенаправляем на cancelUrl
                        $authIdentity->cancel();
                    }
                }
                // Что-то пошло не так, перенаправляем на страницу входа
                $this->redirect(Yii::app()->createUrl('/user/login/'));
                // заходим не через соц. сеть
            } else {
                $user = new User('login');

                // if it is ajax validation request
                $this->performAjaxValidation($user, 'login-form');

                // collect user input data
                if (isset($_POST['User'])) {
                    $user->attributes = $_POST['User'];
                    // validate user input and redirect to the previous page if valid
                    if ($user->validate() && $user->login($user->email)) {
                        // если вход при переходе на добавление объявления
                        if (isset(Yii::app()->user->redirectUrl)) {
                            if (!empty(Yii::app()->user->redirectUrl)) {
                                $this->redirect(Yii::app()->user->redirectUrl);
                                Yii::app()->user->setState('redirectUrl', NULL);
                                exit();
                            }
                        }
                        $this->redirect(Yii::app()->request->getUrlReferrer());
                        exit();
                    }
                }
                // display the login form
                $this->render('login', array('model' => $user));
            }
        } else {
            //Yii::app()->user->returnUrl
            $this->redirect(Yii::app()->user->returnUrl);
        }
    }

    public function actionRegistration() {
        if (Yii::app()->user->isGuest) {
            // создаём модель и задаём сценарий registration для валидации
            $user = new User('registration');

            /*
             * Ajax валидация
             */
            $this->performAjaxValidation($user, 'registration-form');

            if (empty($_POST['User'])) {
                $this->render('registration', array('model' => $user));
            } else {
                /*
                 * Форма получена
                 */
                $user->attributes = $_POST['User'];

                /*
                 * Валидация данных
                 */
                if ($user->validate('registration')) {
                    /*
                     * Если проверка пройдена, проверяем на уникальность имя
                     * пользователя и e-mail
                     */
                    if ($user->model()->count("email = :email", array(':email' => $user->email))) {
                        $user->addError('email', 'E-mail уже занят');
                        $this->render("registration", array('model' => $user));
                    } else {
                        /*
                         * Если проверки пройдены шифруем пароль, генерируем код
                         * активации аккаунта, а также устанавливаем время регистрации
                         * и роль по умолчанию для пользователя
                         */
                        $password = $user->password;

                        $salt = CUserHelper::genRandomPassword(32);
                        $crypt = $user->getCryptedPassword($user->password, $salt);
                        $user->password = $crypt . ':' . $salt;

                        $user->activation = substr(md5(uniqid(rand(), true)), 0, rand(10, 15));
                        $user->block = 0;
                        $user->confirm_email = 0;
                        $user->registerDate = date('Y-m-d H:i:s', time());

                        /*
                         * Проверяем если добавление пользователя прошло успешно
                         * устанавливаем ему права.
                         */
                        if ($user->save(false)) {

                            $user->password = $password;
                            if ($user->login(array('email' => $user->email))) {
                                CUserHelper::sendRegistrationMail($user->activation, $user->email, $user->name, $password);
                                Yii::app()->user->setFlash('successMessage', "Благодарим вас за регистрацию на портале Ninja.ru!");
                                $this->redirect('/');
                                //$this->redirect(Yii::app()->request->getUrlReferrer());
                            }
                        } else {
                            throw new CHttpException(403, 'Ошибка добавления в базу данных.');
                        }
                    }
                } else {
                    $this->render('registration', array('model' => $user));
                }
            }
        } else {
            $this->redirect(Yii::app()->request->hostInfo);
        }
    }

    protected function performAjaxValidation($model, $formid) {
        if (isset($_POST['ajax'])) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
