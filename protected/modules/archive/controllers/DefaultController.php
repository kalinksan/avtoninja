<?php

class DefaultController extends Controller
{
	public function actionView($id)
	{
            $model = $this->loadModel($id);
            $this->pageDescription = $model->metakey;
            $this->pageKeywords = $model->metakey;
            Yii::app()->clientScript->registerScript('scrolling',"	
			$('#right_content_block').containedStickyScroll({
				unstick : true,
				easing: 'linear',
				duration: 500,
				queue: false,
				closeChar: '',
				closeTop: 0,
				closeRight: 0   
			});");
            $this->render('view',array(
                'model'=>$model,
            ));
	}        
        
	public function loadModel($id)
	{
            $model=Archive::model()->findByPk($id);
            if($model===null)
                throw new CHttpException(404,'Запрашиваемая страница не существует.');
            return $model;
	}
}