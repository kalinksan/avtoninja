<?php $this->pageTitle = $model->metatitle ? $model->metatitle : $model->title; ?>
<?php $this->pageCanonicalUrl = $model->canonical; ?>
<?php if ($model->noindex) Yii::app()->clientScript->registerMetaTag("noindex", 'robots'); ?>
<div class="inner_block">
    <?php echo Yii::app()->controller->renderPartial('application.views.reklama.topRekBlock'); ?>
    <div class="gray_block testdrive archive">
        <h1><?php echo $model->title; ?></h1>
        <?php echo $model->fulltext; ?>
    </div>
    <?php echo Yii::app()->controller->renderPartial('application.views.reklama.bottomRekBlock'); ?>
</div>
<?php $this->beginClip('sidebar'); ?>
<div class="usefull_goods_block">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- На Авто Ниндзю -->
    <ins class="adsbygoogle"
    style="display:block"
    data-ad-client="ca-pub-7917871120550568"
    data-ad-slot="6092430064"
    data-ad-format="auto"></ins>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</div>
<?php $this->endClip(); ?>