<?php $this->pageTitle = $model->metatitle ? $model->metatitle : $model->title;?>
<?php $this->pageDescription = $model->metadesc;?>
<?php $this->pageKeywords = $model->metakey;?>
<?php $this->pageCanonicalUrl = $model->canonical; ?>
<?php if($model->noindex) Yii::app()->clientScript->registerMetaTag("noindex", 'robots'); ?>
<?php
    Yii::app()->clientScript->registerScript('psevdo-menu',
    "
    $('a.psevdo-link').click(function(event) {
        var hBlock = $('.psevdo-menu');
        hBlock.fadeToggle(300);
        return false;
    });
    $(document).click(function (e) {
        if ($(event.target).closest('.psevdo-menu').length) return;
        $('.psevdo-menu').fadeOut(300);
        event.stopPropagation();
    });
    ");
?>
<div class="inner_block">
    <?php echo Yii::app()->controller->renderPartial('application.views.reklama.topRekBlock');?>   
    <ul class="submenu">
        <li><?php echo CHtml::link('Объявления', Yii::app()->homeUrl);?></li>
        <?php if(count($model->testdriveModel)==1):?>
        <li><?php echo CHtml::link('Тест-драйвы '.$model->testdriveModel[0]->mark->name.' '.$model->testdriveModel[0]->name, Yii::app()->createUrl('/testdrive/default/list', array('model'=>$model->testdriveModel[0]->mname,'mark' => $model->testdriveModel[0]->mark->mname)));?></li>
        <?php else:?>
        <li class="submenu-psevdo">
            <?php echo CHtml::link('Тест-драйвы','#testdrive',array('class'=>'psevdo-link'));?>
            <ul class="psevdo-menu">
                <?php foreach($model->testdriveModel as $mmodel):?>
                <li><?php echo CHtml::link($mmodel->mark->name.' '.$mmodel->name, Yii::app()->createUrl('/testdrive/default/list', array('model'=>$mmodel->mname,'mark' => $mmodel->mark->mname)));?></li>
                <?php endforeach;?>
            </ul>
        </li>
        <?php endif;?>
        <?php if($reviewCount):?>
        <li>
            <?php 
            echo CHtml::link(
                'Отзывы '.$model->testdriveModel[0]->mark->name.' '.$model->testdriveModel[0]->name, 
                Yii::app()->createUrl('/review/default/list',array('model'=>$model->testdriveModel[0]->mname,'mark' => $model->testdriveModel[0]->mark->mname))
                );
            ?>
        </li>
        <?php endif;?>     
    </ul>
    <div class="gray_block testdrive">
        <h1><?php echo $model->title;?></h1>
        <?php echo $model->fulltext;?>
    </div>
    <?php echo Yii::app()->controller->renderPartial('application.views.reklama.bottomRekBlock');?>
</div>
<?php $this->beginClip('sidebar');?>
<div class="usefull_goods_block">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- На Авто Ниндзю -->
    <ins class="adsbygoogle"
    style="display:block"
    data-ad-client="ca-pub-7917871120550568"
    data-ad-slot="6092430064"
    data-ad-format="auto"></ins>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</div>
<?php $this->endClip();?>