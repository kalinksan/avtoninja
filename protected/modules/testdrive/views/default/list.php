<?php
    $pageTitle = 'Тест-драйв ' . $mark->name . ' ' . $testdrives[0]->testdriveModel[0]->name;
    $synonymMark = explode(',', trim($mark->synonym));
    $synonymModel = explode(',', trim($testdrives[0]->testdriveModel[0]->synonym));
    $synonym = array_merge($synonymMark,$synonymModel);
    $pageTitle = $synonym ? $pageTitle . " (" .  trim(implode(', ', $synonym), ', ') . ")" : $pageTitle; 
    $this->pageTitle = $pageTitle;  
    $this->pageDescription = isset($testdrives[0]->testdriveModel[0]->testdrive_metadesc) ? $testdrives[0]->testdriveModel[0]->testdrive_metadesc : '';
?>
<?php if($testdrives[0]->testdriveModel[0]->text_block == ''){
    Yii::app()->clientScript->registerMetaTag("noindex", 'robots');  
    Yii::app()->clientScript->registerMetaTag("noindex, nofollow", 'robots'); 
}?>
<?php //$this->pageCanonicalUrl = $model->canonical;    ?>
<?php //if($model->noindex) Yii::app()->clientScript->registerMetaTag("noindex", 'robots');    ?>
<div class="inner_block">
    <?php echo Yii::app()->controller->renderPartial('application.views.reklama.topRekBlock'); ?>
    <ul class="submenu">
        <li><?php echo CHtml::link('Объявления', Yii::app()->homeUrl);?></li>
        <li><?php echo CHtml::link('Тест-драйв '.$mark->name, Yii::app()->createUrl('/testdrive/default/ModelList', array('mark' => $mark->mname)));?></li>
        <?php if($reviewCount):?>
        <li>
            <?php echo CHtml::link(
                'Отзывы '.$mark->name, 
                Yii::app()->createUrl('/review/default/ModelList', array('mark' => $mark->mname))
            );
            ?>
        </li> 
        <?php endif;?>   
    </ul>
    <div class="gray_block reply">
        <h1><?php echo $pageTitle ?></h1>
        <!--<iframe width="500" height="100" frameborder="0" scrolling="no" src="http://avto.ninja" allowtransparency="true"> </iframe>-->
        <?php foreach ($testdrives as $test): ?>
            <div class="reply_item">
                <?php if ($test->main_image): ?>
                    <div class="image">
                        <?php echo CHtml::image('/images/testdrive/thumb/' . $test->main_image, '', array('width' => '95px', 'height' => '95px')) ?>
                    </div>
                <?php endif; ?>
                <div class="description">
                    <?php if($test->type=='text'):?>
                    <h2 class="item_header outer_link"><span class="psevdo_link" data-id="<?php echo $test->id;?>"></span></h2>
                    <div class="reply_text"><?php echo $test->introtext ?></div>
                    <?php elseif($test->type=='video'):?>
                    <h2 class="item_header video_testdrive"><?php echo $test->title ?></h2>
                    <div class="reply_text toggle_text"><?php echo $test->introtext ?></div>
                    <?php endif;?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    
    <?php echo Yii::app()->controller->renderPartial('application.views.reklama.bottomRekBlock'); ?>
    
    <?php if(!empty($gallerys)):?>
        <div class="gallery">
            <?php foreach($gallerys as $gallery):?>
                <?php if(!empty($gallery->gallery->galleryPhotos)):?>
                    <?php foreach($gallery->gallery->galleryPhotos as $img):?>
                        <?php echo CHtml::image('/gallery/'.$img->id.'big.jpg',$img->name);?>
                    <?php endforeach;?>
                <?php endif;?>
            <?php endforeach;?>
        </div>
    <?php endif;?>
    
    <?php if($testdrives[0]->testdriveModel[0]->text_block!=''):?>
        <div class="text_block_bottom">
            <?php echo $testdrives[0]->testdriveModel[0]->text_block;?>
        </div>
    <?php endif;?>  
</div>
<?php $this->beginClip('sidebar'); ?>
<div class="usefull_goods_block">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- На Авто Ниндзю -->
    <ins class="adsbygoogle"
    style="display:block"
    data-ad-client="ca-pub-7917871120550568"
    data-ad-slot="6092430064"
    data-ad-format="auto"></ins>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</div>
<?php $this->endClip(); ?>
