<?php 
$pageTitle = 'Тест-драйв ' . $marks->name ;
$pageTitle = $marks->synonym ? $pageTitle . " (" . $marks->synonym . ")" : $pageTitle;
$this->pageTitle = $pageTitle; 
$this->pageDescription = isset($marks->testdrive_metadesc) ? $marks->testdrive_metadesc : '';
?>
<?php if($marks->text_block == ''){
    Yii::app()->clientScript->registerMetaTag("noindex", 'robots');  
    Yii::app()->clientScript->registerMetaTag("noindex, nofollow", 'robots'); 
}?>
<?php //$this->pageCanonicalUrl = $model->canonical;  ?>
<?php //if($model->noindex) Yii::app()->clientScript->registerMetaTag("noindex", 'robots');  ?>
<div class="inner_block testdrive_list">
    <h1><?php echo $pageTitle ?></h1>
    <?php echo Yii::app()->controller->renderPartial('application.views.reklama.topRekBlock'); ?>
    <div class="testdrive_block">
        <?php if (!empty($marks)): ?>
            <div class="marks_group_menu">
                <?php foreach($menuGroups as $mg):?>
                    <?php $this->widget('zii.widgets.CMenu', array('items'=>$mg,'htmlOptions'=>array('class'=>'group_cat'))); ?>
                <?php endforeach;?>
            </div>
        <?php endif; ?>
    </div>
    <?php echo Yii::app()->controller->renderPartial('application.views.reklama.bottomRekBlock'); ?>
    <?php if($marks->text_block!=''):?>
        <div class="text_block_bottom">
            <?php echo $marks->text_block;?>
        </div>
    <?php endif;?>
</div>
<?php $this->beginClip('sidebar'); ?>
<div class="usefull_goods_block">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- На Авто Ниндзю -->
    <ins class="adsbygoogle"
    style="display:block"
    data-ad-client="ca-pub-7917871120550568"
    data-ad-slot="6092430064"
    data-ad-format="auto"></ins>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</div>
<?php $this->endClip(); ?>