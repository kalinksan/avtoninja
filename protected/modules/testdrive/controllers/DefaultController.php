<?php

class DefaultController extends Controller {

    private $_model;

    public function actionIndex() {
        $marks = MyMark::model()->with(
                        array(
                            'automodels' => array(
                                'with' => array('testdrives' => array(
                                        'select' => 'id',
                                    )
                                ),
                                'select'=>'model_id, name, mname'
                            )
                        )
                )->findAll(
                array(
                    'select' => 'mark_id, popular, mname, name',
                    'condition' => '`testdrives`.`published`=1',
                    'order' => '`t`.`name` ASC',
                )
        );
        
        $groups = MyTestdrive::getIndexMenu($marks);
        $menuGroups = $groups['allMarks'];
        $menuPopulargroups = $groups['popularMarks'];
        $this->render('index', array(
            'marks' => $marks,
            'menuGroups' => $menuGroups,
            'menuPopulargroups' => $menuPopulargroups
        ));
    }
    /*
    public function actionResaveMark() {
        set_time_limit(3000);
        MyMark::resave();
    }
    
    public function actionResaveModel() {
        set_time_limit(3000);
        MyMmodel::resave();
    }*/
    
    public function actionModelList($mark) {
        $marks = MyMark::model()->with(
                    array(
                        'automodels' => array(
                            'with' => array(
                                'testdrives' => array(
                                    'select' => 'id',
                                ),
                            ),
                            'select'=>'model_id, name, mname',
                        )
                    )
                )->find(
                    array(
                        'condition' => '`testdrives`.`published`=1 AND `t`.`mname`=:mark',
                        'params' => array(':mark' => $mark)
                    )
                );
        if (empty($marks))
            throw new CHttpException(404, 'Запрашиваемая страница не существует.');
        
        $groups = MyTestdrive::getIndexModelList($marks);
        
        $this->render('mmodel_list', array(
            'marks' => $marks,
            'menuGroups' => $groups,
        ));
    }

    public function actionList() {
        $mmodel = Yii::app()->request->getParam('model');
        $markGet = Yii::app()->request->getParam('mark');
        if ($mmodel) {
            $testdrives = MyTestdrive::model()->with(
                    array(
                            'testdriveModel'
                        )
                    )->findAll(
                    array(
                        'condition' => 'testdriveModel.mname=:mmodel AND published=1',
                        'params' => array(':mmodel' => $mmodel)
            ));
            
            if (empty($testdrives)){
                throw new CHttpException(404, 'Запрашиваемая страница не существует.');
            }
            
            $mark = MyMark::model()->findByPk($testdrives[0]->testdriveModel[0]->mark_id);
            
            if($markGet != $mark->mname){
                throw new CHttpException(404, 'Запрашиваемая страница не существует.');
            }
            
            $gallerys = MyAvtoGallery::model()->with(
                array(
                    'galleryModel'=>array(
                        'select' => 'model_id'
                    ),
                    'gallery'=>array(
                        'select' => 'id',
                        'with' => array(
                            'galleryPhotos'=>array(
                                'select'=>'id, name'
                            )
                        )
                    )
                )
            )->findAll(
                array(
                    'condition' => 'galleryModel.mname=:mmodel AND razdel LIKE "%testdrive%" AND published=1',
                    'params' => array(':mmodel' => $mmodel)
                )
            );        
            
            /* Проверка существования отзывов марки для вывода в меню */
            $reviews = MyReview::model()->count(
                array(
                    'with'=>array(
                        'mark'=>array(
                            'select'=>'mark_id',
                            'condition'=>'`mark`.`mark_id`=:mark_id',
                            'params' => array(':mark_id'=>$mark->mark_id),
                        )
                    ),
                    'select'=>'id',
                    )
                );            
            
            Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/assets/testdrive.list.js');
            $this->render('list', array(
                'mark' => $mark,
                'testdrives' => $testdrives,
                'gallerys' => $gallerys,
                'reviewCount'=> ($reviews) ? true : false
            ));
        }else {
            throw new CHttpException(404, 'Запрашиваемая страница не существует.');
        }
    }

    public function actionReturnLink(){
        $idTestdrive = Yii::app()->getRequest()->getPost('id');
        $testdrive = MyTestdrive::model()->findByPk($idTestdrive);
        $this->renderPartial('_link', array(
            'testdrive' => $testdrive,
        ), false, false);
    }
    
    public function loadModel($id) {
        if ($this->_model === null) {
            if (isset($id)) {
                $criteria = new CDbCriteria;
                $criteria->with = array('testdriveModel'=>array('with'=>'mark'));
                $criteria->params = array(':id' => $id);
                $this->_model = MyTestdrive::model()->find($criteria);
            }
            if ($this->_model === null)
                throw new CHttpException(404, 'Запрашиваемая страница не существует.');
        }
        return $this->_model;
    }

}
