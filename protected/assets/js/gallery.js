$(document).ready(function(){		
    var slider = $('.bxObject').bxSlider({
      pagerCustom: '#bx-pager-object',
      mode: 'fade',
      responsive: false,
      controls: false,
      nextSelector: 'bx-viewport'
    });

    $('.bxObject').click(function(){
        slider.goToNextSlide();
        return false;
    });
});	