jQuery.fn.live = function (types, data, fn){jQuery(this.context).on(types,this.selector,data,fn); return this;};
$(document).ready(function(){    
    $('#searchform .subitems .filter-model a').click(function(e){
        e.preventDefault();
        $('#searchform .subitems .filter-model').toggleClass('active');
        $('#searchform .subitems .non-popular').toggle();
    });
    
    if($("#MyObjects_user_phone").length){
        $("#MyObjects_user_phone").mask("+7 (999) 999-99-99");
    }
    if($(".phone-mask").length){
        $(".phone-mask").mask("+7 (999) 999-99-99");
    }
	
    $('a[href="#login_block2"]').click(function(){
            destination = $(this).attr('data-destination');
            if(destination){
                    $.cookie('destination',destination, { path: '/'});
            }else{
                    $.cookie('destination',null, { path: '/'});
            }
    });

});
