function onAddTag(tag) {
        alert("Added a tag: ");
}
function onRemoveTag(tag) {
        alert("Removed a tag: ");
}

function onChangeTag(input,tag) {
        alert("Changed a tag: ");
}	

$(document).ready(function(){		
    
        $('.tag').tagsInput({
            width: 'auto'
        });
        
	$('#search_block div.tagsinput input').focus();	
	
	$(".clear").click(function(){	
            $('.tag').importTags('');	
            $('#search_block div.tagsinput input').focus();	
	});							
	/*
	$(".star").click(function(){
            $(this).toggleClass("selected");		
	});	*/
	
	$(".city_block a").click(function(){
            $(".city_block").toggleClass("visible");		
	});	

	$(".city").click(function(){
            $(".city_block").toggleClass("visible");		
	});	
	
	$(".inline").colorbox({
		inline:true,
		onClosed: function(){
			$.cookie('destination', null, { path: '/'});
		}
	});
	
	$(".advertisement .phone_trig").click(function(){
            $(this).find('a').addClass("visible");
            $(this).find('a').unwrap();
            /*phoneStr = $.trim($(this).text());
            phoneStr = phoneStr.replace($(this).find('.full_number_text').text(),'');
            phoneHref = 'tel:' + phoneStr.replace(/[-\(\)\s]/gi, "");
            $(this).attr('href',phoneHref);*/
        });	

	$(".select_container input").click(function(){
            $(".info").addClass("visible");		
        });	
        
	$(document).mouseup(function () {        
		$(".info").removeClass("visible");			
        });

	$('select').styler({
                selectSearch: true
        });
	
	$(".wide_submenu .more").click(function(){
            $(".more_popup").toggleClass("visible");		
        });	

        $(".garage_block h4").click(function(){
            $(".popup_block").toggleClass("visible");		
        });	

        $(".popup_block .close").click(function(){
            $(".popup_block").toggleClass("visible");		
        });	
        
        var filter_keys = {
            apply_events: function ($jq, callback) {
                $jq.bind({
                    keyup: function (e) {
                        callback(e);
                    },
                    keydown: function (e) {
                        callback(e);
                    },
                    keypress: function (e) {
                        callback(e);
                    }
                });
            },

            get_float: function (num) {
                var new_num = parseFloat((num.replace(/[^0-9]/gi, "")).toString());
                new_num = isNaN(new_num) ? "" : new_num;
                return new_num;
            },

            numbers: function ($input, callback) {
                $input.each(function () {
                    var $this = $(this),
                        value;

                    filter_keys.apply_events($this, function (e) {
                        var type_event = e.type,
                            type;
                        value = $this.val();

                        type = String.fromCharCode(e.which);
                        if (type_event === "keypress") {

                            if (! /^\d+$/.test(type) && type) {
                                value = filter_keys.get_float(value);
                                $this.val( value );
                            }

                        } else {
                            value = $this.val();
                            if (! /^\d+$/.test(value)) {
                                value = filter_keys.get_float(value);
                                $this.val( value );
                            }
                        }

                        if (typeof callback == "function") callback($this ,value, e);

                    });
                });
            }
        };
    
        filter_keys.numbers($(".intonly"));
        
        /*var HeaderTop = $('#search_block').offset().top;
 
        $(window).scroll(function(){
            if( $(window).scrollTop() > HeaderTop ) {
                    $('#search_block').css({position: 'fixed', top: '0px'});
            }
            else {
                    $('#search_block').css({position: 'static'});
            }
        });*/
	openTab = function(){
            var s = $(this).offset().top;			
            var s2 = $(this).siblings(".subitems:visible").height();			
            var s4 = $('#menu').offset().top;
            var res = s - s2;		
            if (res < 0){
                res = s;
            };
            $('html, body').animate({scrollTop: res-s4}, 'slow'); 
            $(this).next(".subitems").slideToggle().siblings(".subitems:visible").slideUp();
            $(this).toggleClass("active");		
            $(this).siblings(".item").removeClass("active");					
        }
        $("#searchform .item").live('click',openTab);
});	

jQuery('input[placeholder], textarea[placeholder]').placeholder();	





