<?php
class ServiceUserIdentity extends UserIdentity {
    const ERROR_NOT_AUTHENTICATED = 3;

    /**
     * @var EAuthServiceBase the authorization service instance.
     */
    protected $service;
    private $_id;
    
    /**
     * Constructor.
     * @param EAuthServiceBase $service the authorization service instance.
     */
    public function __construct($service) {
        $this->service = $service;
    }
    
    /**
     * Authenticates a user based on {@link username}.
     * This method is required by {@link IUserIdentity}.
     * @return boolean whether authentication succeeds.
     */
    public function authenticate() {
		$user = new User('serviceLogin');
		$curUser = $user->model()->find(Yii::app()->request->getQuery('service').'=?',array($this->service->id));
		// если пользователя ещё нет в нашей базе, записываем его
		if(!$curUser) {
			$user->block = '0';
			$user->registerDate = date('Y-m-d H:i:s', time());
			$user->name = $this->service->getAttribute('name'); 
			$user->email = $this->service->getAttribute('email'); 
			$user->{Yii::app()->request->getQuery('service')} = $this->service->id;
			$user->confirm_email = 0;
			
			if($user->save(false)) {
				$curUser->id = $user->id;
				$curUser->confirm_email = $user->confirm_email;
			} else {
				throw new CHttpException(403, 'Ошибка сохранения в базу данных.');
			}
		}

		if(empty($curUser->email)) {
			$_SESSION['requestEmail'] = 1;
		}
		
		// если пользователь ужё заходил через соц. сеть, проверяем не заблокирован ли он
		if(isset($curUser->block) && $curUser->block == 0) {
                    $curUser->updateByPk($curUser->id, array('lastvisitDate' => date('Y-m-d H:i:s', time())));
                    $this->_id = $curUser->id;
                    $this->username = $curUser->name;
                    ServiceUserIdentity::setUserStates($curUser);
                    $this->errorCode = self::ERROR_NONE;		
		}
                elseif ($this->service->isAuthenticated && !isset($curUser->block)) {
                    $user->model()->updateByPk($curUser->id, array('lastvisitDate' => date('Y-m-d H:i:s', time())));
                    $this->username = $this->service->getAttribute('name');
                    $this->_id = $curUser->id;
                    ServiceUserIdentity::setUserStates($curUser);
                    $this->errorCode = self::ERROR_NONE;
                }
                else {
                    $_SESSION['requestEmail'] = 0;
                    $this->errorCode = self::ERROR_NOT_AUTHENTICATED;
                    //Yii::app()->user->setFlash('errorBlockUser','Ваш аккаунт заблокирован за нарушение правил портала!');
                    //Yii::app()->request->redirect(Yii::app()->createUrl('user/login'));
                    echo 'Ваш аккаунт заблокирован за нарушение правил портала!'; die;
                }
                return !$this->errorCode;
    }
	
	// устанавливает дополнительные переменные сессии для пользователя
	private function setUserStates($user) {
		Yii::app()->user->setState('email', $user->email);
		Yii::app()->user->setState('login', $user->username);
		Yii::app()->user->setState('service', $this->service->name);	// название сервиса, с которого вошли
		Yii::app()->user->setState('confirm', $user->confirm_email);	// подтверждение email
	}
	
    public function getId()
    {
        return $this->_id;
    }
}