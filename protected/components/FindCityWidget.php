<?php
class FindCityWidget extends CWidget {
    public $cityName;
    public $cityId;
    public function init(){
        $cityInfo = getCity();
        $this->cityName = $cityInfo['cityName'];
        $this->cityId = $cityInfo['cityId'];
    }
    
    public function run(){
        $this->render('application.views.widgets.cityChecker', array(
            'guessVisible' => Yii::app()->request->cookies['guessVisible']->value,
            'cityName' => $this->cityName,
            'cityId' => $this->cityId,
            'model' => new FormsCityChange()
        ));
    }
}
?>