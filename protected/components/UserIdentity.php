<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    private $_id;
    private $field = 'username';	// поле логина по умолчанию

    // авторизация при связке аккаунтов
    public function authenticateById($userid, $username) {
            $user=User::model()->findByPk($userid);
            $user->updateByPk($userid, array('lastvisitDate' => date('Y-m-d H:i:s', time())));
            $this->_id=$userid;
            $this->username=$username;
            UserIdentity::setUserStates($user);	// дополнительные переменные в сессию
    }

    // устанавливает дополнительные переменные сессии для пользователя
    private function setUserStates($user) {
            Yii::app()->user->setState('email', $user->email);
            Yii::app()->user->setState('login', $user->username);
            Yii::app()->user->setState('confirm', $user->confirm_email);	// подтверждение email
    }	
	
    public function authenticate()
    {
        if(is_array($this->username)) {
                $username=strtolower($this->username['email']);		
                $this->field='email';	// производить авторизацию по полю email (для входа на сайт)
        } else {
                $username=strtolower($this->username);	// для входа в админку
        }
        $user=User::model()->find($this->field.'=? AND block=?',array($username, 0));
		
        if($user===null)
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        elseif(!$user->validatePassword($this->password))
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        else
        {
            $user->updateByPk($user->id, array('lastvisitDate' => date('Y-m-d H:i:s', time())));
            $this->_id=$user->id;
            $this->username=$user->name;
            UserIdentity::setUserStates($user);	// дополнительные переменные в сессию
            $this->errorCode=self::ERROR_NONE;
        }
        return $this->errorCode==self::ERROR_NONE;
    }
 
    public function getId()
    {
        return $this->_id;
    }
}