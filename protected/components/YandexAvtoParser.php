<?php
class YandexAvtoParser extends CComponent
{
    private $mark_id;
    private $model_id;
    private $white_site = array('TopGear Russia', 'www.drive.ru', 'motor.ru', 'Авто Mail.Ru');
    
    
    private function curlConnect($url){
        $options = array(
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_USERAGENT      => "Mozilla/5.0 (Windows; U; Windows NT 5.1; de; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3",					
          CURLOPT_AUTOREFERER    => true,
          CURLOPT_CONNECTTIMEOUT => 120,
          CURLOPT_TIMEOUT        => 120,
          CURLOPT_MAXREDIRS      => 10,
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_COOKIEJAR	   => $user_cookie_file,
          CURLOPT_COOKIEFILE	   => $user_cookie_file
        );
        $ch      = curl_init( $url );
        curl_setopt_array( $ch, $options );            
        $page = curl_exec($ch);
        curl_close($ch);
        return $page;
    }

    public function Grab(){
        $page = $this->curlConnect('https://auto.yandex.ru/articles');
        $this->grabMarkPage($page);
    }    
    
    private function grabMarkPage($page){
        
        $document = phpQuery::newDocument($page);
        $linkMarks = $document->find('.list_content_all a');
        foreach($linkMarks as $mark){
            $mark = pq($mark);
            $hrefMark =  $mark->attr('href');
            $nameMark = preg_replace('/<span[^>]*>(.*?)<\/span>/i', '', $mark->html());
            $dbMark = MyMark::model()->findByAttributes(array('name' => trim($nameMark)));
            if($dbMark){
                $this->mark_id = $dbMark->mark_id;
                $pageMarks = $this->curlConnect('https://auto.yandex.ru'.$hrefMark);
                $this->grabModelPage($pageMarks);
            }else{
                continue;
            }
        }        
    }
    
    private function grabModelPage($page){
        $document = phpQuery::newDocument($page);
        $linkModel = $document->find('.list_content_popular a');
        foreach($linkModel as $model){
            $model = pq($model);
            $hrefModel =  $model->attr('href');
            $nameModel = preg_replace('/<span[^>]*>(.*?)<\/span>/i', '', $model->html());
            $dbModel = MyMmodel::model()->findByAttributes(array('name' => trim($nameModel),'mark_id' => $this->mark_id));
            if($dbModel){
                $this->model_id = $dbModel->model_id;
                $pageArticles = $this->curlConnect('https://auto.yandex.ru'.$hrefModel.'?sort=dtreviewed_desc');
                $this->grabArticlePage($pageArticles);
            }else{
                continue;
            }
        }        
    }
    
    private function grabArticlePage($page){
        $document = phpQuery::newDocument($page);
        $articleBlocks = $document->find('.b-articles .b-article');
        foreach($articleBlocks as $ab){
            $ab = pq($ab);
            $siteSource = trim($ab->find('.b-article__item_type_info a')->text());
            if( in_array($siteSource, $this->white_site) ){
                $siteAuthor = trim($ab->find('.b-article__item_type_info span.b-article__author')->text());
                //$siteDate = trim($ab->find('.b-article__item_type_info span.b-article__date')->text());
                $siteTitle = trim($ab->find('h3 a')->text());
                $siteHref = trim($ab->find('h3 a')->attr('href'));
                $checkSave = MyTestdrive::model()->countByAttributes(array('type'=>'text', 'link'=>$siteHref));
                if(!$checkSave){
                    $testdrive = new MyTestdrive;
                    $testdrive->type = 'text';
                    $testdrive->title = $siteTitle;
                    $testdrive->link = $siteHref;
                    $testdrive->author = $siteAuthor;
                    $testdrive->testdriveModel = $this->model_id;
                    $testdrive->published = 1;
                    if( $testdrive->save() ){
                        $testdrive->saveRelated('testdriveModel');
                    }
                }
                //Запускаем только одни раз для верхнего тест-драйва
                break;
           }
        }
    }
}
