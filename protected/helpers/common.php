<?php

function getCity(){
    $cookieCityName = Yii::app()->request->cookies['city_name']->value;
    $cookieCityId = Yii::app()->request->cookies['city_id']->value;
    if($cookieCityName && $cookieCityId && intval($cookieCityId)){
        return array('cityName'=>$cookieCityName,'cityId'=>$cookieCityId);
    }else{
        Yii::app()->request->cookies['guessVisible'] = new CHttpCookie('guessVisible', 1 ,array('expire'=>time()+60*60*24*180));//показывать "угадали?"
        return setCity();
    }
}

function setCity(){
    $DEFAULT_CITY_NAME = 'Санкт-Петербург';
    $DEFAULT_CITY_ID = 546;
    Yii::import('ext.SxGeo.SxGeo');
    $SxGeo = new SxGeo();
    $ip = CHttpRequest::getUserHostAddress();
    $cityData = $SxGeo->getCity($ip);
    if($cityData){
        $cityName = $cityData['city']['name_ru'];
        $checkCity = MyCity::model()->findByAttributes(array('city_name'=>$cityName));
        if(!empty($checkCity)){
            Yii::app()->request->cookies['city_name'] = new CHttpCookie('city_name', $checkCity->city_name,array('expire'=>time()+60*60*24*180));
            Yii::app()->request->cookies['city_id'] = new CHttpCookie('city_id', $checkCity->city_id,array('expire'=>time()+60*60*24*180));
            return array('cityName'=>$checkCity->city_name,'cityId'=>$checkCity->city_id);
        }
    }
    Yii::app()->request->cookies['city_name'] = new CHttpCookie('city_name', $DEFAULT_CITY_NAME,array('expire'=>time()+60*60*24*180));
    Yii::app()->request->cookies['city_id'] = new CHttpCookie('city_id', $DEFAULT_CITY_ID,array('expire'=>time()+60*60*24*180));
    return array('cityName'=>$DEFAULT_CITY_NAME,'cityId'=>$DEFAULT_CITY_ID);
}

function param($name, $default = null) {
	if (isset(Yii::app()->params[$name]))
		return Yii::app()->params[$name];
	else
		return $default;
}

function throw404(){
	throw new CHttpException(404, 'Запрашиваемая страница не найдена.');
}

function get_date_month_year($date) {
	$mounth_d = array(1=>'января',2=>'февраля',3=>'марта',4=>'апреля',5=>'мая',6=>'июня',7=>'июля',8=>'августа',9=>'сентября',10=>'октября',11=>'ноября',12=>'декабря');
	return date('d', $date).' '.$mounth_d[intval(date('m', $date))].' '.date('Y', $date);
}

function check_robot($code){
    if($code){
        $verifyUrl = 'https://www.google.com/recaptcha/api/siteverify';
        $params = array(
            "secret"=>Yii::app()->params['recaptcha_secret'],
            "response"=>$code,
            "remoteip"=>$_SERVER["REMOTE_ADDR"]
        );
        $response = file_get_contents($verifyUrl, false, stream_context_create(array(
            'http' => array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));
        
        $response = CJSON::decode($response,true);
        if ($response['success']) {
            return true;
        }
    }
    return false;
}

function rrmdir($dir) {
	if (is_dir($dir)) {
		$objects = scandir($dir);
		if($objects){
			foreach ($objects as $object) {
				if ($object != "." && $object != "..") {
					if (filetype($dir . "/" . $object) == "dir")
						rrmdir($dir . "/" . $object);
					else
						unlink($dir . "/" . $object);
				}
			}
		}
		reset($objects);
		rmdir($dir);
	}
}

//Находит и удаляет заданый файл в каталоге и подкаталогах
function findDelFile($dir, $tosearch) {
    $files = array_diff( scandir( $dir ), Array( ".", ".." ) );     
    foreach( $files as $d ) { 
            $path = $dir."/".$d;
            $path = str_replace('//', '/', $path);
            if( !is_dir($dir."/".$d) ) { 
                if ($d == $tosearch) unlink($path);
            } else { 
                $res = findDelFile($path, $tosearch); 
                if ($res) 
                    unlink($path);
            } 
    } 
    return false; 
}

//Возвращает данные маркеров карты по id контента
function getMarkers($id){
    $markers = Yii::app()->db->createCommand()
    ->select('*')
    ->from('content_location cl')
    ->join('content c', 'cl.entity_id=c.id')
    ->where('c.id=:id', array(':id'=>$id))
    ->queryAll();
    return $markers;

}

function multi_array_unique($array){
    foreach ($array AS $key => $item) { 
       $arrList[$key] = serialize($item); 
    } 
    $array = array_unique($arrList); 
    foreach ($array AS $key => $strItem) { 
       $array[$key] = unserialize($strItem); 
    } 
    return $array;
}
//Возвращает json маркеров карты по id контента
function getMarkersJson($id){
	$markers = getMarkers($id);
	$dataJson = array();
	if(!empty($markers)){
		foreach($markers as $k=>$m){
			$dataJson[$k]['coord'][0] = $m['lat'];
			$dataJson[$k]['coord'][1] = $m['lon'];
		}
	}
	return json_encode($dataJson);
}

function toBytes($str) {
	$val = trim($str);
	$last = strtolower($str[strlen($str) - 1]);
	switch ($last) {
		case 'g': $val *= 1024;
		case 'm': $val *= 1024;
		case 'k': $val *= 1024;
	}
	return $val;
}

function sendMail($to, $from, $subject, $message, $template = '') {
	$email = Yii::app()->email;
	$email->contentType = 'UTF-8';
	$email->to = $to; 
	$email->from = "Sibgr.ru <mail@sibgr.ru>";
	
	mb_internal_encoding("UTF-8");
	$email->subject = $subject;
	$email->message = $message;
	
	if($template) {
		$email->view = $template->view;
		$email->viewVars = array('data'=>$template->data);
	} else {
		$email->message .= '<br><br>--<br>С уважением, администрация сайта sibgr.ru';
	}
	if($email->send()) {
		return true;
	}
	return false;
}

function columnSizer($array,$cols){
    //  а теперь сам код
    $total = sizeof($array);         //  сколько у нас элементов
    $min   = floor($total / $cols);  //  минимальное количество элементов в столбце
    $extra = $total - $min * $cols;  //  "лишние" элементы

    $colsIndex = array();  //  массив с "пограничными" индексами для колонок
    $prevCol = 0;          //  количество элементов, которое уже распределено по колонкам
    for($q = 0; $q < $cols; $q++){
        //  если еще есть лишние элементы, то добавляем один из них к текущей колонке
        $colNum = $extra-- > 0 ? $min + 1 : $min;

        $prevCol += $colNum;
        $colsIndex[] = $prevCol;
    }

    $curCol = 0;  //  какую колонку выводим
    $groupArr = array(); // группированный массив
    foreach($array as $c => $val){

        if($c == $colsIndex[$curCol]){
          ++$curCol;
        }

        $groupArr[$curCol][] = $val;
    }
    return $groupArr;
}
