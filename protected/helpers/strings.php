<?php
function truncateText($text, $numOfWords = 10, $add = ''){
	if($numOfWords){
		$text = strip_tags($text, '<br/>');
		//$text = str_replace(array("\r", "\n"), '', $text);

		$lenBefore = strlen($text);
		if($numOfWords){
			if(preg_match("/(\S+\s*){0,$numOfWords}/", $text, $match))
				$text = trim($match[0]);
			if(strlen($text) != $lenBefore){
				$text .= $add;
			}
		}
	}

	return $text;
}