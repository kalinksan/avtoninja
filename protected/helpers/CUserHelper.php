<?php 
class CUserHelper {

    public function sendActivationKey($activationKey, $email) {
            $subject = 'Подтверждение e-mail на сайте Pushkin.ru';
            $message = 'Для подтверждение e-mail перейдите по ссылке: <a href="'.Yii::app()->request->hostInfo.'/user/activation/key/'.$activationKey.'">'.Yii::app()->request->hostInfo.'/user/activation/key/'.$activationKey.'</a><br />Если вы не добавляли e-mail на pushkin.ru, проигнорируйте это письмо.';
            sendMail($email, 'pushkin@info.ru', $subject, $message);
    }

    public function sendRegistrationMail($activationKey, $email, $name, $password) {
            $subject = 'Регистрация на сайте Pushkin.ru';
            $message = $name.', здравствуйте.<br />Ваш аккаунт успешно зарегистрирован на сайте <a href="'.Yii::app()->request->hostInfo.'">pushkin.ru</a><br />Данные для входа:<br />E-mail: '.$email.'<br />Пароль: '.$password.'<br /><br />Для завершения регистрации перейдите по ссылке <a href="'.Yii::app()->request->hostInfo.'/user/activation/key/'.$activationKey.'">'.Yii::app()->request->hostInfo.'/user/activation/key/'.$activationKey.'</a>';
            sendMail($email, 'pushkin@info.ru', $subject, $message);
    }

    // если сессии хранятся в базе
    public static function getSessionUser($user_id) {
            $row = Yii::app()->db->createCommand(array(
                    'select' => array('id'),
                    'from' => 'user_sessions',
                    'where' => 'user_id=:id',
                    'params' => array(':id'=>$user_id),
            ))->queryRow();		

            return $row['id'];
    }

    public static function genRandomPassword($length = 8) {
            $salt = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            $base = strlen($salt);
            $makepass = '';
            $random = CUserHelper::genRandomBytes($length + 1);
            $shift = ord($random[0]);

            for ($i = 1; $i <= $length; ++$i)
            {
                    $makepass .= $salt[($shift + ord($random[$i])) % $base];
                    $shift += ord($random[$i]);
            }

            return $makepass;	
    }
    
    public static function genRandomBytes($length = 16){
            $sslStr = '';
            if (
                function_exists('openssl_random_pseudo_bytes')
                && (version_compare(PHP_VERSION, '5.3.4') >= 0
                        || substr(PHP_OS, 0, 3) !== 'WIN'
                )
            ){
                $sslStr = openssl_random_pseudo_bytes($length, $strong);
                if ($strong){
                    return $sslStr;
                }
            }

            $bitsPerRound = 2;
            $maxTimeMicro = 400;
            $shaHashLength = 20;
            $randomStr = '';
            $total = $length;

            $urandom = false;
            $handle = null;
            if (function_exists('stream_set_read_buffer') && @is_readable('/dev/urandom'))
            {
                    $handle = @fopen('/dev/urandom', 'rb');
                    if ($handle)
                    {
                            $urandom = true;
                    }
            }

            while ($length > strlen($randomStr))
            {
                    $bytes = ($total > $shaHashLength)? $shaHashLength : $total;
                    $total -= $bytes;

                    $entropy = rand() . uniqid(mt_rand(), true) . $sslStr;
                    $entropy .= implode('', @fstat(fopen( __FILE__, 'r')));
                    $entropy .= memory_get_usage();
                    $sslStr = '';
                    if ($urandom)
                    {
                            stream_set_read_buffer($handle, 0);
                            $entropy .= @fread($handle, $bytes);
                    }
                    else
                    {
                            $samples = 3;
                            $duration = 0;
                            for ($pass = 0; $pass < $samples; ++$pass)
                            {
                                    $microStart = microtime(true) * 1000000;
                                    $hash = sha1(mt_rand(), true);
                                    for ($count = 0; $count < 50; ++$count)
                                    {
                                            $hash = sha1($hash, true);
                                    }
                                    $microEnd = microtime(true) * 1000000;
                                    $entropy .= $microStart . $microEnd;
                                    if ($microStart > $microEnd) {
                                            $microEnd += 1000000;
                                    }
                                    $duration += $microEnd - $microStart;
                            }
                            $duration = $duration / $samples;

                            $rounds = (int)(($maxTimeMicro / $duration) * 50);

                            $iter = $bytes * (int) ceil(8 / $bitsPerRound);
                            for ($pass = 0; $pass < $iter; ++$pass)
                            {
                                    $microStart = microtime(true);
                                    $hash = sha1(mt_rand(), true);
                                    for ($count = 0; $count < $rounds; ++$count)
                                    {
                                            $hash = sha1($hash, true);
                                    }
                                    $entropy .= $microStart . microtime(true);
                            }
                    }

                    $randomStr .= sha1($entropy, true);
            }

            if ($urandom)
            {
                    @fclose($handle);
            }

            return substr($randomStr, 0, $length);
    }	
}