<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
require_once( dirname(__FILE__) . '/db.php');
require_once( dirname(__FILE__) . '/../helpers/strings.php');
require_once( dirname(__FILE__) . '/../helpers/common.php');
Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');
Yii::setPathOfAlias('adm', dirname(__FILE__).'/../modules/admin');
$config =  array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'sourceLanguage'=>'en',
	'language'=>'ru',
	'charset' => 'utf-8',
	'name'=>'Avto.Ninja',
	'theme'=>'ninja',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
                'application.helpers.*',
                'application.models.*',
                'application.components.*',
                'application.extensions.image.Image',
                'application.modules.admin.extensions.bootstrap',
                'application.extensions.phpQuery.phpQuery.phpQuery',
                'ext.eoauth.*',
                'ext.eoauth.lib.*',
                'ext.lightopenid.*',
                'ext.eauth.services.*'            
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
                        'generatorPaths'=>array(
				'bootstrap.gii',
			),
			'class'=>'system.gii.GiiModule',
			'password'=>'321#@!ewq',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
                'admin',
                'review',
                'objects',
                'archive',
                'user',
		'testdrive'
	),

	// application components
	'components'=>array(
                'request'=>array(
                    'enableCsrfValidation'=>false,
                    'enableCookieValidation'=>true,
                ),
                'loid' => array(
                    'class' => 'ext.lightopenid.loid',
                ),
                'eauth' => array(
                    'class' => 'ext.eauth.EAuth',
                    'popup' => true, // Use the popup window instead of redirecting.
                    'services' => array( // You can change the providers and their classes.
                        'google' => array(
                            'class' => 'GoogleOAuthService',
                            'client_id' => '247066608291-gdja9u96l3u6johidk978n8nst5ojpts.apps.googleusercontent.com',
                            'client_secret' => 'I7S1RLrjIwRlX9N9UltSNPhW',
                        ),
                        'facebook' => array(
                            'class' => 'FacebookOAuthService',
                            'client_id' => '1535843766667490',
                            'client_secret' => '6d37497ac6a28a49a73c185b5c64f70d',
                        ),
                        'vkontakte' => array(
                            'class' => 'VKontakteOAuthService',
                            'client_id' => '4741669',
                            'client_secret' => 'nMsqZXw9nll1Y4FuE0rT',
                        ),
                    ),
                ),               
                'input'=>array(   
                        'class'         => 'CmsInput',  
                        'cleanPost'     => false,  
                        'cleanGet'      => false,   
		),
		'image'=>array(
		    'class'=>'application.extensions.image.CImageComponent',
		    // GD or ImageMagick
		    'driver'=>'GD',
		),            
		// uncomment the following to enable URLs in path-format
		'email'=>array(
			'class'=>'application.extensions.email.Email',
			'delivery'=>YII_DEBUG ? 'debug' : 'php', //Will use the php mailing function.  
		),
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName' => false,
			'rules'=>array(
                            '/' => 'objects/default/index',
                            'gii/<controller:\w+>/<action:\w+>/*' => 'gii/<controller>/<action>/',

                            'admin/<controller:\w+>/<action:\w+>/*' => 'admin/<controller>/<action>/',
                            'admin/<controller:\w+>/' => 'admin/<controller>/',

                            'thnx' => array('site/thnx'),
                            'sitemap.xml'=>'sitemap/index',

                            'user/profile/<action>/' => 'user/profile/<action>/',				
                            'user/profile/' => 'user/profile/',			
                            'user/<action:\w+>/<id:\d+>' => 'user/default/<action>/',		
                            'user/<action:\w+>/*' => 'user/default/<action>/',
                            'perenos/<action:\w+>' => 'perenos/<action>/',
                            //'objects' => array('objects/default/index'),

                            'test-drive/ReturnLink' => array('testdrive/default/ReturnLink'),
                            'test-drive' => array('testdrive/default/index'),
                            //'test-drive/<id:\d+>-<slug>' => array('testdrive/default/view'),
                            'test-drive/<mark>' => array('testdrive/default/ModelList'),
                            'test-drive/<mark>/<model>' => array('testdrive/default/list'),


                            'objects/<action:(create|upload|DelImage|preview|recomended)>' => array('objects/default/<action>'),
                            'objects/<id:\d+>' => array('objects/default/view'),	
                            'objects/<action:(update|delete|preview)>/<id>' => array('objects/default/<action>'),
                            'objects/dynamicmodel' => array('objects/default/dynamicmodel'),
                            'objects/orderCall' => array('objects/default/orderCall'),
                            'objects/writeLetter' => array('objects/default/writeLetter'),
                            'objects/askQuestion' => array('objects/default/askQuestion'),
                            'objects/page/<page>' => 'objects/default/index',          
                            
                            'contacts' => array('site/contacts'),
                            'about' => array('site/about'),
                            'certified' => array('site/certified'),
                            'love' => array('site/love'),
                            'otzivy' => array('site/otzivy'),
                            'press' => array('site/press'),
                            'prices' => array('site/prices'),
                            'privacy-policy' => array('site/privacyPolicy'),
                            'team' => array('site/team'),
                            'top-five-questions' => array('site/topFiveQuestions'),
                            'site/<controller:\w+>/<action:\w+>/*' => 'site/<controller>/<action>/',
                            'site/<controller:\w+>/' => 'site/<controller>/',

                            'review/<action:(create|upload|DelImage)>' => array('review/default/<action>'),
                            'review/<action:(update|delete)>/<id>' => array('review/default/<action>'),
                            'review/dynamicmodel' => array('review/default/dynamicmodel'),
                            'review/<mark>/<model>/<id:\d+>' => array('review/default/view'),	
                            'review/<mark>' => array('review/default/ModelList'),
                            'review/<mark>/<model>' => array('review/default/list'),

                            '<module:\w+>/page/<page>' => '<module>/default/index',
                            '<module:\w+>' => '<module>/default/index',

                            '<module:\w+>/<id>' => array('<module>/default/view'),

                            '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                            '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                            '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		
		// uncomment the following to use a MySQL database
		
		'db'=>$db,
		
		'log'=>array(
		    'class'=>'CLogRouter',
		    'routes'=>array(
			array(
			    // направляем результаты профайлинга в ProfileLogRoute (отображается
			    // внизу страницы)
			    'class'=>'CProfileLogRoute',
			    'levels'=>'profile',
			    'enabled'=>YII_DEBUG,
			),
		    ),  
                ),
		'bootstrap'=>array(
		    'class'=>'bootstrap.components.Bootstrap',
		),     
               /* 'authManager' => array(
                    // Будем использовать свой менеджер авторизации
                    'class' => 'PhpAuthManager',
                    // Роль по умолчанию. Все, кто не админы, модераторы и юзеры — гости.
                    'defaultRoles' => array('guest'),
                ),*/
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'loginUrl'=>array('user/login'),
		),
		'cache'=>array(
		    'class'=>'CFileCache',//Заглушка при необходимости CDummyCache
		   // 'connectionID' => 'db',
		),		
		'session' => array(
			//'class' => 'DbHttpSession',
			'timeout' => 60*60*24*30,
			//'sessionName' => 'Access',
			'autoStart' => false,
			//'cookieMode' => 'only',
			//'connectionID' => 'db',
			//'sessionTableName' => 'user_sessions',
		),            
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
                'adminBaseId'=>9,
		'adminEmail'=>'kalinksan@gmail.com, avto.ninja@yandex.ru',
		'fromEmail'=>'info@avtoninja.ru',
                'recaptcha_key'=>'6LflyAoTAAAAAOrWpYs8tn4EaaoVZd9LVoYZF54M',
                'recaptcha_secret'=>'6LflyAoTAAAAADVBnMxv3ZuMh38HTLk0R__XflNP',
	),
);
if(YII_DEBUG){
    $config['components']['kint'] = array('class' => 'ext.Kint.Kint');
    $config['preload'] = array('log','kint');
}
if(!YII_DEBUG){
    $config['components']['errorHandler'] = array('errorAction'=>'site/error');
}

return $config;