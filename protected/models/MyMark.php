<?php
class MyMark extends Mark
{	
        public function rules()
	{
		return array(
			array('name', 'required','except'=>'turboexp'),
			array('name', 'required','on'=>'turboexp'),
			array('popular', 'numerical', 'integerOnly'=>true),
			array('name, mname', 'length', 'max'=>255),
                        array('text_block, text_block_review, synonym, review_metadesc, testdrive_metadesc', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('mark_id, name, mname, popular, text_block, text_block_review, synonym, review_metadesc, testdrive_metadesc', 'safe', 'on'=>'search'),
		);
	}

        public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'automodels' => array(self::HAS_MANY, 'MyMmodel', 'mark_id'),
			'objects' => array(self::HAS_MANY, 'MyObjects', 'mark_id'),
                        'reviews' => array(self::HAS_MANY, 'Review', 'mark_id'),
		);
	}
        
	public function attributeLabels()
	{
		return array(
			'mark_id' => 'ID',
			'name' => 'Название марки',
			'mname' => 'Машинное имя',
                        'text_block' => 'Текст в тест-драйвах',
                        'text_block_review' => 'Текст в отзывах',
                        'synonym' => 'Синоним (елси несколько, то указывать через запятую)',
			'popular' => 'Популярная марка (будет сразу отображаться в фильтре)',
                        'review_metadesc' => 'Отзывы meta description',
			'testdrive_metadesc' => 'Тест-драйвы meta description',
		);
	}
	
	public function behaviors(){
		return array(
			'SlugBehavior' => array(
					'class' => 'application.models.behaviors.SlugBehavior',
					'slug_col' => 'mname',
					'title_col' => 'name',
					'max_slug_chars' => 10,
					'overwrite' => true
			),             
		);
	}
	
        public static function resave(){
            $marks = MyMark::model()->findAll();
            foreach($marks as $mark){
                $mark->save();  
            }
        }


        public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('mark_id',$this->mark_id,true);
		$criteria->compare('our',$this->our);
		$criteria->compare('popular',$this->popular);
		$criteria->compare('mname',$this->mname,true);
                $criteria->compare('text_block',$this->text_block,true);
                $criteria->compare('synonym',$this->synonym,true);
                $criteria->compare('text_block_review',$this->text_block_review,true);
                $criteria->compare('review_metadesc',$this->review_metadesc,true);
		$criteria->compare('testdrive_metadesc',$this->testdrive_metadesc,true);
		$criteria->compare('name',$this->name,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=>100,
			),		
			'criteria'=>$criteria,
		));
	}		
		
	public function defaultScope()
	{
		return array(
			'order' => 'popular DESC',
		);      

	}
}
