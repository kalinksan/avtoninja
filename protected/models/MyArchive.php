<?php
class MyArchive extends Archive
{
        public $text_size;
	public function rules()
	{
		return array(
			array('title', 'required'),
                        array('noindex', 'numerical', 'integerOnly'=>true),
			array('title, oldurl, metatitle, metakey, canonical', 'length', 'max'=>255),
			array('type', 'length', 'max'=>50),
			array('fulltext, metadesc', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, fulltext, oldurl, type, metatitle, metakey, metadesc, noindex, canonical', 'safe', 'on'=>'search'),
		);
	}
	
        public function afterFind() {
            $this->text_size = iconv_strlen(trim(strip_tags($this->fulltext)));
            parent::afterFind();
        }
        
        public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('fulltext',$this->fulltext,true);
		$criteria->compare('oldurl',$this->oldurl,true);
		$criteria->compare('text_size',$this->text_size,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('metatitle',$this->metatitle,true);
		$criteria->compare('metakey',$this->metakey,true);
		$criteria->compare('metadesc',$this->metadesc,true);
                $criteria->compare('noindex',$this->noindex);
                $criteria->compare('canonical',$this->canonical,true);

		return new CActiveDataProvider($this, array(
                    'pagination'=>array(
                            'pageSize'=>100,
                    ),
                    'criteria'=>$criteria,
                    'sort'=>array(
                        'attributes'=>array(
                            'fulltext'=>array(
                                'asc'=>'length(`t`.`fulltext`)',
                                'desc'=>'length(`t`.`fulltext`) DESC',
                            ),
                            'title',
                            'type'
                        ),
                    ),
		));
	}
        
        public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Заголовок',
			'fulltext' => 'Текст',
			'oldurl' => 'Старый url с avto-avto.ru',
			'type' => 'Тип',
			'noindex' => 'Скрыть от индексации',
			'metatitle' => 'Metatitle',
			'metakey' => 'Metakey',
			'metadesc' => 'Metadesc',
			'canonical' => 'Canonical url',
                        'text_size' => 'Размер'
		);
	}

        public function scopes() {
            return array(
                'published'=>array(
                    'select'=>array('id'),
                    'condition'=>'t.noindex = 0',
                ),
            );
        }

        private $_url;

        public function getUrl(){
            if ($this->_url === null)
                $this->_url = Yii::app()->createUrl('archive/view', array('id'=>$this->id));
            return $this->_url;
        }        
        
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
