<?php

class MyObjects extends Objects {

    public $cost_from;
    public $cost_to;
    public $year_from;
    public $year_to;
    public $volume_from;
    public $volume_to;
    public $km_age_from;
    public $km_age_to;
    public $with_photo;
    public $nonpop_mark_id;
    
    public $searchMark;
    public $searchModel;
    public $searchCity;
    public static $body_type = array(
        'sedan' => 'Седан',
        'hetchback' => 'Хэтчбек',
        'universal' => 'Универсал',
        'vnedorojnik' => 'Внедорожник',
        'krossover' => 'Кроссовер',
        'pikap' => 'Пикап',
        'kupe' => 'Купе',
        'kabriolet' => 'Кабриолет',
        'miniven' => 'Минивэн',
        'furgon' => 'Фургон',
        'mikroavtobus' => 'Микроавтобус'
    );
    public static $color = array(
        'beige' => 'Бежевый',
        'white' => 'Белый',
        'blue' => 'Голубой',
        'yellow' => 'Желтый',
        'green' => 'Зеленый',
        'gold' => 'Золотой',
        'brown' => 'Коричневый',
        'red' => 'Красный',
        'orange' => 'Оранжевый',
        'pink' => 'Розовый',
        'silver' => 'Серебряный',
        'grey' => 'Серый',
        'darkblue' => 'Синий',
        'violet' => 'Фиолетовый',
        'black' => 'Черный',
    );
    public static $transmission = array(
        'akpp' => 'Автомат',
        'mkpp' => 'Механика'
    );
    public static $transmissionMin = array(
        'akpp' => 'AT',
        'mkpp' => 'MT'
    );
    public static $engine = array(
        'benzin' => 'Бензин',
        'gibrid' => 'Гибрид',
        'dizel' => 'Дизель',
        'electro' => 'Электро'
    );
    public static $privod = array(
        'peredmiy_privod' => 'Передний',
        'zadniy_privod' => 'Задний',
        'polniy' => 'Полный'
    );
    public static $wheel = array(
        'left' => 'Левый',
        'right' => 'Правый',
    );
    /*public static $state = array(
        'excellent' => 'Отличное',
        'average' => 'Среднее',
        'repair' => 'Требует ремонта',
        'good' => 'Хорошее'
    );*/

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('mark_id, model_id, user_name, user_email, user_phone, city_id, year, cost, km_age, body_type, color, volume, transmission, engine, privod, wheel, state, description', 'required', 'except' => 'turboexp'),
            array('mark_id, model_id, user_name, user_phone, city_id, year, cost, km_age, color, volume, transmission, engine, wheel, state', 'required', 'on' => 'turboexp'),
            array('user_id, cost_from, cost_from, cost_to, year_to, year_from, volume, km_age, km_age_from, km_age_to, mark_id, state, model_id, with_photo, nonpop_mark_id,volume_from,volume_to, published, object_status', 'numerical', 'integerOnly' => true),
            array('user_phone, city_id, metro, cost, turbodealer_id', 'length', 'max' => 20),
            array('user_name', 'length', 'max' => 255),
            array('service', 'length', 'max' => 100),
            array('user_email, body_type, color, transmission, engine, privod, wheel', 'length', 'max' => 50),
            array('user_email', 'email'),
            array('description, check_data', 'safe'),
            array('description', 'safe', 'on' => 'turboexp'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, mark_id, model_id, user_id, user_name, user_email, user_phone, city_id, metro, year, cost, km_age, body_type, color, volume, transmission, engine, privod, wheel, state, description, created, updated, service, published, object_status', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'city' => array(self::BELONGS_TO, 'MyCity', 'city_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'mark' => array(self::BELONGS_TO, 'MyMark', 'mark_id'),
            'model' => array(self::BELONGS_TO, 'MyMmodel', 'model_id'),
            'objectsImages' => array(self::HAS_MANY, 'ObjectsImages', 'entity_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'mark_id' => 'Марка',
            'model_id' => 'Модель',
            'user_id' => 'User_id',
            'user_name' => 'Ваше имя',
            'user_email' => 'Элекстронная почта',
            'user_phone' => 'Телефон',
            'city_id' => 'Город',
            'metro' => 'Метро',
            'year' => 'Год выпуска',
            'cost' => 'Цена',
            'km_age' => 'Пробег',
            'body_type' => 'Тип кузова',
            'color' => 'Цвет',
            'volume' => 'Объем двигателя',
            'transmission' => 'Коробка передач',
            'engine' => 'Тип двигателя',
            'privod' => 'Привод',
            'wheel' => 'Руль',
            'state' => 'Проверено Авто.Ниндзей',
            'description' => 'Описание объявления',
            'created' => 'Добавлено',
            'updated' => 'Обновлено',
            'with_photo' => 'С фото',
            'turbodealer_id' => 'Turbodealer',
            'service' => 'Service',
            'published' => 'Публикация',
            'object_status' => 'Object Status',
            'check_data' => 'Check Data',
        );
    }

    public function beforeSave() {
        if (!$this->user_id) {
            $this->user_id = Yii::app()->user->id;
        }
        if ($this->isNewRecord && !$this->created) {
            $this->created = new CDbExpression('NOW()');
        }
        $this->updated = new CDbExpression('NOW()');
        return parent::beforeSave();
    }

    protected function beforeDelete() {
        if (!empty($this->objectsImages)) {
            $images = $this->objectsImages;
            foreach ($images as $img) {
                findDelFile('images/objects/', $img->image);
            }
        }
        return parent::beforeDelete();
    }

    public static function CityChoices() {
        return CHtml::listData(MyCity::model()->findAll(), 'city_id', 'city_name');
    }

    public function scopes() {
        return array(
            'published'=>array(
                'select'=>array('id'),
                'condition'=>'t.published = 1',
            )
        );
    }
 
    private $_url;
 
    public function getUrl(){
        if ($this->_url === null)
            $this->_url = Yii::app()->createUrl('objects/view', array('id'=>$this->id));
        return $this->_url;
    }
    
    public static function YearsChoices() {
        $y = date('Y');
        $years = array();
        while ($y >= 1960) {
            $years[$y] = $y;
            $y--;
        }
        return $years;
    }

    public static function VolumeChoices() {
        $v = 600;
        $volume = array();
        while ($v <= 8200) {
            $volume[$v] = $v . " см&sup3;";
            $v = $v + 100;
        }
        return $volume;
    }

    public static function LoadTurboD() {
        MyObjects::turboUpdateStatus();
        $turbo_user_id = 19;
        $url = 'http://turbodealer.ru/export/ninja.xml';
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $xml = curl_exec($ch);
        curl_close($ch);
        $xml = simplexml_load_string($xml);
        $offers = $xml->offers;
        $maxPart = 20; //по сколько записей будем загружать
        $k = 0;
        foreach ($offers->offer as $offer) {
           /* if($k>=$maxPart){
                return;
            } */
            $needUpdate = false;  
            $uptDate = Yii::app()->dateFormatter->format("y-MM-dd HH:m:ss",(string)$offer->{'update-date'});
            $checkObj = MyObjects::model()->findByAttributes(
                    array(
                        'turbodealer_id' => (int) $offer->id, 
                        'service' => 'Turbodealer.ru'
                        )
                    );
            if (!empty($checkObj)){
                if( $uptDate <> $checkObj->updated ){
                    $needUpdate = true;
                }
            }
            if ( empty($checkObj) || $needUpdate ) {    
                $checkCity = MyCity::model()->findByAttributes(array('city_name' => (string) $offer->{'seller-city'}));
                if (empty($checkCity)) {
                    $city = new MyCity();
                    $city->city_name = (string)$offer->{'seller-city'};
                    $checkCity = $city->save(false);
                    $checkCity = $city;
                }
                $city_id = $checkCity->city_id;

                $checkMark = MyMark::model()->findByAttributes(array('name' => (string) $offer->mark));
                if (empty($checkMark)) {
                    //print (string)$offer->mark.'<br>';
                    continue;
                }
                $mark_id = $checkMark->mark_id;

                $checkModel = MyMmodel::model()->findByAttributes(array('mark_id' => $mark_id, 'name' => (string) $offer->model));
                if (empty($checkModel)) {
                    //print (string)$offer->model.'<br>';
                    continue;
                }
                $model_id = $checkModel->model_id;
                if ($needUpdate) {
                    $object = $checkObj;
                } else {
                    $object = new MyObjects();
                }
                $object->mark_id = $mark_id;
                $object->model_id = $model_id;
                $object->user_name = (string) $offer->seller;
                $object->user_id = $turbo_user_id;
                $object->user_phone = (string) $offer->{'seller-phone'};
                $object->city_id = $city_id;
                $object->year = (int) $offer->year;
                $object->cost = (int) $offer->price;
                $object->km_age = (int) $offer->run;
                $object->body_type = array_search((string) $offer->{'body-type'}, MyObjects::$body_type);
                $object->color = array_search((string) $offer->color, MyObjects::$color);
                $object->transmission = array_search((string) $offer->transmission, MyObjects::$transmission);
                $object->engine = array_search((string) $offer->{'engine-type'}, MyObjects::$engine);
                $object->privod = array_search((string) $offer->{'gear-type'}, MyObjects::$privod);
                $object->wheel = array_search((string) $offer->{'steering-wheel'}, MyObjects::$wheel);
                $object->state = 0;
                $object->volume = (int) $offer->displacement;
                $object->turbodealer_id = (int) $offer->id;
                $object->description = (string) $offer->{'additional-info'};
                $object->created = date('Y-m-d H:i:s', strtotime((string) $offer->date));
                $object->updated = date('Y-m-d H:i:s', strtotime((string)$offer->{'update-date'}));
                $object->service = 'Turbodealer.ru';
                $object->scenario = 'turboexp';
                if (!$needUpdate) {
                    $exp_images = array();
                    $count_img = count($offer->image);
                    if ($count_img) {
                        for ($i = 0; $i <= $count_img - 1;  ++$i) {
                            $exp_images[] = (string) $offer->image[$i];
                        }
                    }
                    if ($object->validate()) {

                        if ($object->save(false)) {
                            if (!empty($exp_images)) {
                                foreach ($exp_images as $image_url) {
                                    MyObjects::turboUpload($object->id, $image_url);
                                }
                            }
                            $k++;
                        }
                    } else {
                        continue;
                        print_r($object);
                        print_r($object->getErrors());
                        die;
                    }
                } else {
                    if ($object->validate()) {
                        if ($object->update(false)) {
                            $k++;
                        }
                    } else {
                        continue;
                    }
                }
            }
        }
    }

    private static function turboUpdateStatus() {
        $webRoot = str_replace('/protected', '', Yii::getPathOfAlias('webroot'));
        $url = 'http://turbodealer.ru/export/ninja.xml';
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $xml = curl_exec($ch);
        curl_close($ch);
        $xml = simplexml_load_string($xml);
        $offers = $xml->offers;
        $offersArr = array();
        $objectsArr = array();
        foreach ($offers->offer as $offer) {
            $offersArr[(int) $offer->id] = (int) $offer->id;
        }
        $count = count($offersArr);
        if ($count) {
            $objects = MyObjects::model()->findAll(array("select" => "id,turbodealer_id", "condition" => "service='Turbodealer.ru' AND object_status=1"));
            foreach ($objects as $obj) {
                $objectsArr[$obj->turbodealer_id] = $obj->id;
            }
            foreach ($objectsArr as $offerId => $objId) {
                if (!isset($offersArr[$offerId])) {
                    $object = MyObjects::model()->findByPk($objId);
                    $object->published = 0;
                    $object->object_status = 0;
                    $object->update();
                    if (!empty($object->objectsImages)) {
                        $images = $object->objectsImages;
                        foreach ($images as $img) {
                            findDelFile($webRoot . '/images/objects/', $img->image);
                        }
                        ObjectsImages::model()->deleteAll("`entity_id` = {$objId}");
                    }
                    $object->update();
                }
            }
        }
    }

    private static function turboUpload($id, $img_url) {
        $pathinfo = pathinfo($img_url);
        $ch = curl_init($img_url);
        $webRoot = str_replace('/protected', '', Yii::getPathOfAlias('webroot'));
        $path = $webRoot . "/images/turboexp/" . $pathinfo['filename'] . '.' . mb_strtolower($pathinfo['extension']);
        $fp = fopen($path, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        $folder = $webRoot . '/images/objects/';
        //$filename = $pathinfo['filename'];
        $filename = md5(uniqid());
        $ext = $pathinfo['extension'];
        $filename = $filename . '.' . mb_strtolower($ext);
        $image = new Image($path);
        $image->save($folder . $filename);
        
        @mkdir($folder . 'small');
        @mkdir($folder . 'middle');
        @mkdir($folder . 'big');

        $image = new Image($path);
        $image->centeredpreview(79, 53);
        $image->save($folder . 'small/' . $filename);

        $image = new Image($path);
        $image->centeredpreview(171, 126);
        $image->save($folder . 'middle/' . $filename);

        $image = new Image($path);
        $image->centeredpreview(331, 242);
        $image->save($folder . 'big/' . $filename);

        if ($id > 0) {
            $advertImage = new ObjectsImages();
            $advertImage->image = $filename;
            $advertImage->entity_id = intVal($id);
            $advertImage->save(false);
        }
    }

    public static function OuterTurboD(){
        $objects = MyObjects::model()->findAll(array("select" => "id,turbodealer_id", "condition" => "service='Turbodealer.ru' AND object_status=1"));
        $count = count($objects);
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $result = $xml->createElement('Result');
        $result->setAttribute('CountOk',$count);
        $result->setAttribute('CountAll',$count);
        $result->setAttribute('CountError',0);
        $ads = $xml->createElement('Ads');
        $ads = $result->appendChild($ads);
        foreach($objects as $object){
            $ad = $xml->createElement('Ad');
            $ad->setAttribute('result', 'ok');
            $ad->setAttribute('Id', $object->turbodealer_id);
            $ad->setAttribute('САЙТId', $object->id);
            $ad->setAttribute('Url', "http://avto.ninja/objects/{$object->id}");
            $ads->appendChild($ad);
        }
        $xml->appendChild($result);
        file_put_contents(Yii::app()->basePath . '/../turboDealerReport.xml', $xml->saveXML());
        return $xml;
    }
    
    public static function OuterMailRu(){
        $objects = MyObjects::model()->findAll(array("condition" => "object_status=1 AND published=1", "order"=>"updated DESC"));
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $main = $xml->createElement('auto-catalog');
        $date = $xml->createElement('creation-date');
        $date->appendChild($xml->createTextNode(date(DATE_W3C, time()))); 
        $host = $xml->createElement('host');
        $host->appendChild($xml->createTextNode(Yii::app()->request->hostInfo));
        $main->appendChild($date);
        $main->appendChild($host);
        
        $offers = $xml->createElement('offers');
        foreach($objects as $object){
            $offer = $xml->createElement('offer');
            $offer->setAttribute('type', 'commercial');
            
            $date = $xml->createElement('date');
            $date->appendChild($xml->createTextNode(date(DATE_W3C, strtotime($object->created))));
            $offer->appendChild($date);
            
            $mark = $xml->createElement('mark');
            $mark->appendChild($xml->createTextNode($object->mark->name));
            $offer->appendChild($mark);
            
            $model = $xml->createElement('model');
            $model->appendChild($xml->createTextNode($object->model->name));
            $offer->appendChild($model);     
            
            $year = $xml->createElement('year');
            $year->appendChild($xml->createTextNode($object->year));
            $offer->appendChild($year); 
            
            $city = $xml->createElement('city');
            $city->appendChild($xml->createTextNode($object->city->city_name));
            $offer->appendChild($city); 
            
            $user_phone = $xml->createElement('user_phone');
            $user_phone->appendChild($xml->createTextNode($object->user_phone));
            $offer->appendChild($user_phone); 
            
            $cost = $xml->createElement('cost');
            $cost->appendChild($xml->createTextNode($object->cost));
            $offer->appendChild($cost); 
            
            $currencyType = $xml->createElement('currency-type');
            $currencyType->appendChild($xml->createTextNode('руб.'));
            $offer->appendChild($currencyType);    
            
            $kmAge = $xml->createElement('run');
            $kmAge->appendChild($xml->createTextNode($object->km_age));
            $offer->appendChild($kmAge);
            
            $metric = $xml->createElement('run-metric');
            $metric->appendChild($xml->createTextNode('км'));
            $offer->appendChild($metric); 
            
            if(isset(self::$body_type[$object->body_type])){
                $bodyType = $xml->createElement('body-type');
                $bodyType->appendChild($xml->createTextNode(self::$body_type[$object->body_type]));
                $offer->appendChild($bodyType);
            }
            
            $engineType = $xml->createElement('engine-type');
            $engineType->appendChild($xml->createTextNode(self::$engine[$object->engine]));
            $offer->appendChild($engineType);     
            
            $displacement = $xml->createElement('displacement');
            $displacement->appendChild($xml->createTextNode($object->volume));
            $offer->appendChild($displacement); 
            
            if(isset(self::$privod[$object->privod])){
                $gearType = $xml->createElement('gear-type');
                $gearType->appendChild($xml->createTextNode(self::$privod[$object->privod]));
                $offer->appendChild($gearType);
            }
            
            $transmission = $xml->createElement('transmission');
            $transmission->appendChild($xml->createTextNode(self::$transmission[$object->transmission]));
            $offer->appendChild($transmission);     
            
            $wheel = $xml->createElement('steering-wheel');
            $wheel->appendChild($xml->createTextNode(self::$wheel[$object->wheel]));
            $offer->appendChild($wheel);
            
            $date = $xml->createElement('update-date');
            $date->appendChild($xml->createTextNode(date(DATE_W3C, strtotime($object->updated))));
            $offer->appendChild($date);
            
            $seller = $xml->createElement('seller');
            $seller->appendChild($xml->createTextNode($object->user_name));
            $offer->appendChild($seller);    
            
            $color = $xml->createElement('color');
            $color->appendChild($xml->createTextNode(self::$color[$object->color]));
            $offer->appendChild($color); 
            
            $url = $xml->createElement('murl');
            $url->appendChild($xml->createTextNode(Yii::app()->request->hostInfo . '/objects/'. $object->id));
            $offer->appendChild($url);
            
            $url = $xml->createElement('url');
            $url->appendChild($xml->createTextNode(Yii::app()->request->hostInfo . '/objects/'. $object->id));
            $offer->appendChild($url);
            
            $city = $xml->createElement('car-location');
            $city->appendChild($xml->createTextNode($object->city->city_name));
            $offer->appendChild($city);
            
            $additionalInfo = $xml->createElement('additional-info');
            $additionalInfo->appendChild($xml->createCDATASection($object->description));
            $offer->appendChild($additionalInfo);
            foreach($object->objectsImages as $imgObj){
                if(file_exists(Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'objects' . DIRECTORY_SEPARATOR . $imgObj->image)){ 
                    $image = $xml->createElement('image');
                    $image->appendChild($xml->createTextNode(Yii::app()->request->hostInfo . '/images/objects/' . $imgObj->image ));
                    $offer->appendChild($image);
                }
            }

            $offers->appendChild($offer);
        } 
        
        $main->appendChild($offers);
        $xml->appendChild($main);
        
        file_put_contents(Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'fromAvtoNinja.xml', $xml->saveXML());
        return $xml;
    }
    
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('mark_id',$this->mark_id,true);
		$criteria->compare('model_id',$this->model_id,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('user_email',$this->user_email,true);
		$criteria->compare('user_phone',$this->user_phone,true);
		$criteria->compare('city_id',$this->city_id,true);
		$criteria->compare('metro',$this->metro,true);
		$criteria->compare('year',$this->year);
		$criteria->compare('cost',$this->cost,true);
		$criteria->compare('km_age',$this->km_age,true);
		$criteria->compare('body_type',$this->body_type,true);
		$criteria->compare('color',$this->color,true);
		$criteria->compare('volume',$this->volume);
		$criteria->compare('transmission',$this->transmission,true);
		$criteria->compare('engine',$this->engine,true);
		$criteria->compare('privod',$this->privod,true);
		$criteria->compare('wheel',$this->wheel,true);
		$criteria->compare('state',$this->state);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('turbodealer_id',$this->turbodealer_id,true);
		$criteria->compare('service',$this->service,true);
		$criteria->compare('published',$this->published);
		$criteria->compare('object_status',$this->object_status);
                $criteria->compare('check_data',$this->check_data,true);
                $criteria->compare('mark.name', $this->searchMark->name, true);
                $criteria->compare('city.city_name', $this->searchCity->city_name, true);
                $criteria->compare('model.name', $this->searchModel->name, true);
                $criteria->with = array('mark','model','city');
                $criteria->group = 't.id';
                $criteria->together = true;
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>array(
                            'pageSize'=>100,
			),
                        'sort'=>array(
                            'defaultOrder'=>'t.published DESC, t.created DESC',
                            'attributes'=>array(
                                'mark.name'=>array(
                                    'asc'=>'mark.name',
                                    'desc'=>'mark.name DESC',
                                ),
                                'model.name'=>array(
                                    'asc'=>'model.name',
                                    'desc'=>'model.name DESC',
                                ),
                                'city.city_name'=>array(
                                    'asc'=>'city.city_name',
                                    'desc'=>'city.city_name DESC',
                                ),
                                '*',
                            ),
                        ),
		));
	}    
    
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Objects the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
