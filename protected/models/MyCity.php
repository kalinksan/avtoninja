<?php
class MyCity extends City
{
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                        array('sorter', 'numerical', 'integerOnly'=>true),
			array('city_name, city_locative', 'length', 'max'=>100),
			array('city_url', 'length', 'max'=>63),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, city_name, city_locative, city_url, sorter', 'safe', 'on'=>'search'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'city_name' => 'Город',
			'city_url' => 'City Url',
                        'sorter' => 'Sorter',
                        'city_locative' => 'City Locative',
		);
	}
        
        public function defaultScope()
        {
            return array(
                'order' => 'sorter DESC, city_name ASC',
            );      

        }        
        
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}