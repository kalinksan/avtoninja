<?php

/**
 * This is the model class for table "testdrive".
 *
 * The followings are the available columns in table 'testdrive':
 * @property string $id
 * @property string $title
 * @property string $link
 * @property string $type
 * @property string $introtext
 * @property string $author
 * @property string $main_image
 * @property string $created
 * @property string $changed
 * @property integer $published
 *
 * The followings are the available model relations:
 * @property Automodel[] $automodels
 */
class Testdrive extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'testdrive';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, type', 'required'),
			array('published', 'numerical', 'integerOnly'=>true),
			array('title, link, main_image', 'length', 'max'=>255),
			array('type', 'length', 'max'=>50),
			array('author', 'length', 'max'=>100),
			array('introtext, created, changed', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, link, type, introtext, author, main_image, created, changed, published', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'automodels' => array(self::MANY_MANY, 'Automodel', 'testdrive_model(testdrive_id, model_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'link' => 'Link',
			'type' => 'Type',
			'introtext' => 'Introtext',
			'author' => 'Author',
			'main_image' => 'Main Image',
			'created' => 'Created',
			'changed' => 'Changed',
			'published' => 'Published',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('introtext',$this->introtext,true);
		$criteria->compare('author',$this->author,true);
		$criteria->compare('main_image',$this->main_image,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('changed',$this->changed,true);
		$criteria->compare('published',$this->published);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Testdrive the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
