<?php
class MyTestdrive extends Testdrive
{
        public $types = array(
            'text'=>'текстовый',
            'video'=>'видео'
            );
        
	public function beforeDelete(){
		if(!empty($this->main_image) && $this->main_image<>'NULL'){
			findDelFile('images/testdrive',$this->main_image);
		}
		return parent::beforeDelete();
	}
        
        public function relations()
	{
		return array(
			'testdriveModel' => array(self::MANY_MANY, 'MyMmodel', 'testdrive_model(testdrive_id, model_id)'),
		);
	}
        
	public function beforeSave() {
		if ($this->isNewRecord){
			$this->created = new CDbExpression('NOW()');
		}
		$this->changed = new CDbExpression('NOW()');
		
		return parent::beforeSave();
	}
        
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, type, testdriveModel', 'required'),
			array('published', 'numerical', 'integerOnly'=>true),
			array('title, link, main_image', 'length', 'max'=>255),
			array('type', 'length', 'max'=>50),
                        array('author', 'length', 'max'=>100),
			array('link', 'url', 'allowEmpty'=>true,'validateIDN'=>true),
			array('introtext, created, changed', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, link, type, introtext, main_image, created, changed, published, author', 'safe', 'on'=>'search'),
		);
	}

	public static function getIndexMenu($marks){	    
		$group = array();
		$groupPopular = array();
		$marksPopular = array();
                // Разбиваем массив категорий на 4 части для вывода
		$menuGroup = columnSizer($marks, 4);
		//Подготовка массива к выводу в виджет меню
		foreach($menuGroup as $gr => $menu){
			foreach($menu as $mark){
				$group[$gr][] = array(
					'label'=>$mark->name,
					'url'=>Yii::app()->createUrl('/testdrive/default/ModelList', array('mark' => $mark->mname)),
                                        'linkOptions'=>array('class'=>$mark->popular ? 'popular' : ''),
                                        'template'=>'{menu}&nbsp;<span>' . self::countTestdrivesForMark($mark) . '</span>'
					);
                                if($mark->popular){
                                    $marksPopular[] = $mark;
                                }
			}
		}
                $menuGroupPopular = columnSizer($marksPopular, 4);
		foreach($menuGroupPopular as $gr => $menu){
			foreach($menu as $mark){
				$groupPopular[$gr][] = array(
					'label'=>$mark->name,
					'url'=>Yii::app()->createUrl('/testdrive/default/ModelList', array('mark' => $mark->mname)),
                                        'template'=>'{menu}&nbsp;<span>' . self::countTestdrivesForMark($mark) . '</span>'
					);
			}
		}                
		return array('allMarks'=>$group,'popularMarks'=>$groupPopular);		
	}        
        
        private static function countTestdrivesForMark($mark){
            $k = 0;
            foreach($mark->automodels as $model){
                foreach($model->testdrives as $testd){
                   $k++; 
                }
            }
            return $k;
        }
        
        public static function getIndexModelList($marks){
            $group = array();
            // Разбиваем массив категорий на 4 части для вывода
            $menuGroup = columnSizer($marks->automodels, 4);
            //Подготовка массива к выводу в виджет меню
            foreach($menuGroup as $gr => $menu){
                    foreach($menu as $model){
                            $group[$gr][] = array(
                                    'label'=>$model->name,
                                    'url'=>Yii::app()->createUrl('/testdrive/default/list', array('model' => $model->mname,'mark' => $marks->mname)),
                                    'template'=>'{menu}&nbsp;<span>' . count($model->testdrives, COUNT_RECURSIVE) . '</span>'
                                    );
                    }
            }    
            return $group;
        }
        
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Название',
                        'link' => 'Ссылка',
                        'type' => 'Тип тест-драйва',
                        'testdriveModel' => 'Модель',
                        'introtext' => 'Анонс',
                        'author' => 'Автор',
			'main_image' => 'Превью',
                        'published' => 'Публикация',
		);
	}
        
        public function attributeWidgets()
	{
	    return array(
		array('main_image','file'),
		array('created','disabled'),
		array('changed','disabled'),
	    );
	}
	
        public function behaviors(){
		return array(
                    'ESaveRelatedBehavior' => array(
                        'class' => 'application.models.behaviors.ESaveRelatedBehavior'
                    ),
                  /*  'galleryBehavior' => array(
                        'class' => 'application.models.behaviors.GalleryBehavior',
                        'idAttribute' => 'gallery_id',
                        'versions' => array(
                            'big' => array(
                                'resize' => array(800, null),
                            )
                        ),
                        'name' => false,
                        'description' => false,
                    )    */               
		);
	}
        
        public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('author',$this->author,true);
                $criteria->compare('introtext',$this->introtext,true);
		$criteria->compare('main_image',$this->main_image,true);
		$criteria->compare('published',$this->published,true);

		return new CActiveDataProvider($this, array(
                        'pagination'=>array(
				'pageSize'=>100,
			),
			'criteria'=>$criteria,
		));
	}
        
        public function scopes() {
            return array(
                'markCategory'=>array(
                    'select'=>array('id'),
                    'condition' => 't.published=1',
                )
            );
        }        
        
        private $_url;        
        
        public function getUrlMarkCat(){
            if ($this->_url === null)
                $this->_url = Yii::app()->createUrl('/testdrive/default/ModelList', array('mark' => $this->testdriveModel[0]->mark->mname));
            return $this->_url;
        }
        
        public function getUrlModelCat(){
            if ($this->_url === null)
                $this->_url = Yii::app()->createUrl('/testdrive/default/list', array('model' => $this->testdriveModel[0]->mname,'mark' => $this->testdriveModel[0]->mark->mname));
            return $this->_url;
        }
        
	public static function model($className=__CLASS__)
	{
            return parent::model($className);
	}
        
        public static function testdYaAvtoParser(){
            define('AC_DIR', dirname(__FILE__). DIRECTORY_SEPARATOR .'..'. DIRECTORY_SEPARATOR .'components' . DIRECTORY_SEPARATOR . 'aparser');
            require_once( AC_DIR . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'ParserTestdrive.php');     
        }
}