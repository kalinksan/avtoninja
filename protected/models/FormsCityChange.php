<?php
class FormsCityChange extends CFormModel
{
	public $id;
        
	public function rules()
	{
		return array(
			array('id', 'required','message'=>'Выберите город'),
			array('id', 'numerical', 'integerOnly'=>true),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'city_name' => 'Город',
		);
	}
}
