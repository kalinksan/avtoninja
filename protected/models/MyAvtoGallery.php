<?php

class MyAvtoGallery extends AvtoGallery
{
        public $relModels = array();
        public $relModelsStr = '';
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                        array('galleryModel', 'required'),
			array('gallery_id, published', 'numerical', 'integerOnly'=>true),
			array('razdel, created, changed, galleryModel', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('gallery_id, razdel, created, changed, published, relModelsStr', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'galleryModel' => array(self::MANY_MANY, 'MyMmodel', 'gallery_model(avto_gallery_id, model_id)'),
                    'gallery' => array(self::BELONGS_TO, 'Gallery', 'gallery_id'),
		);
	}
        
        public function beforeSave() {
            if ($this->isNewRecord){
                    $this->created = new CDbExpression('NOW()');
            }
            $this->changed = new CDbExpression('NOW()');
            if($this->razdel){
                $this->razdel = implode(',',$this->razdel);
            }
            return parent::beforeSave();
	}
        
        public function afterFind() {
            if($this->razdel){
                $this->razdel = explode(',',$this->razdel);
            }

            foreach($this->galleryModel as $am){
                $this->relModels[] = $am->name;
            }
            $this->relModels = implode(',',$this->relModels);
        }
        
        public function behaviors(){
		return array(
                    'ESaveRelatedBehavior' => array(
                        'class' => 'application.models.behaviors.ESaveRelatedBehavior'
                    ),
                    'galleryBehavior' => array(
                        'class' => 'application.models.behaviors.GalleryBehavior',
                        'idAttribute' => 'gallery_id',
                        'versions' => array(
                            'big' => array(
                                'cresize' => array(858, null),
                            )
                        ),
                        'name' => true,
                        'description' => false,
                    )              
		);
	}

	public function attributeLabels()
	{
		return array(
			'gallery_id' => 'Картинки в галерею',
                        'galleryModel' => 'Модель',
			'razdel' => 'Отображать в разделах',
			'created' => 'Создан',
			'changed' => 'Изменен',
			'published' => 'Публикация',
		);
	}
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                $criteria->with = array(
                    'galleryModel' => array()
                );
                $criteria->together = true;
                $criteria->group = 't.gallery_id';
                
                $criteria->compare("CONCAT(`galleryModel`.`name`)", $this->relModelsStr, true);
		$criteria->compare('gallery_id',$this->gallery_id);
		$criteria->compare('razdel',$this->razdel,true);
		//$criteria->compare('relModelsStr',$this->relModelsStr,true);
                
		$criteria->compare('created',$this->created,true);
		$criteria->compare('changed',$this->changed,true);
		$criteria->compare('published',$this->published);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=>100,
			),		
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
