<?php
class FormsOrderCall extends CFormModel
{
    public $id;
    public $title;
    public $phone;
    public $recaptcha;
    
    public function rules()
    {
        return array(
            array('recaptcha','required','except'=>'captchaCheck', 'message'=>'Пройдите проверку на робота'),
            array('id, title', 'safe'),
            array('phone', 'required','message'=>'Введите номер телефона'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'phone' => 'Телефон',
        );
    }
}
