<?php
class MyReview extends Review
{
        public static $smallImageDir = '/images/reviews/thumb/';
        public $searchMark;
        public $searchModel;
        public static $own = array(
            '1' => 'меньше 6 месяцев',
            '2' => 'от 6 месяцев до 1 года',
            '3' => 'от 1 до 2 лет',
            '4' => 'от 2 до 3 лет',
            '5' => 'от 3 до 5 лет',
            '6' => 'более 5 лет',
        );
        
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('title, fulltext, mark_id, model_id, own, year, face, comfort, safety, reliability, driving, email', 'required'),
			array('title, fulltext, mark_id, model_id, face, comfort, safety, reliability, driving', 'required'),
			array('moderation, user_id, own, year, face, comfort, safety, reliability, driving', 'numerical', 'integerOnly'=>true),
			array('title, main_image, email, metatitle, metakey', 'length', 'max'=>255),
                        array('email','email'),
			array('mark_id, model_id', 'length', 'max'=>20),
			array('plus, minus, created, changed, metadesc', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, fulltext, main_image, moderation, user_id, mark_id, model_id, own, year, plus, minus, face, comfort, safety, reliability, driving, email, created, changed, metatitle, metadesc, metakey', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'model' => array(self::BELONGS_TO, 'MyMmodel', 'model_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'mark' => array(self::BELONGS_TO, 'MyMark', 'mark_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Заголовок отзыва',
			'fulltext' => 'Текст отзыва',
			'main_image' => 'Фото автомобиля',
			'moderation' => 'Модерация',
			'user_id' => 'User',
			'mark_id' => 'Марка',
			'model_id' => 'Модель',
			'own' => 'Владею',
			'year' => 'Год выпуска',
			'plus' => 'Плюсы',
			'minus' => 'Минусы',
			'face' => 'Внешний вид',
			'comfort' => 'Комфорт',
			'safety' => 'Безопасность',
			'reliability' => 'Надежность',
			'driving' => 'Ходовые качества',
			'email' => 'Email',
			'created' => 'Создано',
			'changed' => 'Изменено',
			'metatitle' => 'Metatitle',
			'metadesc' => 'Metadesc',
			'metakey' => 'Metakey',                    
		);
	}
        
        public function beforeSave() {
            
            if (Yii::app()->user->id=='admin') {
                $this->user_id = Yii::app()->params['adminBaseId'];
            }
            if (!$this->user_id) {
                $this->user_id = Yii::app()->user->id;
            }
            if ($this->isNewRecord && !$this->created) {
                $this->created = new CDbExpression('NOW()');
            }
            $this->changed = new CDbExpression('NOW()');
            
            if ($this->isNewRecord){
                $email = Yii::app()->email;
                $email->to = $this->email;
                $email->subject = 'Добавлен новый отзыв на Авто.Нинзя';
                $email->message = 'Спасибо Вам за отставленный отзыв.<br>Отзыв будет опубликован на сайте после прохождения модерации.<br>Отзыв доступен по '.CHtml::link('ссылке',Yii::app()->createAbsoluteUrl('/review/default/view',array('id'=>$this->id))).'.';
                $email->send();                
            }elseif($this->moderation){
                $email = Yii::app()->email;
                $email->to = $this->email;
                $email->subject = 'Ваш отзыв опубликован на Авто.Нинзя';
                $email->message = 'Ваш отзыв опубликован на сайте.<br>Отзыв доступен по '.CHtml::link('ссылке',Yii::app()->createAbsoluteUrl('/review/default/view',array('id'=>$this->id))).'.';
                $email->send();                
            }
            return parent::beforeSave();
        }

        protected function beforeDelete() {
            if (!empty($this->main_image)) {
                $image = $this->main_image;
                findDelFile('images/reviews/', $image);
            }
            return parent::beforeDelete();
        }        

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('fulltext',$this->fulltext,true);
		$criteria->compare('main_image',$this->main_image,true);
		$criteria->compare('moderation',$this->moderation);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('mark_id',$this->mark_id,true);
		$criteria->compare('model_id',$this->model_id,true);
		$criteria->compare('own',$this->own);
		$criteria->compare('year',$this->year);
		$criteria->compare('plus',$this->plus,true);
		$criteria->compare('minus',$this->minus,true);
		$criteria->compare('face',$this->face);
		$criteria->compare('comfort',$this->comfort);
		$criteria->compare('safety',$this->safety);
		$criteria->compare('reliability',$this->reliability);
		$criteria->compare('driving',$this->driving);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('changed',$this->changed,true);
                $criteria->compare('metatitle',$this->metatitle,true);
		$criteria->compare('metadesc',$this->metadesc,true);
		$criteria->compare('metakey',$this->metakey,true);
                $criteria->compare('mark.name', $this->searchMark->name, true);
                $criteria->compare('model.name', $this->searchModel->name, true);
                $criteria->with = array('mark','model');
                $criteria->group = 't.id';
                $criteria->together = true;
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>array(
                            'pageSize'=>100,
			),
                        'sort'=>array(
                            'defaultOrder'=>'t.changed DESC',
                            'attributes'=>array(
                                'mark.name'=>array(
                                    'asc'=>'mark.name',
                                    'desc'=>'mark.name DESC',
                                ),
                                'model.name'=>array(
                                    'asc'=>'model.name',
                                    'desc'=>'model.name DESC',
                                ),
                                '*',
                            ),
                        ),
		));
	}

	public static function getIndexMenu($marks){	    
		$group = array();
		$groupPopular = array();
		$marksPopular = array();
                // Разбиваем массив категорий на 4 части для вывода
		$menuGroup = columnSizer($marks, 4);
                //Подготовка массива к выводу в виджет меню
		foreach($menuGroup as $gr => $menu){
			foreach($menu as $mark){
				$group[$gr][] = array(
					'label'=>$mark->name,
					'url'=>Yii::app()->createUrl('/review/default/ModelList', array('mark' => $mark->mname)),
                                        'linkOptions'=>array('class'=>$mark->popular ? 'popular' : ''),
                                        'template'=>'{menu}&nbsp;<span>' . count($mark->reviews, COUNT_RECURSIVE) . '</span>'
					);
                                if($mark->popular){
                                    $marksPopular[] = $mark;
                                }
			}
		}
                $menuGroupPopular = columnSizer($marksPopular, 4);
		foreach($menuGroupPopular as $gr => $menu){
			foreach($menu as $mark){
				$groupPopular[$gr][] = array(
					'label'=>$mark->name,
					'url'=>Yii::app()->createUrl('/review/default/ModelList', array('mark' => $mark->mname)),
                                        'template'=>'{menu}&nbsp;<span>' . count($mark->reviews, COUNT_RECURSIVE) . '</span>'
					);
			}
		}                
		return array('allMarks'=>$group,'popularMarks'=>$groupPopular);		
	}           
        
        public static function getIndexModelList($marks){
            $group = array();
            // Разбиваем массив категорий на 4 части для вывода
            $menuGroup = columnSizer($marks->automodels, 4);
            //Подготовка массива к выводу в виджет меню
            foreach($menuGroup as $gr => $menu){
                    foreach($menu as $model){
                            $group[$gr][] = array(
                                    'label'=>$model->name,
                                    'url'=>Yii::app()->createUrl('/review/default/list', array('model' => $model->mname,'mark' => $marks->mname)),
                                    'template'=>'{menu}&nbsp;<span>' . count($model->reviews, COUNT_RECURSIVE) . '</span>'
                                    );
                    }
            }    
            return $group;
        }        
        
        public function scopes() {
            return array(
                'published'=>array(
                    'condition'=>'t.moderation = 1',
                ),
                'markCategory'=>array(
                    'select'=>array('id','mark_id','model_id'),
                    'condition' => 't.moderation=1',
                )
            );
        }        
        
        private $_url;

        public function getUrl(){
            if ($this->_url === null)
                $this->_url = Yii::app()->createUrl('/review/default/view', array('id'=>$this->id,'mark' => $this->mark->mname,'model' => $this->model->mname));
            return $this->_url;
        }
        
        public function getUrlMarkCat(){
            if ($this->_url === null)
                $this->_url = Yii::app()->createUrl('/review/default/ModelList', array('mark' => $this->mark->mname));
            return $this->_url;
        }
        
        public function getUrlModelCat(){
            if ($this->_url === null)
                $this->_url = Yii::app()->createUrl('/review/default/list', array('model' => $this->model->mname,'mark' => $this->mark->mname));
            return $this->_url;
        }
        
	public static function model($className=__CLASS__)
	{
            return parent::model($className);
	}
}
