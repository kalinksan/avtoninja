<?php
class FormsWriteLetter extends CFormModel
{
    public $id;
    public $title;
    public $text;
    public $email;
    public $phone;
    public $recaptcha;
    
    public function rules()
    {
        return array(
            array('recaptcha','required','except'=>'captchaCheck', 'message'=>'Пройдите проверку на робота'),
            array('id, title, phone', 'safe'),
            array('text', 'required','message'=>'Введите ваш вопрос'),
            array('email', 'email','message'=>'Введен неверный E-mail'),
            array('email', 'required','message'=>'Введите ваш E-mail'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'phone' => 'Телефон',
        );
    }
}
