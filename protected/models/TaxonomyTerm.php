<?php

/**
 * This is the model class for table "taxonomy_term".
 *
 * The followings are the available columns in table 'taxonomy_term':
 * @property string $id
 * @property string $vid
 * @property string $mname
 * @property string $name
 * @property string $header
 * @property string $text
 * @property integer $visible
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 *
 * The followings are the available model relations:
 * @property Realty[] $realties
 * @property TaxonomyVoc $v
 */
class TaxonomyTerm extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'taxonomy_term';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vid, mname, name, header, text, seo_title, seo_description, seo_keywords', 'required'),
			array('visible', 'numerical', 'integerOnly'=>true),
			array('vid', 'length', 'max'=>11),
			array('mname', 'length', 'max'=>100),
			array('name, header, seo_title, seo_description, seo_keywords', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, vid, mname, name, header, text, visible, seo_title, seo_description, seo_keywords', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'catRealty' => array(self::BELONGS_TO, 'Realty', 'tid'),
			'voc' => array(self::BELONGS_TO, 'TaxonomyVoc', 'vid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'vid' => 'Vid',
			'mname' => 'Mname',
			'name' => 'Name',
			'header' => 'Header',
			'text' => 'Text',
			'visible' => 'Visible',
			'seo_title' => 'Seo Title',
			'seo_description' => 'Seo Description',
			'seo_keywords' => 'Seo Keywords',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('vid',$this->vid,true);
		$criteria->compare('mname',$this->mname,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('header',$this->header,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('visible',$this->visible);
		$criteria->compare('seo_title',$this->seo_title,true);
		$criteria->compare('seo_description',$this->seo_description,true);
		$criteria->compare('seo_keywords',$this->seo_keywords,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TaxonomyTerm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
