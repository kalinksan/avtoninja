<?php

/**
 * This is the model class for table "review".
 *
 * The followings are the available columns in table 'review':
 * @property string $id
 * @property string $title
 * @property string $fulltext
 * @property string $main_image
 * @property integer $moderation
 * @property integer $user_id
 * @property string $mark_id
 * @property string $model_id
 * @property integer $own
 * @property integer $year
 * @property string $plus
 * @property string $minus
 * @property integer $face
 * @property integer $comfort
 * @property integer $safety
 * @property integer $reliability
 * @property integer $driving
 * @property string $email
 * @property string $created
 * @property string $changed
 * @property string $metatitle
 * @property string $metadesc
 * @property string $metakey
 *
 * The followings are the available model relations:
 * @property Users $user
 * @property Autobrand $mark
 * @property Automodel $model
 */
class Review extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'review';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, fulltext, user_id, mark_id, model_id, own, year, face, comfort, safety, reliability, driving, email', 'required'),
			array('moderation, user_id, own, year, face, comfort, safety, reliability, driving', 'numerical', 'integerOnly'=>true),
			array('title, main_image, email, metatitle, metakey', 'length', 'max'=>255),
			array('mark_id, model_id', 'length', 'max'=>20),
			array('plus, minus, created, changed, metadesc', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, fulltext, main_image, moderation, user_id, mark_id, model_id, own, year, plus, minus, face, comfort, safety, reliability, driving, email, created, changed, metatitle, metadesc, metakey', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'mark' => array(self::BELONGS_TO, 'Autobrand', 'mark_id'),
			'model' => array(self::BELONGS_TO, 'Automodel', 'model_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'fulltext' => 'Fulltext',
			'main_image' => 'Main Image',
			'moderation' => 'Moderation',
			'user_id' => 'User',
			'mark_id' => 'Mark',
			'model_id' => 'Model',
			'own' => 'Own',
			'year' => 'Year',
			'plus' => 'Plus',
			'minus' => 'Minus',
			'face' => 'Face',
			'comfort' => 'Comfort',
			'safety' => 'Safety',
			'reliability' => 'Reliability',
			'driving' => 'Driving',
			'email' => 'Email',
			'created' => 'Created',
			'changed' => 'Changed',
			'metatitle' => 'Metatitle',
			'metadesc' => 'Metadesc',
			'metakey' => 'Metakey',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('fulltext',$this->fulltext,true);
		$criteria->compare('main_image',$this->main_image,true);
		$criteria->compare('moderation',$this->moderation);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('mark_id',$this->mark_id,true);
		$criteria->compare('model_id',$this->model_id,true);
		$criteria->compare('own',$this->own);
		$criteria->compare('year',$this->year);
		$criteria->compare('plus',$this->plus,true);
		$criteria->compare('minus',$this->minus,true);
		$criteria->compare('face',$this->face);
		$criteria->compare('comfort',$this->comfort);
		$criteria->compare('safety',$this->safety);
		$criteria->compare('reliability',$this->reliability);
		$criteria->compare('driving',$this->driving);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('changed',$this->changed,true);
		$criteria->compare('metatitle',$this->metatitle,true);
		$criteria->compare('metadesc',$this->metadesc,true);
		$criteria->compare('metakey',$this->metakey,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Review the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
