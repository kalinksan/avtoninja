<?php

/**
 * This is the model class for table "objects".
 *
 * The followings are the available columns in table 'objects':
 * @property string $id
 * @property string $mark_id
 * @property string $model_id
 * @property integer $user_id
 * @property string $user_name
 * @property string $user_email
 * @property string $user_phone
 * @property string $city_id
 * @property string $metro
 * @property integer $year
 * @property string $cost
 * @property string $km_age
 * @property string $body_type
 * @property string $color
 * @property integer $volume
 * @property string $transmission
 * @property string $engine
 * @property string $privod
 * @property string $wheel
 * @property integer $state
 * @property string $description
 * @property string $created
 * @property string $updated
 * @property string $turbodealer_id
 * @property string $service
 * @property integer $published
 * @property integer $object_status
 * @property string $check_data
 *
 * The followings are the available model relations:
 * @property Users $user
 * @property City $city
 * @property Autobrand $mark
 * @property Automodel $model
 * @property ObjectsImages[] $objectsImages
 */
class Objects extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'objects';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('mark_id, model_id, user_id, user_name, user_phone, city_id, year, cost, km_age, body_type, color, volume, transmission, engine, privod, wheel, created, updated', 'required'),
			array('user_id, year, volume, state, published, object_status', 'numerical', 'integerOnly'=>true),
			array('mark_id, model_id, user_phone, city_id, metro, cost, km_age, turbodealer_id', 'length', 'max'=>20),
			array('user_name', 'length', 'max'=>255),
			array('user_email, body_type, color, transmission, engine, privod, wheel', 'length', 'max'=>50),
			array('service', 'length', 'max'=>100),
			array('description, check_data', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, mark_id, model_id, user_id, user_name, user_email, user_phone, city_id, metro, year, cost, km_age, body_type, color, volume, transmission, engine, privod, wheel, state, description, created, updated, turbodealer_id, service, published, object_status, check_data', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'city' => array(self::BELONGS_TO, 'City', 'city_id'),
			'mark' => array(self::BELONGS_TO, 'Autobrand', 'mark_id'),
			'model' => array(self::BELONGS_TO, 'Automodel', 'model_id'),
			'objectsImages' => array(self::HAS_MANY, 'ObjectsImages', 'entity_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'mark_id' => 'Mark',
			'model_id' => 'Model',
			'user_id' => 'User',
			'user_name' => 'User Name',
			'user_email' => 'User Email',
			'user_phone' => 'User Phone',
			'city_id' => 'City',
			'metro' => 'Metro',
			'year' => 'Year',
			'cost' => 'Cost',
			'km_age' => 'Km Age',
			'body_type' => 'Body Type',
			'color' => 'Color',
			'volume' => 'Volume',
			'transmission' => 'Transmission',
			'engine' => 'Engine',
			'privod' => 'Privod',
			'wheel' => 'Wheel',
			'state' => 'State',
			'description' => 'Description',
			'created' => 'Created',
			'updated' => 'Updated',
			'turbodealer_id' => 'Turbodealer',
			'service' => 'Service',
			'published' => 'Published',
			'object_status' => 'Object Status',
			'check_data' => 'Check Data',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('mark_id',$this->mark_id,true);
		$criteria->compare('model_id',$this->model_id,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('user_email',$this->user_email,true);
		$criteria->compare('user_phone',$this->user_phone,true);
		$criteria->compare('city_id',$this->city_id,true);
		$criteria->compare('metro',$this->metro,true);
		$criteria->compare('year',$this->year);
		$criteria->compare('cost',$this->cost,true);
		$criteria->compare('km_age',$this->km_age,true);
		$criteria->compare('body_type',$this->body_type,true);
		$criteria->compare('color',$this->color,true);
		$criteria->compare('volume',$this->volume);
		$criteria->compare('transmission',$this->transmission,true);
		$criteria->compare('engine',$this->engine,true);
		$criteria->compare('privod',$this->privod,true);
		$criteria->compare('wheel',$this->wheel,true);
		$criteria->compare('state',$this->state);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('turbodealer_id',$this->turbodealer_id,true);
		$criteria->compare('service',$this->service,true);
		$criteria->compare('published',$this->published);
		$criteria->compare('object_status',$this->object_status);
		$criteria->compare('check_data',$this->check_data,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Objects the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
