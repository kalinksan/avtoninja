<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $name
 * @property string $username
 * @property string $email
 * @property string $password
 * @property integer $block
 * @property integer $gid
 * @property string $registerDate
 * @property string $lastvisitDate
 * @property string $activation
 * @property string $vkontakte
 * @property string $google
 * @property string $facebook
 * @property integer $confirm_email
 * @property string $hash_key
 * @property string $phone
 */
class User extends CActiveRecord
{
    	public $password2;
	public $new_password;
	public $captcha;
	public $rememberMe;
        
        private $_identity;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}
	
        // проверяем не заблокирован ли пользователь
	public function beforeValidate() {
		$id = Yii::app()->user->id;
		if($id > 0) {
			$user = User::model()->findByPk($id);
			if($user->block == 1) {
				Yii::app()->user->logout();
			}
		}
		return parent::beforeValidate();
	}      
        
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, email, password, name, phone, new_password', 'filter', 'filter' => 'strip_tags'),
			array('username, email, password, name, phone, new_password', 'filter', 'filter' => array(Yii::app()->input, 'purify')),
			
			array('email, password', 'required', 'except'=>'changeUsername, updateProfile, addEmail, changePassProfile, recovery, changePass, admin, adminUserUpdate', 'message'=>'Обязательное поле'),
		
			
			// проверка блокировки пользователя
			array('email', 'userBlockCheck', 'on'=>'login, recovery, registration'),		
			
			// правила для формы регистрации
			array('name', 'required', 'on'=>'registration', 'message'=>'Обязательное поле'),
			array('email', 'unique', 'on'=>'registration'),
			
			// для связки аккаунтов
			array('email', 'userBlockCheck', 'on'=>'addEmail, updateProfile'),
			array('email', 'unique', 'on'=>'addEmail, updateProfile', 'message'=>'Такая почта уже есть на сайте. Если это ваш адрес, вы можете объединить аккаунты.<br />'.CHtml::button('Связать аккаунты', array('id'=>'account_binding'))),
			array('username', 'required', 'on'=>'choicesUsername', 'message'=>'Обязательное поле'),
			
			array('email', 'length',  'max' => '100', 'on'=>'registration'),
                        array('password, new_password', 'length', 'min' => '5', 'on'=>'registration, changePass, changePassProfile'),
			
			array('phone, name, vkontakte, facebook, google', 'length', 'max'=>255),
			array('name, registerDate, lastvisitDate', 'safe'),
			
			// для редактирования профиля
			array('email', 'required', 'on'=>'addEmail, updateProfile', 'message'=>'Обязательное поле'),
			
			// для восстановления пароля
			array('email', 'required', 'on'=>'recovery', 'message'=>'Обязательное поле'),
			
			// правила для входа на сайт через соц. сети
			// array('service_id', 'required', 'on'=>'serviceLogin'),

			// правила для формы входа в админку
			array('username, password', 'required', 'on'=>'admin'),
			array('password', 'authenticateAdmin', 'on'=>'admin'),
			
			// общие правила
			array('id, block, gid', 'numerical', 'integerOnly'=>true),
			//array('email', 'match', 'pattern' => '/^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$/', 'message' => 'Неверный формат e-mail адреса.', 'except'=>'changeUsername, changePassProfile, changePass'),
			array('email', 'email', 'message' => 'Неверный формат e-mail адреса.', 'except'=>'changeUsername, changePassProfile, changePass'),
                        array('username', 'match', 'pattern' => '/^[-A-Za-z0-9_\s\.]+$/u','message'  => 'Логин содержит недопустимые символы.', 'except'=>'updateProfile, addEmail'),
			
			// изменение логина для почты для домена
			array('username', 'required', 'on'=>'changeUsername'),
			array('username', 'unique', 'on'=>'changeUsername'),
			
			// для формы изменения пароля в профиле
			array('password, password2, new_password', 'required', 'on'=>'changePassProfile', 'message'=>'Обязательное поле'),
			array('password2', 'compare', 'compareAttribute' => 'new_password', 'message' => 'Пароли должны совпадать', 'on'=>'changePassProfile'),
			
			// для формы изменения пароля
			array('password, password2', 'required', 'on'=>'changePass', 'message'=>'Обязательное поле'),
			array('password2', 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли должны совпадать', 'on'=>'changePass'),

			// для восстановления пароля
			array('email', 'exist', 'className'=>'User', 'attributeName' => 'email', 'criteria'=>array('condition'=>'block=:block', 'params'=>array(':block'=>0)), 'on'=>'recovery', 'message'=>'Email не найден в базе данных'),
			
			// аутентификация пользователя
			array('email, password', 'authenticate', 'on'=>'login'),
			
			array('id, name, username, email, password, block, gid, registerDate, lastvisitDate, name, phone, activation', 'safe', 'on'=>'search'),
		);
	}

	public function userBlockCheck($attribute,$params) {
		if(!$this->hasErrors())
		{
			$user = User::model()->findByAttributes(array('email'=>$this->email));
			if($user->block) {
				$this->addError($attribute, 'Пользователь заблокирован.');
			}
		}
	}
        
	/* методы для валидации полей формы */
	// аутентификация (срабатывает только для сценария login)
	public function authenticate($attribute,$params)
	{
		if(!$this->hasErrors())
		{
			$this->_identity=new UserIdentity(array('email'=>$this->email),$this->password);
			if(!$this->_identity->authenticate()) {
				$this->addError($attribute, 'Такого пользователя не существует.');
				//$this->addErrors(array('Такого пользователя нет.'));	// в errorSummary 
			}
		}
	}
        
	// аутентификация (срабатывает только для сценария admin)
	public function authenticateAdmin($attribute,$params)
	{
		if(!$this->hasErrors())
		{
			$this->_identity=new UserIdentity($this->username,$this->password);
			if(!$this->_identity->authenticate()) {
				$this->addErrors(array('Неправильные логин или пароль.'));
			}
		}
	}        
        
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'objects' => array(self::HAS_MANY, 'Objects', 'user_id'),
                    'reviews' => array(self::HAS_MANY, 'Review', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Ваше имя',
			'username' => 'Логин',
			'password' => 'Пароль',
			'email' => 'Email',
			'password2' => 'Повторите пароль',
			'new_password' => 'Новый пароль',
                        'captcha'   => 'Введите код с картинки',
			'block' => 'Заблокирован',
			'gid' => 'Гид',
			'registerDate' => 'Дата регистрации',
			'lastvisitDate' => 'Последний визит',
			'activation' => 'Activation',
			'rememberMe' => 'Запомнить меня',
			'phone' => 'Телефон',
			'confirm_email' => 'Подтвердил email',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('block',$this->block);
		$criteria->compare('gid',$this->gid);
		$criteria->compare('registerDate',$this->registerDate,true);
		$criteria->compare('lastvisitDate',$this->lastvisitDate,true);
		$criteria->compare('activation',$this->activation,true);
		$criteria->compare('vkontakte',$this->vkontakte,true);
		$criteria->compare('google',$this->google,true);
		$criteria->compare('facebook',$this->facebook,true);
		$criteria->compare('confirm_email',$this->confirm_email);
		$criteria->compare('phone',$this->phone,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	// вход на сайт и в админку
	public function login($field)
	{
		if($this->_identity===null)
		{
                    $this->_identity=new UserIdentity($field,$this->password);
                    $this->_identity->authenticate();
		}
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
		{
                    //$duration=3600*24*30; // 30 days
                    Yii::app()->user->login($this->_identity);	// если передать duration, будет использоваться авторизация через cookie
                    return true;
		}
		else
                return false;
	}
	
	
	public function validatePassword($password) {
		$parts	= explode(':', $this->password);
		$crypt	= $parts[0];
		$salt	= @$parts[1];
		
		$testcrypt = $this->getCryptedPassword($password, $salt);
		if($crypt == $testcrypt) {
			return true;
		}
		return false;
	}
	function getCryptedPassword($plaintext, $salt = '', $show_encrypt = false)
	{
		$encrypted = ($salt) ? md5($plaintext.$salt) : md5($plaintext);
		return ($show_encrypt) ? '{MD5}'.$encrypted : $encrypted;
	}        
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
