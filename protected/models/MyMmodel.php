<?php
class MyMmodel extends Mmodel
{
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('mark_id, name', 'required','except'=>'turboexp'),
			array('mark_id, name', 'required','on'=>'turboexp'),
			array('mark_id', 'length', 'max'=>20),
			array('name, mname', 'length', 'max'=>255),
                        array('text_block, text_block_review, synonym, review_metadesc, testdrive_metadesc', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('model_id, mark_id, name, mname, text_block, text_block_review, synonym, review_metadesc, testdrive_metadesc', 'safe', 'on'=>'search'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'model_id' => 'ID',
			'mark_id' => 'Марка',
			'name' => 'Название модели',
                        'synonym' => 'Синоним (елси несколько, то указывать через запятую)',
                        'text_block' => 'Текст в тест-драйвах',
                        'text_block_review' => 'Текст в отзывах',
			'mname' => 'Машинное имя модели',
                        'review_metadesc' => 'Отзывы meta description',
			'testdrive_metadesc' => 'Тест-драйвы meta description',                    
		);
	}
        
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'mark' => array(self::BELONGS_TO, 'MyMark', 'mark_id'),
			'objects' => array(self::HAS_MANY, 'MyObjects', 'model_id'),
                        'galleries' => array(self::MANY_MANY, 'MyAvtoGallery', 'gallery_model(model_id, avto_gallery_id)'),
                        'testdrives' => array(self::MANY_MANY, 'MyTestdrive', 'testdrive_model(model_id, testdrive_id)'),
                        'reviews' => array(self::HAS_MANY, 'Review', 'model_id'),
		);
	}        
        
	public function defaultScope()
	{
		return array(
			//'order' => 't.name DESC',
		);      

	}
	
        public function behaviors(){
		return array(
			'SlugBehavior' => array(
					'class' => 'application.models.behaviors.SlugBehavior',
					'slug_col' => 'mname',
					'title_col' => 'name',
					'max_slug_chars' => 10,
					'overwrite' => true
			),             
		);
	}        
        
        public static function resave(){
            $marks = MyMmodel::model()->findAll(array('condition'=>'mname=""'));
            foreach($marks as $mark){
                $mark->save();  
            }
        }
	
        public static function modelAdminChoices(){
            $list=MyMmodel::model()->with('mark')->findAll(array('order'=>'t.mark_id'));
            $mmodels = array();
            foreach($list as $l){
                $mmodels[$l['model_id']] = $l->name . ' ( ' . $l->mark->name . ' )';
            }
            return $mmodels;
        }
        
	public static function MarkChoices($count='all')
	{
		if($count=='popular'){
			return CHtml::listData(MyMark::model()->findAll(array('order'=>'popular DESC','condition'=>'popular=:popular','params'=>array(':popular'=>1))), 'mark_id', 'name');
		}
		if($count=='non-popular'){
			return CHtml::listData(MyMark::model()->findAll(array('condition'=>'popular=:popular','params'=>array(':popular'=>0))), 'mark_id', 'name');
		}
		return CHtml::listData(MyMark::model()->findAll(array('order'=>'popular DESC, name')), 'mark_id', 'name');
	}
	
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('model_id',$this->model_id,true);
		$criteria->compare('mark_id',$this->mark_id,true);
		$criteria->compare('mname',$this->mname,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('text_block',$this->text_block,true);
		$criteria->compare('text_block_review',$this->text_block_review,true);
		$criteria->compare('synonym',$this->synonym,true);
                $criteria->compare('review_metadesc',$this->review_metadesc,true);
		$criteria->compare('testdrive_metadesc',$this->testdrive_metadesc,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=>100,
			),		
			'criteria'=>$criteria,
		));
	}	
        
	public static function model($className=__CLASS__)
	{
            return parent::model($className);
	}
        
}
