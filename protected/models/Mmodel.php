<?php

/**
 * This is the model class for table "automodel".
 *
 * The followings are the available columns in table 'automodel':
 * @property string $model_id
 * @property string $mark_id
 * @property string $mname
 * @property string $name
 * @property string $modelrus
 * @property string $synonym
 * @property string $text_block
 * @property string $text_block_review
 * @property string $review_metadesc
 * @property string $testdrive_metadesc
 *
 * The followings are the available model relations:
 * @property Autobrand $mark
 * @property AvtoGallery[] $avtoGalleries
 * @property Objects[] $objects
 * @property Review[] $reviews
 * @property Testdrive[] $testdrives
 */
class mmodel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'automodel';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('mark_id, mname, name', 'required'),
			array('mark_id', 'length', 'max'=>20),
			array('mname, name, synonym', 'length', 'max'=>255),
			array('modelrus', 'length', 'max'=>100),
			array('text_block, text_block_review, review_metadesc, testdrive_metadesc', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('model_id, mark_id, mname, name, modelrus, synonym, text_block, text_block_review, review_metadesc, testdrive_metadesc', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'mark' => array(self::BELONGS_TO, 'Autobrand', 'mark_id'),
			'avtoGalleries' => array(self::MANY_MANY, 'AvtoGallery', 'gallery_model(model_id, avto_gallery_id)'),
			'objects' => array(self::HAS_MANY, 'Objects', 'model_id'),
			'reviews' => array(self::HAS_MANY, 'Review', 'model_id'),
			'testdrives' => array(self::MANY_MANY, 'Testdrive', 'testdrive_model(model_id, testdrive_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'model_id' => 'Model',
			'mark_id' => 'Mark',
			'mname' => 'Mname',
			'name' => 'Name',
			'modelrus' => 'Modelrus',
			'synonym' => 'Synonym',
			'text_block' => 'Text Block',
			'text_block_review' => 'Text Block Review',
			'review_metadesc' => 'Review Metadesc',
			'testdrive_metadesc' => 'Testdrive Metadesc',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('model_id',$this->model_id,true);
		$criteria->compare('mark_id',$this->mark_id,true);
		$criteria->compare('mname',$this->mname,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('modelrus',$this->modelrus,true);
		$criteria->compare('synonym',$this->synonym,true);
		$criteria->compare('text_block',$this->text_block,true);
		$criteria->compare('text_block_review',$this->text_block_review,true);
		$criteria->compare('review_metadesc',$this->review_metadesc,true);
		$criteria->compare('testdrive_metadesc',$this->testdrive_metadesc,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return mmodel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
