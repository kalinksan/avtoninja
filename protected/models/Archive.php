<?php

/**
 * This is the model class for table "archive".
 *
 * The followings are the available columns in table 'archive':
 * @property string $id
 * @property string $title
 * @property string $fulltext
 * @property string $oldurl
 * @property string $type
 * @property string $metatitle
 * @property string $metakey
 * @property string $metadesc
 * @property integer $noindex
 * @property string $canonical
 */
class Archive extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'archive';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('noindex', 'numerical', 'integerOnly'=>true),
			array('title, oldurl, metatitle, metakey, canonical', 'length', 'max'=>255),
			array('type', 'length', 'max'=>50),
			array('fulltext, metadesc', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, fulltext, oldurl, type, metatitle, metakey, metadesc, noindex, canonical', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'fulltext' => 'Fulltext',
			'oldurl' => 'Oldurl',
			'type' => 'Type',
			'metatitle' => 'Metatitle',
			'metakey' => 'Metakey',
			'metadesc' => 'Metadesc',
			'noindex' => 'Noindex',
			'canonical' => 'Canonical',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('fulltext',$this->fulltext,true);
		$criteria->compare('oldurl',$this->oldurl,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('metatitle',$this->metatitle,true);
		$criteria->compare('metakey',$this->metakey,true);
		$criteria->compare('metadesc',$this->metadesc,true);
		$criteria->compare('noindex',$this->noindex);
		$criteria->compare('canonical',$this->canonical,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Archive the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
