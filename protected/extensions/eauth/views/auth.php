<div class="services">
  <ul class="auth-services clear">
  <?php
	foreach ($services as $name => $service) {
		echo '<li class="auth-service '.$service->id.'">';
		$html = '<span class="auth-icon '.$service->id.'"><i></i></span>';
		$html .= '<span class="auth-title">'.$service->title.'</span>';
		$html = CHtml::link($html, Yii::app()->createUrl('/user/login/', array('service'=>$name)), array(
			'class' => 'auth-link '.$service->id,
		));
		echo $html;
		echo '</li>';
	}
  ?>
  </ul>
</div>
<?php
/*foreach ($services as $name => $service) {
	$html = '<p class="auth-service '.$service->id.'">';
	$html .= '<a class="auth-link" href="'.Yii::app()->createUrl('/user/login/', array('service'=>$name)).'"></a></p>';
	echo $html;
}*/
?>