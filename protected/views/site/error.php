<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';

?>
<div class="inner_block">
	<h2>Error <?php echo $code; ?></h2>
	<?php
	switch ($code){
	case 404:
		echo 'Страницы не существует';
		break;
	case 403:
		echo 'Доступ запрещен';
		break;
	}
?>
</div>