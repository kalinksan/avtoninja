<div class="location_block">
    <div class="city_block <?php echo $guessVisible ? 'visible' : ''?>">
        <p>Ваш город — <?php echo $cityName ?></p>
        <p>Угадали?</p>
        <div>
                <a href="#city_change_block" class="no inline">нет</a>
                <?php echo CHtml::ajaxLink(
                    $text = 'да', 
                    $url = 'site/CityCheck', 
                    $ajaxOptions=array (
                        'type'=>'POST',
                        'data'=>array(
                            'check'=>'yes',
                            Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                        ),
                        //'success'=>'function(html){ jQuery("#your_id").html(html); }'
                    ), 
                    $htmlOptions=array('class'=>'yes')
                );?>
        </div>
    </div>
    <div class="triangle"></div>
    <div class="city"><?php echo $cityName ?></div>
    <div class="place"></div>
</div>
<div style="display:none">
    <div id="city_change_block" class="pop-form">
        <div class="block_header">Выберите Ваш город</div>
        <div class="add_block">
            <?php
                $form=Yii::app()->controller->beginWidget('CActiveForm', array(
                    'id'=>'city-cahnge-form',
                    'method' => 'POST',
                    'action'=>Yii::app()->createUrl('site/CityChange'),
                    'clientOptions' => array(
                        'validateOnSubmit' => 'true',
                    ),
                    'enableAjaxValidation' => true,
                    'enableClientValidation'=>true,
                )); 
            ?>
                <div class="change-city-field">
                        <?php echo $form->dropDownList($model,'id', MyObjects::CityChoices(),array('empty'=>'Город','data-placeholder' => 'Город')); ?>
                        <?php echo $form->error($model,'id'); ?>
                </div>
                <div class="buttons">
                        <?php echo CHtml::submitButton('Сохранить',array('class'=>'publish')); ?>
                </div>
            <?php Yii::app()->controller->endWidget(); ?>
        </div>
    </div>
</div>