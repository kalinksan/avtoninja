<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
    <aside id="right_content_block" >
        <?php echo $this->clips['sidebar'];?>
        <?php //echo $this->clips['fixed'];?>
    </aside>
    <div id="left_content_block">
        <?php echo $content; ?>			
    </div>
<?php $this->endContent(); ?>