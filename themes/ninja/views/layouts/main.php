<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
	<title><?php echo CHtml::encode($this->pageTitle) . ' на Авто.Ниндзя'; ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/images/favicon.ico" type="image/x-icon" />
	<link rel="canonical" href="<?php echo !empty($this->pageCanonicalUrl) ? $this->pageCanonicalUrl : Yii::app()->request->getHostInfo() . '/' . Yii::app()->request->getPathInfo();?>" />
	<?php if(!$this->pageRobotsIndex):?>
            <meta name="robots" content="noindex">
	<?php endif;?>
	<meta name="description" content="<?php echo !empty($this->pageDescription) ? CHtml::encode($this->pageDescription) : ""; ?>">
	<meta name="keywords" content="<?php echo !empty($this->pageKeywords) ? CHtml::encode($this->pageKeywords) : ""; ?>">
	<meta name='yandex-verification' content='4912d34e72db349e' />
	<meta name="google-site-verification" content="gkVdfnuYp9l03mocG7zoqY6nkN72ppqg0aodu34e2eo" />
	<meta name="google-site-verification" content="bsTK02US9AqJlossYp1VJsDhPSeepmhrmUytIwJccjk" />
	<meta name="msvalidate.01" content="505178D7073EF6C15DB4517F49FE331A" />
	<?php Yii::app()->clientScript->registerCssFile(Yii::app()->controller->assetsBase.'/css/style.css');?>
	<?php Yii::app()->clientScript->registerCssFile(
		Yii::app()->clientScript->getCoreScriptUrl().
		'/jui/css/base/jquery-ui.css'
	);?>
	<?php Yii::app()->clientScript->registerCssFile(Yii::app()->controller->assetsBase.'/css/colorbox.css');?>
	<?php Yii::app()->clientScript->registerCssFile(Yii::app()->controller->assetsBase.'/css/jquery.formstyler.css');?>
	<!--[if IE]>
	<script>
		document.createElement('header');
		document.createElement('nav');
		document.createElement('section');
		document.createElement('article');
		document.createElement('aside');
		document.createElement('footer');
	</script>
	<![endif]-->        

	<?php Yii::app()->getClientScript()->registerCoreScript('jquery');?>
	<?php //Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->assetsBase.'/js/jquery.min.js');?>
	<?php //Yii::app()->getClientScript()->registerCoreScript( 'jquery.ui' );?>
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->assetsBase.'/js/jquery-contained-sticky-scroll-min.js');?>
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->assetsBase.'/js/jquery.placeholder.min.js');?>
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->assetsBase.'/js/jquery.colorbox.js');?>
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->assetsBase.'/js/jquery.tagsinput.js');?>
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->assetsBase.'/js/jquery.cookie.js');?>
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->assetsBase.'/js/utils.js');?>
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->assetsBase.'/js/jquery.formstyler.js');?>
	<?php /*Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->assetsBase.'/js/jquery.browser.js');*/?>
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->assetsBase.'/js/script.js');?>
        <?php Yii::app()->clientScript->registerScript('pulso', "
         (function() {
          if (window.pluso)if (typeof window.pluso.start == 'function') return;
          if (window.ifpluso==undefined) { window.ifpluso = 1;
            var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
            s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
            s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
            var h=d[g]('body')[0];
            h.appendChild(s);
          }})();"); 
        ?>
</head>
<body>
    <header id="header">
	<div class="wrapper">
	<div class="container">
		<div class="logo"><?php echo CHtml::link(CHtml::image(Yii::app()->controller->assetsBase.'/images/logo.png','',array('width'=>'203','height'=>'45')), Yii::app()->homeUrl);?></div>
		<div class="login_block">
                    <?php if(Yii::app()->user->isGuest): ?>
                        <?php echo CHtml::link('Вход', '#login_block2',array('class'=>'inline'));?>
                    <?php else:?>
                        <?php echo CHtml::link('Выход', Yii::app()->createUrl('user/logout'));?>
                    <?php endif;?>
                </div>
            <?php $this->widget('application.components.FindCityWidget'); ?>
	</div>
	</div>
    </header>
    <div id="main_container">
	<div class="wrapper">
            <div class="container">
                    <ul id="menu">
                            <!-- <li>
                                   <a rel="nofollow" href="http://ad.avto.ninja/1" class="find">Подберите<br/>мне авто</a>					
                            </li> -->
                            <li>
                                <?php 
                                    echo CHtml::link('Подать<br />объявление', Yii::app()->createUrl('objects/create'),array('class'=>'adv','rel'=>'nofollow'));
                                    /*if(!Yii::app()->user->isGuest){
                                        echo CHtml::link('Подать<br />объявление',  Yii::app()->createUrl('objects/create'),array('class'=>'adv','rel'=>'nofollow'));
                                    }else{
                                        echo CHtml::link('Подать<br />объявление', '#login_block2',array('class'=>'inline adv','rel'=>'nofollow','data-destination'=>'/objects/create'));
                                    }*/
                                ?>
                            </li>
                            <li>
                                <?php 
                                    echo CHtml::link('Добавить<br />отзыв', Yii::app()->createUrl('/review/default/create'),array('class'=>'adv','rel'=>'nofollow'));
                                ?>
                            </li>                        
                           <!-- <li>
                                    <a rel="nofollow" href="http://ad.avto.ninja/2" class="safety">Купить безопасно</a>					
                            </li>-->
                    </ul>
                    <div class="spacer"></div>
                    <div id="main_submenu">
                        <div class="sub-links">
                            <?php echo CHtml::link('Тест-драйвы',$this->createUrl('/test-drive'));?>
                            <?php echo CHtml::link('Отзывы',$this->createUrl('/review'));?>
                        </div>
                        
                        <div class="pluso" data-background="transparent" data-options="medium,round,line,horizontal,counter,theme=04" data-services="facebook,vkontakte,twitter,google" data-user="355250779"></div>
                    </div>
                    <div class="spacer"></div>
                    <?php echo $content;?>
            </div>
	</div>
    </div>
    <footer id="footer">
            <div class="wrapper">
            <div class="container">
                    <div class="copy">&copy; 2014 — 2015 ООО "Авто.Ниндзя"</div>
                    <div class="footer_pane1">
                            <ul>
                                    <li><a href="#">Помощь </a></li>
                                    <li><a href="#">Обратная связь</a></li>
                                    <li>Москва, ул. Никольская, 19/21</li>
                            </ul>
                    </div>
                    <div class="footer_pane2">
                            <ul>
                                    <li>Для партнеров</li>
                                    <li>Реклама</li>
                                    <li>Статистика</li>
                            </ul>
                    </div>
                    <div class="footer_links">					
                            <?php echo CHtml::link(CHtml::image(Yii::app()->controller->assetsBase.'/images/vk.png','',array('width'=>'42','height'=>'42')), 'https://vk.com/avto.ninja', array('rel'=>'nofollow'));?>
                            <?php echo CHtml::link(CHtml::image(Yii::app()->controller->assetsBase.'/images/tw.png','',array('width'=>'42','height'=>'42')), 'https://twitter.com/falkovsky', array('rel'=>'nofollow'));?>
                            <?php echo CHtml::link(CHtml::image(Yii::app()->controller->assetsBase.'/images/fb.png','',array('width'=>'42','height'=>'42')), 'https://www.facebook.com/Avto.Ninja', array('rel'=>'nofollow'));?>
                            <div class="visa"><?php echo CHtml::image(Yii::app()->controller->assetsBase.'/images/visa.png','',array('width'=>'92','height'=>'52'));?></div>
                    </div>
            </div>
            </div>
    </footer>
    <?php if(Yii::app()->user->isGuest): ?>
    <div style="display:none">
	<div id="login_block2">
		<div class="block_header">ВХОД</div>
		<div class="fb">
                    <?php 
                        echo CHtml::link('facebook', Yii::app()->createUrl('/user/login/', array('service'=>'facebook')));
                    ?>
                </div>
                <div class="google">
                    <?php 
                        echo CHtml::link('google', Yii::app()->createUrl('/user/login/', array('service'=>'google')));
                    ?>
                </div>
                <div class="vk">
                    <?php 
                        echo CHtml::link('vkontakte', Yii::app()->createUrl('/user/login/', array('service'=>'vkontakte')));
                    ?>
                </div>		
	</div>
    </div>
    <?php endif;?>
    <!-- Google Tag Manager --> <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PH8WSZ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-PH8WSZ');</script> <!-- End Google Tag Manager -->
	<!-- Hello Bar  <script src="//my.hellobar.com/9383c7dbeee8a931d5eadb61060c084a5b9bfbc8.js" type="text/javascript" charset="utf-8" async="async"></script> End Hello Bar -->
	</body>	
</html>