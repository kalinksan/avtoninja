$(document).ready(function(){ 
    /*$('.psevdo_link').replaceWith(function(){
        return '<a href="' + $(this).attr('data-url')
            + '" target="_blank'
            + '">' + $(this).html() + '</a>';
    });*/
    
    $(".psevdo_link").each(function() {
        var idTestdrive = $(this).attr('data-id');
        var link = $(this);
        $.post("/test-drive/ReturnLink", {id: idTestdrive}, function(data) {
            link.replaceWith(data);
        });
    });
    
    $('.video_testdrive').click(function(){
        textBlock = $(this).siblings('.toggle_text');
        textBlock.slideToggle('fast', function(){
            if(textBlock.is(":visible")){
                var scrollTop = textBlock.offset().top;
                $("html, body").animate({ scrollTop: scrollTop }, 500);
            }
        });

    });
});

